import helpers from './helpers.js'
import crypt from './crypto.js'
import {LoginError} from './errors.js'
var asset = {}
// assetAdmin is logged in user
asset.newAsset = async (assetName,assetAdmin,assetMinters,balance,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        assetAdmin: assetAdmin,
        assetMinters: assetMinters,
        balance: parseInt(balance)
        })
    // console.log(data)
    const signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: helpers.arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/newAsset"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while creating new asset: ' + e)
        })
}

asset.getMinters = async (assetName,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName
        })
    console.log(data)
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/getMinters"        
    fetch(url, {
        method: "POST",
        body: data
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while getting minters: ' + e)
        })
}

/*
data:{
                AssetName  string `json:"assetName"`
                NewAdmin   string `json:"newAdmin"`
},
signature (admin)
*/
asset.changeAdmin = async (assetName,assetAdmin,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        newAdmin: assetAdmin,
        })
    console.log(data)
    try {
        if(Window.ePrivateKey == undefined)
        throw new LoginError()
    }
    catch (e){
        console.log(e)
        return
    }
    const signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: helpers.arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/changeAdmin"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while changing asset admin: ' + e)
        })
}
/*
data:{
               AssetName    string   `json:"assetName"`
               Amount       int      `json:"amount"`
},
signature (minter)
*/
asset.burn = async (assetName,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: helpers.arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/burn"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while burning: ' + e)
        })
}

asset.totalSupply  = async (assetName,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName
        })
    console.log(data)
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/totalSupply"        
    fetch(url, {
        method: "POST",
        body: data
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while getting total supply: ' + e)
        })
}
/*
data:{
                AssetName   string `json:"assetName"`
                From        string `json:"from"`
                To          string `json:"to"`
                Amount      int `json:"amount"`
},
signature (From)
*/

asset.transfer = async (assetName,from,to,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        from: from,
        to:to,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: helpers.arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/transfer"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while transfering: ' + e)
        })
}

// data:{
//     AssetName   string `json:"assetName"`
//     From        string `json:"from"`
//     Amount      int `json:"amount"`
// },
// signature (From)

asset.transferToAdmin = async (assetName,from,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        from: from,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: helpers.arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/transferToAdmin"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while transfering to admin: ' + e)
        })
}

// data:{
//     AssetName   string `json:"assetName"`
//     To          string `json:"to"`
//     Amount      int `json:"amount"`
// },
// signature (admin)

asset.transferFromAdmin = async (assetName,to,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        to: to,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: helpers.arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/transferFromAdmin"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while transfering from admin: ' + e)
        })
}



//data:{
//     AssetName    string   `json:"assetName"`
//     Minters      []string `json:"minters"`
// },
//signature (admin)
asset.addMinters = async (assetName,minters,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        minters: minters,
        })
    console.log(data)
    const signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data)) 
    console.log(signature)
    const body = {
        data: data,
        signature: helpers.arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/addMinters"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while adding minters: ' + e)
        })
}

asset.removeMinters = async (assetName,minters,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        minters: minters,
        })
    console.log(data)
    const signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data)) 
    console.log(signature)
    const body = {
        data: data,
        signature: helpers.arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/removeMinters"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while removing minters: ' + e)
        })
}

// // data:{
// //     AssetName    string   `json:"assetName"`
// //      Minter       string   `json:"minter"`
// //      Target       string   `json:"target"`
// //      Amount       int      `json:"amount"`
// // },
// // signature (minter)

asset.mint = async (assetName,minter,target,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        minter: minter,
        target: target,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data))
    console.log(signature)
    const body = {
        data: data,
        signature: helpers.arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/mint"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while minting: ' + e)
        })
}


asset.checkBalance = (assetName,target,nodeIp,nodePort) => {
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/balanceOf"      
    const body = {
        assetName: assetName,
        target: target
    }
    fetch (url, {
        method: "POST",
        body: JSON.stringify(body)
    })
    .then((response) => {
        helpers.handleFetchError(response)
        response.json()
        .then((responseJson=> console.log(responseJson)))
    })
}
asset.checkAdminBalance = (assetName, nodeIp,nodePort) => {
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/balanceOfAdmin"      
    const body = {
        assetName: assetName
    }
    fetch (url, {
        method: "POST",
        body: JSON.stringify(body)
    })
    .then((response) => {
        helpers.handleFetchError(response)
        response.json()
        .then((responseJson=> console.log(responseJson)))
    })
}
export default asset
