import asset from './assets.js';
import helpers from './helpers.js'
import crypt from './crypto.js'
import {PermissionError,HttpError, LoginError} from './errors.js';

(function () {
    class Medblocks {
        constructor (localNodeIP = "127.0.0.1", localNodePort = "8080", localIpfsIp = "127.0.0.1", localIpfsPort = "5001") {
            this.localNodeIP = localNodeIP;
            this.localNodePort = localNodePort;
            this.localIpfsIp = localIpfsIp;
            this.localIpfsPort = localIpfsPort;

            // User data
            this.email;
            this.publicKeys = {
                e: undefined,
                s: undefined
            };
            this.privateKeys = {
                e: undefined,
                s: undefined
            };
        }

        _checkLogin () {
            return (
                this.email != undefined &&
                this.privateKeys.e != undefined &&
                this.privateKeys.s != undefined &&
                this.publicKeys.e != undefined &&
                this.publicKeys.s != undefined
            );
        }

        _post (endpoint, body) {
            let url = `http://${this.nodeIp}:${this.nodePort}/${endpoint}`;
            try {
                return await fetch(url, {
                    method: 'POST',
                    body: JSON.stringify(body)
                })
                .then(helpers.handleFetchError);
            }
            catch (e) {
                throw new Error(e);
            }
        }

        _ipfs (endpoint, ...n) {
            let path, args, body;
            switch (endpoint) {
                case "getBlock":
                    path = `cat`;
                    args = `arg=${n[0]}`;
                    break;
                case "addBlock":
                    path = `add`;
                    body = n[0];
                    break;
            }
            let url = `http://${this.ipfsNodeIp}:${this.ipfsPort}/api/v0/${path}${ (args != undefined) ? (`?=${args}`) : (``) }`;
            try {
                return await fetch(url, {
                    method: 'POST',
                    body: body ? body : undefined
                })
                .then(helpers.handleFetchError);
            }
            catch (e) {
                throw new Error(e);
            }
        }

        async register (email, password) {
            let eKeyPair = await crypt.generate_RSA_Keypair(),
            sKeyPair = await crypt.generateRSASSA_KeyPair();

            // Encrypt the private keys with password
            let encryptedEKey = await crypt.encryptkey(password, await crypt.getPkcs8(eKeyPair.keyPairCrypto.privateKeyCrypto));
            let encryptedSKey = await crypt.encryptkey(password, await crypt.getPkcs8(sKeyPair.keyPairCrypto.privateKeyCrypto));

            // Storing Private Keys
            let privateStore = await this._post('storeKey', {
                emailId: email,
                ePublicKey: await crypt.getPem(eKeyPair.keyPairCrypto.publicKeyCrypto),
                ePrivateKey: helpers.arrayToBase64(encryptedEKey.encryptedKeyBuffer),
                sPublicKey: await crypt.getPem(sKeyPair.keyPairCrypto.publicKeyCrypto),
                sPrivateKey: helpers.arrayToBase64(encryptedSKey.encryptedKeyBuffer),
                IV: {
                    ive: helpers.arrayToBase64(encryptedEKey.iv),
                    ivs: helpers.arrayToBase64(encryptedSKey.iv)
                }
            }).json();

            // Storing Public Keys
            let publicStore;
            try {
                publicStore = await this._post('register', {
                    emailId: email,
                    ePublicKey: await crypt.getPem(eKeyPair.keyPairCrypto.publicKeyCrypto),
                    sPublicKey: await crypt.getPem(sKeyPair.keyPairCrypto.publicKeyCrypto)
                }).json();
            }
            catch (e) {
                console.error('Error while storing public keys:');
                throw new Error(e);
            }

            return publicStore;
        }

        async list (ownerEmailId, permittedEmailId) {
            ownerEmailId = ownerEmailId || "";
            permittedEmailId = permittedEmailId || "";
            
            // Sending List Request
            let list = await this._post('list', {
                ownerEmailId: ownerEmailId,
                permittedEmailId: permittedEmailId
            }).json();

            return list;
        }

        async getPublicKey (emailId) {
            let publicKeyJsonRes = await this._post('getUser', {
                emailId: emailId
            });
            let publicKey;
            try {
                publicKey = await publicKeyJsonRes.json().then(async json => await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(json.ePublicKey))));
            }
            catch (e) {
                console.error('Error while fetching public keys:');
                throw new Error(e);
            }
            return publicKey;
        }

        async login (email, password) {
            // Get Public Keys
            let publicKeyJsonRes = await this._post('getUser', {
                emailId: emailId
            });
            let publicEKey, publicSKey;
            try {
                publicEKey = await publicKeyJsonRes.json().then(async json => await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(json.ePublicKey))));
                publicSKey = await publicKeyJsonRes.json().then(async json => await crypt.getSPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(json.sPublicKey))));
            }
            catch (e) {
                console.error('Error while fetching public keys:');
                throw new Error(e);
            }

            // Get Private Keys
            let privateKeyJsonRes = await this._post('getKey', {
                emailId: emailId
            });
            let privateEKey, privateSKey;
            try {
                privateEKey = await privateKeyJsonRes.json().then(async json => await crypt.getEPrivateKeyFromBuffer(await crypt.decryptKey(password, helpers.base64ToArrayBuffer(json.ePrivateKey), helpers.base64ToArrayBuffer(json.IV.IVE))))
                privateSKey = await privateKeyJsonRes.json().then(async json => await crypt.getSPrivateKeyFromBuffer(await crypt.decryptKey(password, helpers.base64ToArrayBuffer(json.sPrivateKey), helpers.base64ToArrayBuffer(json.IV.IVS))))
            }
            catch (e) {
                console.error('Error while fetching private keys:');
                throw new Error(e);
            }

            // Set user data
            this.email = email;
            this.publicKeys = {
                e: publicEKey,
                s: publicSKey
            };
            this.privateKeys = {
                e: privateEKey,
                s: privateSKey
            };
            
            return undefined;
        }

        async addBlocks (intxs) {
            if (this._checkLogin() === false) {
                throw new LoginError();
            }
            else {
                // Generate AES keys
                let aesKeys = await Promise.all(intx.map(intx => crypt.generate_AES_Key()));
                let ivs = intx.map(intx => window.crypto.getRandomValues(new Uint8Array(16)));
                // Encrypt intxs
                let encrypted_intxs = await Promise.all(intxs.map((intx, i) => {
                    // Get AES key & iv
                    let aesKey = aesKeys[i];
                    let iv = ivs[i];
                    // Encrypt intxs.data
                    return crypt.encryptFileAes(intx.data, aesKey, iv);
                }));
                // Add files to ipfs and return hashes
                let export_files = encrypted_intxs.map((intx, i) => {
                    // create a file
                    return new File(new Blob([intx]), intxs[i].name, {
                        type: intxs[i].type
                    });
                });
                let formData = new FormData();
                export_files.forEach(file => {
                    formData.append('path', file);
                });
                let hashes = await this._ipfs('addBlock', formData);
                hashes = await hashes.text();
                hashes = data.split('\n');
                hashes.pop();
                hashes = hashes.map(t => JSON.parse(t));
                // Encrypt AES keys
                let encryted_keys = await Promise.all(aesKeys.map(async aesKey => crypt.encryptAesKey(await crypt.exportAes(aesKey), await this.getPublicKey(this.email))));
                // Send to addBlock
                let data = JSON.stringify(intxs.map((intx, i) => {
                    return {
                        ipfsHash: hashes[i],
                        name: intx.name,
                        format: intx.type,
                        signatoryEmailId:this.email,
                        ownerEmailId: this.email,
                        permissions: [
                            {
                                receiverEmailId: this.email,
                                receiverKey: helpers.arrayToBase64(encryted_keys[i])
                            }
                        ],
                        IV: helpers.arrayToBase64(ivs[i])
                    };
                }));
                let signature = await crypt.getSignature(helpers.convertStringToArrayBuffer(data));
                return await this._post('addBlock', {
                    data: data,
                    signature: helpers.arrayToBase64(signature)
                }).json();
            }
        }

        async getBlock (hash, raw = false) {
            if (this._checkLogin() === false) {
                throw new LoginError();
            }
            else {
                // Get block data
                let res = await this._post('getBlock', {
                    ipfsHash: hash
                });
                let blockData = await res.json();
    
                if (raw === true) {
                    return blockData;
                }
                else {
                    // Check Permissions
                    let receiverKeyS;
                    for (let i = 0; i < blockData.permissions.length; i++) {
                        if (blockData.permissions[i].receiverEmailId === this.email) {
                            receiverKeyS = blockData.permissions[i].receiverKey;
                            break;
                        }
                    }
                    if (receiverKeyS === undefined) {
                        throw new PermissionError();
                    }
                    else {
                        // Decrypt key
                        let receiverKeyB = helpers.base64ToArrayBuffer(receiverKeyS);
                        let aesKeyBuffer = await crypt.decryptAesKey(receiverKeyB, this.privateKeys.e);
                        let aesKey = await crypt.importAes(aesKeyBuffer);
                        let iv = helpers.base64ToArrayBuffer(blockData.IV);
                        // Get file from ipfs
                        let fileRaw = this._ipfs('getBlock', hash);
                        // Decrypt file
                        let file = fileRaw.arrayBuffer().then(async fb => await crypt.decryptAesKey(fb, aesKey, iv));
                        return file;
                    }
                }
            }
        }

        async addFiles (filelist) {
            if (filelist instanceof FileList) {
                filelist = Array.from(filelist);
            }
            // Make into intxs
            let intxs = await Promise.all(filelist.map(function (file) {
                return new Promise(function (res) {
                    let reader = new FileReader();
                    reader.addEventListener('load', async function (result) {
                        res({
                            data: result,
                            name: file.name,
                            type: file.type
                        });
                    });
                    reader.readAsArrayBuffer(file);
                });
            }));
            return await this.addBlocks(intxs);
        }

        async getFiles (hashlist) {
            return await Promise.all(hashlist.map(async hash => {
                let meta = await this.getBlock(hash, true);
                let data = await this.getBlock(hash);
                return new File(new Blob([data]), meta.name, {
                    type: meta.type
                });
            }));
        }

        async addPermission (hash, to) {
            if (this._checkLogin() === false) {
                throw new LoginError();
            }
            else {
                // Get block data
                let res = await this._post('getBlock', {
                    ipfsHash: hash
                });
                let blockData = await res.json();

                // Check Permissions
                let receiverKeyS;
                for (let i = 0; i < blockData.permissions.length; i++) {
                    if (blockData.permissions[i].receiverEmailId === this.email) {
                        receiverKeyS = blockData.permissions[i].receiverKey;
                        break;
                    }
                }
                if (receiverKeyS === undefined) {
                    throw new PermissionError();
                }
                else {
                    // Decrypt key
                    let receiverKeyB = helpers.base64ToArrayBuffer(receiverKeyS);
                    let aesKeyBuffer = await crypt.decryptAesKey(receiverKeyB, this.privateKeys.e);
                    let aesKey = await crypt.importAes(aesKeyBuffer);
                    let iv = helpers.base64ToArrayBuffer(blockData.IV);
                    let receiverKey = await crypt.encryptAesKey(await crypt.exportAes(aesKey), await this.getPublicKey(to));
                    let data = {
                        ipfsHash: hash,
                        senderEmailId: this.email,
                        permissions: [
                            {
                                receiverEmailId: to,
                                receiverKey: helpers.arrayToBase64(receiverKey)
                            }
                        ]
                    };
                    try {
                        let response = await this._post('addPermission', {
                            data: data,
                            signature: await crypt.getSignature(helpers.convertStringToArrayBuffer(data))
                        }).json();
                        return response;
                    }
                    catch (e) {
                        console.error('Could not add permission:');
                        throw new Error(e);
                    }
                }
            }
        }

        async updateIdentity (hash) {
            if (this._checkLogin() === false) {
                throw new LoginError();
            }
            else {
                let data = {
                    identityFileHash: hash,
                    emailId: this.email
                };
                try {
                    let response = await this._post('updateIdentity', {
                        data: data,
                        signature: await crypt.getSignature(helpers.convertStringToArrayBuffer(data))
                    }).json();
                    return response;
                }
                catch (e) {
                    console.log('Could not update identity:');
                    throw new Error(e);
                }
            }
        }

        async logout () {
            this.email = undefined;
            this.publicKeys = {
                e: undefined,
                s: undefined
            };
            this.privateKeys = {
                e: undefined,
                s: undefined
            };
        }

    }

    window.Medblocks = Medblocks;
});