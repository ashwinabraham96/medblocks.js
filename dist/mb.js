/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/medblocks.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/assets.js":
/*!**************************!*\
  !*** ./src/js/assets.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers.js */ "./src/js/helpers.js");
/* harmony import */ var _crypto_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./crypto.js */ "./src/js/crypto.js");
/* harmony import */ var _errors_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./errors.js */ "./src/js/errors.js");



var asset = {}
// assetAdmin is logged in user
asset.newAsset = async (assetName,assetAdmin,assetMinters,balance,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        assetAdmin: assetAdmin,
        assetMinters: assetMinters,
        balance: parseInt(balance)
        })
    // console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/newAsset"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while creating new asset: ' + e)
        })
}

asset.getMinters = async (assetName,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName
        })
    console.log(data)
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/getMinters"        
    fetch(url, {
        method: "POST",
        body: data
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while getting minters: ' + e)
        })
}

/*
data:{
                AssetName  string `json:"assetName"`
                NewAdmin   string `json:"newAdmin"`
},
signature (admin)
*/
asset.changeAdmin = async (assetName,assetAdmin,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        newAdmin: assetAdmin,
        })
    console.log(data)
    try {
        if(Window.ePrivateKey == undefined)
        throw new _errors_js__WEBPACK_IMPORTED_MODULE_2__["LoginError"]()
    }
    catch (e){
        console.log(e)
        return
    }
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/changeAdmin"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while changing asset admin: ' + e)
        })
}
/*
data:{
               AssetName    string   `json:"assetName"`
               Amount       int      `json:"amount"`
},
signature (minter)
*/
asset.burn = async (assetName,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/burn"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while burning: ' + e)
        })
}

asset.totalSupply  = async (assetName,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName
        })
    console.log(data)
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/totalSupply"        
    fetch(url, {
        method: "POST",
        body: data
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while getting total supply: ' + e)
        })
}
/*
data:{
                AssetName   string `json:"assetName"`
                From        string `json:"from"`
                To          string `json:"to"`
                Amount      int `json:"amount"`
},
signature (From)
*/

asset.transfer = async (assetName,from,to,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        from: from,
        to:to,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/transfer"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while transfering: ' + e)
        })
}

// data:{
//     AssetName   string `json:"assetName"`
//     From        string `json:"from"`
//     Amount      int `json:"amount"`
// },
// signature (From)

asset.transferToAdmin = async (assetName,from,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        from: from,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/transferToAdmin"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while transfering to admin: ' + e)
        })
}

// data:{
//     AssetName   string `json:"assetName"`
//     To          string `json:"to"`
//     Amount      int `json:"amount"`
// },
// signature (admin)

asset.transferFromAdmin = async (assetName,to,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        to: to,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/transferFromAdmin"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while transfering from admin: ' + e)
        })
}



//data:{
//     AssetName    string   `json:"assetName"`
//     Minters      []string `json:"minters"`
// },
//signature (admin)
asset.addMinters = async (assetName,minters,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        minters: minters,
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    console.log(signature)
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/addMinters"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while adding minters: ' + e)
        })
}

asset.removeMinters = async (assetName,minters,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        minters: minters,
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data)) 
    console.log(signature)
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/removeMinters"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while removing minters: ' + e)
        })
}

// // data:{
// //     AssetName    string   `json:"assetName"`
// //      Minter       string   `json:"minter"`
// //      Target       string   `json:"target"`
// //      Amount       int      `json:"amount"`
// // },
// // signature (minter)

asset.mint = async (assetName,minter,target,amount,nodeIp,nodePort) => {
    const data = JSON.stringify({
        assetName: assetName,
        minter: minter,
        target: target,
        amount: parseInt(amount)
        })
    console.log(data)
    const signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_1__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(data))
    console.log(signature)
    const body = {
        data: data,
        signature: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].arrayToBase64(signature)
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/mint"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    console.log(responseJson)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while minting: ' + e)
        })
}


asset.checkBalance = (assetName,target,nodeIp,nodePort) => {
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/balanceOf"      
    const body = {
        assetName: assetName,
        target: target
    }
    fetch (url, {
        method: "POST",
        body: JSON.stringify(body)
    })
    .then((response) => {
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
        response.json()
        .then((responseJson=> console.log(responseJson)))
    })
}
asset.checkAdminBalance = (assetName, nodeIp,nodePort) => {
    const url = "http://" + nodeIp + ":"+ nodePort +"/assets/balanceOfAdmin"      
    const body = {
        assetName: assetName
    }
    fetch (url, {
        method: "POST",
        body: JSON.stringify(body)
    })
    .then((response) => {
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].handleFetchError(response)
        response.json()
        .then((responseJson=> console.log(responseJson)))
    })
}
/* harmony default export */ __webpack_exports__["default"] = (asset);


/***/ }),

/***/ "./src/js/crypto.js":
/*!**************************!*\
  !*** ./src/js/crypto.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers.js */ "./src/js/helpers.js");
/**
    * SECTION: crypto.js
    */

/*
* Contains all necessary cryptographic functions imlemented through webcrypto
*/

var crypt = {}
//const iv = new Uint8Array([154, 188, 93, 65, 87, 138, 37, 254, 240, 78, 10, 105]) 

crypt.generate_RSA_Keypair = async () => {
    let generatedKey = await crypto.subtle.generateKey
        (
            //algorithmIdentifier
            {
                "name": "RSA-OAEP",
                modulusLength: 2048,
                publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                hash:
                {
                    name: 'SHA-256'
                }

            },
            //boolean extractible true so exportKey can be called
            true,
            //keyUsages values set to encrypt,decrypt
            ["encrypt", "decrypt"]
        )
        .then(function (keypair) {
            return keypair
        })
        .catch((e) => { console.error("Error occured during RSA-OEAP key generation Message :" + e) })
    var jsonPrivateKey = await crypto.subtle.exportKey("jwk", generatedKey.privateKey)
    var jsonPublicKey = await crypto.subtle.exportKey("jwk", generatedKey.publicKey)
    return {
        keyPairJson:
        {
            publicKeyJson: jsonPublicKey,//Json web dictionary object
            privateKeyJson: jsonPrivateKey
        },
        keyPairCrypto:
        {
            publicKeyCrypto: generatedKey.publicKey,//CryptoKey object
            privateKeyCrypto: generatedKey.privateKey
        }
    }
}

crypt.generate_RSASSA_KeyPair = async () => {
    var generatedKey = await window.crypto.subtle.generateKey
        (
            {
                name: "RSASSA-PKCS1-v1_5",
                modulusLength: 2048, //can be 1024, 2048, or 4096
                publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
            },
            true, //whether the key is extractable (i.e. can be used in exportKey)
            ["sign", "verify"] //can be any combination of "sign" and "verify"
        )
        .then(function (keypair) {
            return keypair
        })
        .catch(function (e) {
            console.error(e)
        })
    var jsonPrivateKey = await crypto.subtle.exportKey("jwk", generatedKey.privateKey)
    var jsonPublicKey = await crypto.subtle.exportKey("jwk", generatedKey.publicKey)
    return {
        keyPairJson:
        {
            publicKeyJson: jsonPublicKey,//Json web dictionary object
            privateKeyJson: jsonPrivateKey
        },
        keyPairCrypto:
        {
            publicKeyCrypto: generatedKey.publicKey,//CryptoKey object
            privateKeyCrypto: generatedKey.privateKey
        }
    }
}

crypt.generate_AES_Key = async () => {
    var key = await window.crypto.subtle.generateKey(
        {
            name: "AES-GCM",
            length: 256, //can be  128, 192, or 256
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //can "encrypt", "decrypt", "wrapKey", or "unwrapKey"
    )
        .then(function (key) {
            //returns a key object
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    return key
}

crypt.getPkcs8 = async (privateKeyCrypto) => {
    var pk_pkcs8 = await window.crypto.subtle.exportKey(//returns array buffer
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyCrypto //can be a publicKey or privateKey, as long as extractable was true
    )
        .then(function (keydata) {
            //returns the exported key data
            return keydata //arrayBuffer of the private key
        })
        .catch(function (e) {
            console.error(e)
        })
    return pk_pkcs8
}
crypt.getPem = async (publicKeyCrypto) => {
    var spki = await window.crypto.subtle.exportKey(
        "spki", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        publicKeyCrypto //can be a publicKey or privateKey, as long as extractable was true
    )
        .then(function (keydata) {
            //returns the exported key data
            return keydata
        })
        .catch(function (e) {
            console.error(e)
        })
    let pem = _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].spkiToPEM(spki)
    return pem
}
crypt.getPrivateKeyFromBuffer = async (priavteKeyBuffer) => {
    var privateKeyCrypto = await window.crypto.subtle.importKey(
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyBuffer,
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["decrypt"] //"encrypt" or "wrapKey" for public key import or
        //"decrypt" or "unwrapKey" for private key imports
    )
        .then(function (key) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    return privateKeyCrypto
}
crypt.getSPrivateKeyFromBuffer = async (privateKeyBuffer) => {
    var sPrivateKeyCrypto = await window.crypto.subtle.importKey(
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyBuffer,
        {   //these are the algorithm options
            name: "RSASSA-PKCS1-v1_5",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["sign"] //"verify" for public key import, "sign" for private key imports
    )
        .then(function (publicKey) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return publicKey
        })
        .catch(function (e) {
            console.error(e)
        })
    return sPrivateKeyCrypto
}
crypt.getEPrivateKeyFromBuffer = async (privateKeyBuffer) => {
    var privateKeyCrypto = await window.crypto.subtle.importKey(
        "pkcs8", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        privateKeyBuffer,
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["decrypt"] //"encrypt" or "wrapKey" for public key import or
        //"decrypt" or "unwrapKey" for private key imports
    )
        .then(function (key) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return key
        })
        .catch(function (e) {
            console.error("error decrypting from decrypted buffer" + e)
        })
    return privateKeyCrypto
}
crypt.getEPublicKeyFromBuffer = async (publicKeyBuffer) => {
    var publicKeyCrypto = await window.crypto.subtle.importKey(
        "spki", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        publicKeyBuffer,//pass arraybuffer of the public key
        {   //these are the algorithm options
            name: "RSA-OAEP",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt"] //"encrypt" or "wrapKey" for public key import or
        //"decrypt" or "unwrapKey" for private key imports
    )
        .then(function (key) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return key
        })
        .catch((e) => {
            console.error(e, e.stack)
        })
    return publicKeyCrypto
}
crypt.getSPublicKeyFromBuffer = async (publicKeyBuffer) => {
    var ePublicKeyCrypto = await window.crypto.subtle.importKey(
        "spki", //can be "jwk" (public or private), "spki" (public only), or "pkcs8" (private only)
        publicKeyBuffer,
        {   //these are the algorithm options
            name: "RSASSA-PKCS1-v1_5",
            hash: { name: "SHA-256" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["verify"] //"verify" for public key import, "sign" for private key imports
    )
        .then(function (publicKey) {
            //returns a publicKey (or privateKey if you are importing a private key)
            return publicKey
        })
        .catch(function (e) {
            console.error(e)
        })
    return ePublicKeyCrypto
}
crypt.encryptAesKey = async (aesKeyBuffer, publicKeyCrypto) => {
    // pk is privatekey CrytoKey object
    //generate random aes key then encrypt the aes key with user's (owner) public key 
    var encryptedAesKey = await window.crypto.subtle.encrypt(
        {
            name: "RSA-OAEP",
            //label: Uint8Array([...]) //optional
        },
        publicKeyCrypto, //from generateKey or importKey above
        aesKeyBuffer //ArrayBuffer of data you want to encrypt
    )
        .then(function (encrypted) {
            //returns an ArrayBuffer containing the encrypted data
            return encrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return encryptedAesKey//encrypt aes key with public key 
}
crypt.decryptAesKey = async (aesKeyBuffer, privateKeyCrypto) => {
    // pk is privatekey CrytoKey object
    //generate random aes key then encrypt the aes key with user's (owner) public key 
    var decryptedAesKey = await window.crypto.subtle.decrypt(
        {
            name: "RSA-OAEP",
            //label: Uint8Array([...]) //optional
        },
        privateKeyCrypto, //from generateKey or importKey above
        aesKeyBuffer //ArrayBuffer of data you want to decrypt
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the encrypted data
            return decrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return decryptedAesKey//encrypt aes key with public key 
}
crypt.decrypt_AES = async (privateKeyString, password) => {
    var iv = new Uint8Array([154, 188, 93, 65, 87, 138, 37, 254, 240, 78, 10, 105])
    var key = await window.crypto.subtle.importKey(
        "raw", //only "raw" is allowed
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password), //your user's password
        {
            name: "PBKDF2",
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["deriveKey"] //can be any combination of "deriveKey" and "deriveBits"
    )
        .then(function (key) {
            return key
            //return imported key object from password	
        })
        .catch(function (e) {
            console.error(e)
        })
    var enkey = await window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password),
            iterations: 1000,
            hash: { name: "SHA-1" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        key, //your key from generateKey or importKey
        { //the key type you want to create based on the derived bits
            name: "AES-GCM", //can be any AES algorithm ("AES-CTR", "AES-CBC", "AES-CMAC", "AES-GCM", "AES-CFB", "AES-KW", "ECDH", "DH", or "HMAC")
            //the generateKey parameters for that type of algorithm
            length: 128, //can be  128, 192, or 256
        },
        true, //whether the derived key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //limited to the options in that algorithm's importKey
    )
        .then(function (key) {
            //returns the derived key
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    //step 2: use key to decrypt
    var decrypted_data = await window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv, //The initialization vector you used to encrypt
            //additionalData: ArrayBuffer, //The addtionalData you used to encrypt (if any)
            //tagLength: 128, //The tagLength you used to encrypt (if any)
        },
        enkey, //from generateKey or importKey above
        //helpers.convertStringToArrayBuffer(privateKeyString) //ArrayBuffer of the data for dummy data
        privateKeyString// for dummy data as it is already an array buffer
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the decrypted data
            return decrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return decrypted_data
}
crypt.encryptFileAes = async (fileBuffer, aesKey, iv) => {
    let encryptedFileBuffer = await window.crypto.subtle.encrypt(
        {
            name: "AES-GCM",

            //Don't re-use initialization vectors!
            //Always generate a new iv every time your encrypt!
            //Recommended to use 12 bytes length
            //iv: window.crypto.getRandomValues(new Uint8Array(12)),
            iv: iv,
            //Tag length (optional)
            tagLength: 128, //can be 32, 64, 96, 104, 112, 120 or 128 (default)
        },
        aesKey, //from generateKey or importKey above
        fileBuffer //ArrayBuffer of data you want to encrypt
    )
        .then(function (encrypted) {
            //returns an ArrayBuffer containing the encrypted data
            return encrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return encryptedFileBuffer
}
crypt.decryptFileAes = async (fileBuffer, aesKey, iv) => {
    let decryptedFileBuffer = await window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv, //The initialization vector you used to encrypt
            //additionalData: ArrayBuffer, //The addtionalData you used to encrypt (if any)
            tagLength: 128, //The tagLength you used to encrypt (if any)
        },
        aesKey, //from generateKey or importKey above
        fileBuffer //ArrayBuffer of the data
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the decrypted data
            return decrypted
        })
        .catch(function (e) {
            console.error(e)
        })
    return decryptedFileBuffer
}
crypt.getSignature = async (buffer) => {
    // if (Window.sPrivateKey== undefined){
    // 	this.getPrivateKey(emailId)
    // }
    let signature = await window.crypto.subtle.sign(
        {
            name: "RSASSA-PKCS1-v1_5",
        },
        Window.sPrivateKey, //from generateKey or importKey above
        buffer//ArrayBuffer of data you want to sign
    )
        .then(function (signature) {
            //returns an ArrayBuffer containing the signature
            return signature
        })
        .catch(function (e) {
            console.error(e)
        })
    return signature
}
crypt.exportAes = async (aesKey) => {
    let key = await window.crypto.subtle.exportKey(
        "raw", //can be "jwk" or "raw"
        aesKey //extractable must be true
    )
        .then(function (keydata) {
            //returns the exported key data
            return keydata
        })
        .catch(function (e) {
            console.error(e)
        })
    return key
}
crypt.importAes = async (aesKeyBuffer) => {
    let key = await window.crypto.subtle.importKey(
        "raw", //can be "jwk" or "raw"
        aesKeyBuffer,
        {   //this is the algorithm options
            name: "AES-GCM",
        },
        true, //whether the key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //can "encrypt", "decrypt", "wrapKey", or "unwrapKey"
    )
        .then(function (key) {
            //returns the symmetric key
            return key
        })
        .catch(function (e) {
            console.error(e)
        })
    return key
}
crypt.encryptkey = async (password, keyBuffer) => {
    //create Initialization vector for aes key
    let iv = window.crypto.getRandomValues(new Uint8Array(16))
    //create a key from user's password
    let passwordKey = await window.crypto.subtle.importKey(
        "raw",
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password),
        { "name": "PBKDF2" },
        false,
        ["deriveKey"]
    )
    let encryptionKey = await window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password),
            iterations: 1000,
            hash: { name: "SHA-1" }
        },
        passwordKey,
        {
            "name": "AES-GCM",
            "length": 128
        },
        true,
        ["encrypt", "decrypt"]
    )
    let encryptedKeyData = await window.crypto.subtle.encrypt
        ({
            name: "AES-GCM",
            iv: iv
        },
            encryptionKey,
            keyBuffer
        )
    //let encryptedKeyBuffer = new Uint8Array(encryptedKeyData)
    return {
        encryptedKeyBuffer: encryptedKeyData,
        iv: iv
    }
}
crypt.decryptKey = async (password, keyBuffer, iv) => {
    //console.log(iv) 
    //console.log(keyBuffer)
    //console.log(password)
    //returns binary stream of private key
    var passwordKey = await window.crypto.subtle.importKey(
        "raw", //only "raw" is allowed
        _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password), //your user's password
        {
            name: "PBKDF2",
        },
        false, //whether the key is extractable (i.e. can be used in exportKey)
        ["deriveKey"] //can be any combination of "deriveKey" and "deriveBits"
    )
        .then(function (key) {
            return key
            //return imported key object from password	
        })
        .catch(function (e) {
            console.error(e)
        })
    var decryptionKey = await window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            salt: _helpers_js__WEBPACK_IMPORTED_MODULE_0__["default"].convertStringToArrayBuffer(password),
            iterations: 1000,
            hash: { name: "SHA-1" }, //can be "SHA-1", "SHA-256", "SHA-384", or "SHA-512"
        },
        passwordKey, //your key from generateKey or importKey
        { //the key type you want to create based on the derived bits
            name: "AES-GCM", //can be any AES algorithm ("AES-CTR", "AES-CBC", "AES-CMAC", "AES-GCM", "AES-CFB", "AES-KW", "ECDH", "DH", or "HMAC")
            //the generateKey parameters for that type of algorithm
            length: 128, //can be  128, 192, or 256
        },
        true, //whether the derived key is extractable (i.e. can be used in exportKey)
        ["encrypt", "decrypt"] //limited to the options in that algorithm's importKey
    )
        .then(function (key) {
            //returns the derived key
            return key
        })
        .catch(function (e) {
            console.error(e)
        });
    //step 2: use key to decrypt
    var decryptedKeyData = await window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: iv, //The initialization vector you used to encrypt
            //additionalData: ArrayBuffer, //The addtionalData you used to encrypt (if any)
            //tagLength: 128, //The tagLength you used to encrypt (if any)
        },
        decryptionKey, //from generateKey or importKey above
        //this.convertStringToArrayBuffer(privateKeyString) //ArrayBuffer of the data for dummy data
        keyBuffer// for dummy data as it is already an array buffer
    )
        .then(function (decrypted) {
            //returns an ArrayBuffer containing the decrypted data
            //console.log("key decrypted: "+ decrypted)
            return decrypted
        })
        .catch(function (e) {
            console.error("Erroer while decrypting private key: " + e)
        })
    //console.log(decryptedKeyData)
    return decryptedKeyData
}

/* harmony default export */ __webpack_exports__["default"] = (crypt);

/***/ }),

/***/ "./src/js/errors.js":
/*!**************************!*\
  !*** ./src/js/errors.js ***!
  \**************************/
/*! exports provided: HttpError, PermissionError, LoginError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpError", function() { return HttpError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionError", function() { return PermissionError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginError", function() { return LoginError; });
class HttpError extends Error {
    constructor(response) {
        super()
        this.name = 'HttpError'
        this.message = 'Invalid Response status: ' + response.status
    }
}
class PermissionError extends Error {
    constructor() {
        super()
        this.name = 'PermissionError'
        this.message = 'Invalid Permissions for the requested medblock'
    }
}
class LoginError extends Error {
    constructor(){
    super()
    this.name = 'LoginError'
    this.message = 'Please make sure you are logged in to continue.'
    }
}


/***/ }),

/***/ "./src/js/helpers.js":
/*!***************************!*\
  !*** ./src/js/helpers.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _errors_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./errors.js */ "./src/js/errors.js");


var helpers = {}
helpers.base64ToArrayBuffer = (base64) => {
    var binary_string = window.atob(base64)
    var len = binary_string.length
    var bytes = new Uint8Array(len)
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i)
    }
    return bytes.buffer
}

helpers.arrayToBase64 = (array) => {
    return btoa(String.fromCharCode.apply(null, new Uint8Array(array)))
}
helpers.convertStringToArrayBuffer = (str) => { //needed for importkey I/P
    var encoder = new TextEncoder("utf-8")
    return encoder.encode(str)
}
helpers.convertArrayBuffertoString = (buffer) => {
    var decoder = new TextDecoder("utf-8")
    return decoder.decode(buffer)
}
// Calculates hexadecimal string representation of array buffer
helpers.hexString = (buffer) => {
    const byteArray = new Uint8Array(buffer)
    const hexCodes = [...byteArray].map(value => {
        const hexCode = value.toString(16)
        const paddedHexCode = hexCode.padStart(2, '0')
        return paddedHexCode
    })
    return hexCodes.join('')
}

helpers.removeLines = (pem) => {
    var lines = pem.split('\n')
    var encodedString = ''
    for (var i = 1; i < (lines.length - 1); i++) {
        encodedString += lines[i].trim()
    }
    return encodedString
}

helpers.spkiToPEM = (keyBuffer) => {
    // var keydataB64S = helpers.converArrayBuffertoString(keyBuffer) 
    // var keydatab64 = this.b64EncodeUnicode(keydataB64S)
    // var keydataB64Pem = this.formatAsPem(keydataB	64)//add new lines and split into 64
    var keydataB64 = helpers.arrayToBase64(keyBuffer)
    var keydataB64Pem = helpers.formatAsPem(keydataB64)
    return keydataB64Pem
}

helpers.formatAsPem = (str) => {
    var finalString = '-----BEGIN PUBLIC KEY-----\n'

    while (str.length > 0) {
        finalString += str.substring(0, 64) + '\n'
        str = str.substring(64)
    }

    finalString = finalString + "-----END PUBLIC KEY-----"

    return finalString
}
helpers.handleFetchError = (response) => {
    if (response.status !== 200 && response.status !== 202) {
        //console.error("Response Object: ")
        //console.error("Response object: "+JSON.stringify(response))
        throw new _errors_js__WEBPACK_IMPORTED_MODULE_0__["HttpError"](response)
    }
}
/* harmony default export */ __webpack_exports__["default"] = (helpers);

/***/ }),

/***/ "./src/js/medblocks.js":
/*!*****************************!*\
  !*** ./src/js/medblocks.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _assets_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets.js */ "./src/js/assets.js");
/* harmony import */ var _helpers_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers.js */ "./src/js/helpers.js");
/* harmony import */ var _crypto_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./crypto.js */ "./src/js/crypto.js");
/* harmony import */ var _errors_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./errors.js */ "./src/js/errors.js");




(function () {
    /**asset
     * SECTION: medblocks.js
     */
    
    /*
    * Main Medblocks class
    */

    class Medblocks {
        constructor(localNodeIP = "127.0.0.1",localNodePort = "8080",localIpfsIp = "127.0.0.1",localIpfsPort = "5001") {
            this.localNodeIP = localNodeIP, 
            this.localNodePort = localNodePort,
            this.localIpfsIp = localIpfsIp,
            this.localIpfsPort = localIpfsPort
        }
        async addPermission(hash,senderEmailId,receiverEmailId,nodeIp = this.localNodeIP,port = this.localNodePort)
        {
            
            //1: Get Block from blockchain and check if the loggedin user is the owner
            let block = await this.getBlock(hash,nodeIp,port)
            //2:Decrypt the receiverKey to get aes key
            let receiverKeyS
            for (let index=0 ; index < block.permissions.length ; index++)
            {
                if(block.permissions[index].receiverEmailId== senderEmailId){
                    receiverKeyS= block.permissions[index].receiverKey
                }
            }
            try
            {
            if (receiverKeyS==undefined)
            {   
                throw new _errors_js__WEBPACK_IMPORTED_MODULE_3__["PermissionError"]()    
            }
            }
            catch(e) {
                console.log(e)
                console.log(e.message)
                return
            }
            let receiverKeyB = _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(receiverKeyS)
            let aesKeyBuffer = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptAesKey(receiverKeyB,Window.ePrivateKey)
            let aesKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].importAes(aesKeyBuffer)
            //let receiverKey = block.permissions[0]["receiverKey"] works
            let receiverKeyBuffer = (await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptAesKey(await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].exportAes(aesKey), await this.getPublicKey(receiverEmailId,nodeIp,port)))
            const url = "http://"+nodeIp+":"+port+"/addPermission" 
            let data = JSON.stringify({
                ipfsHash: hash,
                senderEmailId: senderEmailId,
                permissions:  [
                    { receiverEmailId: receiverEmailId, receiverKey: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(receiverKeyBuffer) }

                ]
            }) 
            let signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].convertStringToArrayBuffer(data)) 
            const body = {
                data: data,
                signature: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(signature)
            }
            fetch(url, {
                method: 'POST',
                body: JSON.stringify(body)
            })
            .then((response) => _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response))
            .catch( (e) => {
                console.error(e)
                return})

        }
        
        async getPublicKey(emailId,nodeIp = this.localNodeIP,nodePort = this.localNodePort)// for addblock getting public keys of other users
        {
            const url = "http://" + nodeIp + ":"+nodePort+"/getUser" 
            const body = {
                emailId: emailId
            }
            var ePublicKey = await fetch(url, {
                method: "POST",
                body: JSON.stringify(body)
            })
            .then(async (response) => {_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                return await response.json()
                .then(
                    async (responseJson) => {
                        // console.log(await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey))))
                        return await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getEPublicKeyFromBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].removeLines(responseJson.ePublicKey)))
                        //return await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey)))
                    }
                )
            })
            .catch(e => {
                console.error("Error occered while fetching public keys :",e)
                return
            })
            return ePublicKey
            
        }

        async register(email,password,nodeIp=this.localNodeIP,nodePort=this.localNodePort,name,sex)//ePublicKey, sPublicKey , emailid, metadata[Optional]
        // No more password encryption of privatekey
        {   
            console.log(name)
            console.log(sex)
            //var iv="iv"
            //console.log ("User entered password: " + password) 
            var eKeyPair = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].generate_RSA_Keypair() 
            var sKeyPair = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].generate_RSASSA_KeyPair() 
            //console.log("Generated Signature key pair: \n")
            //console.log(sKeyPair)
            //console.log("Generated Encryption key pair: \n")
            //console.log(eKeyPair) 
            //https://stackoverflow.com/questions/957537/how-can-i-display-a-javascript-object
            //console.log(eKeyPair.keyPairCrypto.privateKeyCrypto)
            //console.log(eKeyPair.keyPairCrypto.privateKeyCrypto)
            await this.storePublicKeys(email,eKeyPair.keyPairCrypto.publicKeyCrypto, sKeyPair.keyPairCrypto.publicKeyCrypto,nodeIp,nodePort)
            await this.storeKeys(email,eKeyPair.keyPairCrypto.publicKeyCrypto,sKeyPair.keyPairCrypto.publicKeyCrypto, eKeyPair.keyPairCrypto.privateKeyCrypto, sKeyPair.keyPairCrypto.privateKeyCrypto,password,nodeIp,nodePort) 
            let userIdentityParams = {
                name: name,
                sex: sex,
                emailid:email
            }
            console.log(userIdentityParams)
            await this.login(email,password,nodeIp,nodePort)
            //create a file for user indentity management 
            let userIdentityFile = new File([JSON.stringify(userIdentityParams)],"userIdentityFile",{type: "text/plain"})
            //upload the file to ipfs
            var a = await this.uploadFile(userIdentityFile,email,email,true,nodeIp,nodePort,this.localIpfsIp,this.localIpfsPort)
            // console.log(a)
        
        
        }
        async storePublicKeys(emailId,ePublicKeyCrypto, sPublicKeyCrypto,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//works
        {//Backend registration
            const url = "http://" + nodeIp + ":" + nodePort + "/register" 
            //let encryptedSkey = await crypt.encryptkey(password,await crypt.getPkcs8(sPublicKeyCrypto))
            const body =
            {
                emailId: emailId,
                ePublicKey: await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPem(ePublicKeyCrypto),
                sPublicKey: await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPem(sPublicKeyCrypto)
            } 
            await fetch(url, {
                method: "POST",
                body: JSON.stringify(body)
            })
            .then((response)=>_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response))
            .catch( e => { 
                console.error('Error while storing public keys: '+e)
                return
            })
        }
        async storeKeys(emailId, ePublicKeyCrypto, sPublicKeyCrypto, ePrivateKeyCrypto, sPrivateKeyCrypto,password,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//works
        {
            //console.log(ePrivateKeyCrypto)
            //console.log(sPrivateKeyCrypto)
            //ecnrypt the privatekeys with user's password 
            const url = "http://" + nodeIp + ":" + nodePort + "/storeKey" 
            let encryptedEKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptkey(password,await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPkcs8(ePrivateKeyCrypto))
            let encryptedSkey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptkey(password,await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPkcs8(sPrivateKeyCrypto))
            const body =
            {

                emailId: emailId,
                sPublicKey: await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPem(sPublicKeyCrypto),
                ePublicKey: await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getPem(ePublicKeyCrypto),
                sPrivateKey: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(encryptedSkey.encryptedKeyBuffer),//returns a string
                ePrivateKey: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(encryptedEKey.encryptedKeyBuffer),
                IV:{
                    ive: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(encryptedEKey.iv),
                    ivs: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(encryptedSkey.iv)
                }
            }
            await fetch(url, {
                method: 'POST',
                body: JSON.stringify(body)
            })
            .then((response) => _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response))
            .catch( e=> {
                console.error('Error while stroing private keys :'+e)
                return})
        }
        async login(email,password,nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
            //create session variables for the user
            Window.mbUser = email
            await this.getPrivateKeys(email,password,nodeIp,nodePort)
            await this.getPublicKeys(email,nodeIp,nodePort)
        }
        async getPublicKeys(emailId,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//fetch public key and set session variable
        {
            const url = "http://" + nodeIp + ":"+ nodePort +"/getUser" 
            const body = {
                emailId: emailId
                // sPublicKey: await crypt.getPem(Window.sPublicKey)
            }
            fetch(url, {
                method: "POST",
                body: JSON.stringify(body)
            })
                .then((response) => 
                {   _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                    response.json()
                    .then(async (responseJson) => {
                        Window.ePublicKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getEPublicKeyFromBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].removeLines(responseJson.ePublicKey)))
                        Window.sPublicKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getEPublicKeyFromBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].removeLines(responseJson.sPublicKey)))
                        console.log(Window.ePublicKey)
                        console.log(Window.sPublicKey)
                    })}
                )
                .catch(e => {
                    console.error('Error occured while getting public keys: '+e)
                    return
                })
        }
        async getPrivateKeys(emailId,password,nodeIp = this.localNodeIP,nodePort = this.localNodePort)
        {
            const url = "http://"+nodeIp +":"+nodePort+"/getKey" 
            const body = {
                emailId: emailId
            }
            await fetch(url, {
                method: "POST",
                body: JSON.stringify(body)
            })
                .then(async (response) => {
                    _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                    await response.json()
                    .then(async (responseJson) => {
                        // console.log(await crypt.decryptKey(password,helpers.base64ToArrayBuffer(responseJson.ePrivateKey),helpers.base64ToArrayBuffer(JSON.parse(responseJson.IV).ive)))
                        Window.ePrivateKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getEPrivateKeyFromBuffer(await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptKey(password,_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(responseJson.ePrivateKey),_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(responseJson.IV.IVE)))
                        Window.sPrivateKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getSPrivateKeyFromBuffer(await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptKey(password,_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(responseJson.sPrivateKey),_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(responseJson.IV.IVS)))
                        // Window.ePublicKey = await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey)))
                        // Window.sPublicKey = await crypt.getSPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.sPublicKey)))                        
                        console.log(Window.ePrivateKey)
                        console.log(Window.sPrivateKey)
                        // console.log(Window.sPublicKey)
                        // console.log(Window.ePublicKey)
                        //console.log(helpers.convertArrayBuffertoString(await crypt.decryptKey(password,helpers.base64ToArrayBuffer(responseJson.sPublicKey),helpers.base64ToArrayBuffer(JSON.parse(responseJson.IV).ivsp))))
                    })}
                )
                .catch(e => {
                    console.error('Error occured while getting private keys: '+e)
                    return
                })
        }
        async uploadFile(file, creatorEmailId, ownerEmailId, isIdentity, nodeIp = this.localNodeIP,nodePort = this.localNodePort,ipfsNodeIp = this.localIpfsIp,ipfsPort = this.localIpfsPort) {
            //1:Generate Random Aes Key and its params
            let aesKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].generate_AES_Key()
            let iv = window.crypto.getRandomValues(new Uint8Array(16))
            //2: Get Hash
            const url = "http://" + ipfsNodeIp + ":"+ipfsPort+"/api/v0/block/put" 
            var reader = new FileReader()
            //fetch doesnt work with local files so I used reader 
            reader.addEventListener("load", async (result) => {
                var fileStream = new FormData() 
                //3: Encrypt the file with Aes Key
                var encryptedFile = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptFileAes(reader.result, aesKey, iv)//add iv
                fileStream.append('path', (new Blob([(encryptedFile)])))
                fetch(url, {
                    method: 'POST',
                    body: fileStream,
                })
                    .then(r => {
                        _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(r)
                        var hash = r.json()
                            .then(
                                async (response) => {
                                    console.log(response)
                                    //3: AddBlock 
                                    await this.addBlock(response.Key, file.name, file.type, creatorEmailId, ownerEmailId, aesKey, iv,nodeIp,nodePort)
                                    if (isIdentity==true){
                                        await this.updateIdentity(response.Key, ownerEmailId)
                                    }
                                }
                            )
                    }
                    )
            })
            reader.readAsArrayBuffer(file)
        }
        async uploadFiles(fileList, creatorEmailId, ownerEmailId,nodeIp = this.localNodeIP ,nodePort = this.localNodePort,ipfsNodeIp = this.localIpfsIp,ipfsPort = this.localIpfsPort) {
            //for loop for multiple files
            fileList.forEach(file => this.uploadFile(file, creatorEmailId, ownerEmailId, false,nodeIp,nodePort,ipfsNodeIp,ipfsPort))
        }
        async addBlock(hash, name, format, creatorEmailId, ownerEmailId, aesKey, iv, nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
            
            //let publicReceiverKey = await this.getPublicKey(ownerEmailId,nodeIp,nodePort)
            console.log(await this.getPublicKey(ownerEmailId,nodeIp,nodePort))
            let ownerKeyBuffer = (await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].encryptAesKey(await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].exportAes(aesKey), await this.getPublicKey(ownerEmailId,nodeIp,nodePort)))
            console.log(ownerKeyBuffer)
            const url = "http://" + nodeIp + ":"+nodePort+"/addBlock"
            const data = JSON.stringify([{
                ipfsHash: hash,
                name: name,
                format: format,
                signatoryEmailId: creatorEmailId,
                ownerEmailId: ownerEmailId,
                permissions: [
                    { receiverEmailId: ownerEmailId, receiverKey: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(ownerKeyBuffer) }

                ],
                IV:_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(iv)
            }])
            // Generate Signature
            let signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].convertStringToArrayBuffer(data))
            const blockdata =
            {
                data: data,
                signature: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(signature)
            }
            return fetch(url, {
                method: "POST",
                body: JSON.stringify(blockdata)
            })
                .then((response) => {
                    _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                    return response.json()
                })

        }
        async updateIdentity(hash, ownerEmailId, nodeIp = this.localNodeIP,nodePort = this.localNodePort) {
            
            //let publicReceiverKey = await this.getPublicKey(ownerEmailId,nodeIp,nodePort)
            console.log(await this.getPublicKey(ownerEmailId,nodeIp,nodePort))
            const url = "http://" + nodeIp + ":"+nodePort+"/updateIdentity"
            const data = JSON.stringify({
                identityFileHash: hash,
                emailId: ownerEmailId
            })
            // Generate Signature
            let signature = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].getSignature(_helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].convertStringToArrayBuffer(data))
            const blockdata =
            {
                data: data,
                signature: _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].arrayToBase64(signature)
            }
            return fetch(url, {
                method: "POST",
                body: JSON.stringify(blockdata)
            })
                .then((response) => {
                    _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                    return response.json()
                })

        }

        async getBlock(hash,nodeIp = this.localNodeIP,nodePort = this.localNodePort)//window object error
        {
            const url = "http://"+nodeIp+":"+nodePort+"/getBlock"
            const body = {
                ipfsHash: hash
            }
            var block = await fetch(url, {
                method: "POST",
                body: JSON.stringify(body)
            })
            .then(async (response) => {
                _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                const responseJson = await response.json();
                return responseJson;
            })
            .catch( (e)=> {
                console.error("Error while fetching block from backend :"+e)
                return})
            return block
        }
        async downloadBlock (hash,emailId,nodeIp = this.localNodeIP,nodePort = this.localNodePort,ipfsNodeIp = this.localIpfsIp,ipfsPort = this.localIpfsPort) 
        {
            let blockData = await this.getBlock(hash,nodeIp,nodePort)
            //check permissions
            let receiverKeyS
            for (let index=0; index < blockData.permissions.length; index++)
            {
                if(blockData.permissions[index].receiverEmailId== emailId){
                    receiverKeyS= blockData.permissions[index].receiverKey
                    break
                }
                
            }
            try 
            {
            if (receiverKeyS == undefined){
                throw new _errors_js__WEBPACK_IMPORTED_MODULE_3__["PermissionError"]()
            }}
            catch(e) {
                console.log(e)
                console.log(e.message)
                return
            }
            
            let receiverKeyB = _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(receiverKeyS)
            //login before you call this method
            let aesKeyBuffer = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptAesKey(receiverKeyB,Window.ePrivateKey)
            let aesKey = await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].importAes(aesKeyBuffer)
            //get file buffer and decrypt with aes key
            let iv = _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].base64ToArrayBuffer(blockData.IV)
            const url = "http://"+ipfsNodeIp+":"+ipfsPort+"/api/v0/block/get?arg="+hash
            var file = await fetch( url, {
                method:"POST",
            })
            .then(async (response)=> 
            {//cant convert to json
                _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response)
                return await response.arrayBuffer().then(
                    async (encryptedFileBuffer) => {
                        let file = new File([await _crypto_js__WEBPACK_IMPORTED_MODULE_2__["default"].decryptFileAes(encryptedFileBuffer,aesKey,iv)],blockData.name,{type: blockData.format})
                        this.save(file)
                        return file
                    }
                )
            })	
            .catch((e)=>
            {
                console.error("Error occured while downloading block: "+e)
                return
            })
            
        }
        
        save(file) {
            //var blob = new Blob([data], {type:type }) 
            if(window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(file, file.name) 
            }
            else{
                var elem = window.document.createElement('a') 
                elem.href = window.URL.createObjectURL(file) 
                elem.download = file.name         
                document.body.appendChild(elem) 
                elem.click()         
                document.body.removeChild(elem) 
            }
        }
        
        //Note that, depending on your situation, you may also want to call URL.revokeObjectURL after removing elem. 
        //According to the docs for URL.createObjectURL:
        
        //Each time you call createObjectURL(), a new object URL is created, 
        //even if you've already created one for the same object. Each of these must be released by calling URL.revokeObjectURL() 
        //when you no longer need them. Browsers will release these automatically when the document is unloaded  
        //however, for optimal performance and memory usage, if there are safe times when you can explicitly unload them, 
        //you should do so.

        async listBlock (nodeIp=this.localNodeIP,nodePort=this.localNodePort,ownerEmailId,permittedEmailId)
        {
            const url = "http://"+nodeIp+":"+nodePort+"/list"
            var body = {}
            if (ownerEmailId!=undefined && permittedEmailId!= undefined)
            {
                body.ownerEmailId= arguments[2]
                body.permittedEmailId= arguments[3]
            }
            if (permittedEmailId==undefined)
            {
                body.ownerEmailId= arguments[2]
            }
            if (ownerEmailId==undefined)
            {
                body.permittedEmailId= permittedEmailId
            }
            if (ownerEmailId==undefined && permittedEmailId==undefined)
            {
                body.ownerEmailId= ""
                body.permittedEmailId= ""
            }
            fetch(url, {
                method:"POST",
                body:JSON.stringify(body)
            })
            .then((response) => _helpers_js__WEBPACK_IMPORTED_MODULE_1__["default"].handleFetchError(response))
            .catch(e => {
                console.error(e)
                return})
        }
        async logout ()
        {
            //document.getElementById('current_user').innerHTML= ""
            delete Window.mbUser
            delete Window.ePublicKey
            delete Window.sPublicKey
            delete Window.ePrivateKey
            delete Window.sPrivateKey
        }
        //Asset functions

        async newAsset(assetName,assetAdmin=Window.mbUser,assetMinters,initBalance,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].newAsset(assetName,assetAdmin,assetMinters,initBalance,nodeIp,nodePort)
        }
        async getMinters (assetName,nodeIp=this.localNodeIP,nodePort=this.localNodePort) {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].getMinters(assetName,nodeIp,nodePort)
        }
        async changeAdmin(assetName,assetAdmin=this.mbUser,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].changeAdmin(assetName,assetAdmin,nodeIp,nodePort)
        }
        async burn (assetName,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].burn(assetName,amount,nodeIp,nodePort)
        }
        async totalSupply  (assetName,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].totalSupply(assetName,nodeIp,nodePort)
        }
        async transfer (assetName,from=this.mbUser,to,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].transfer(assetName,from,to,amount,nodeIp,nodePort)
        }
        async transferToAdmin  (assetName,from,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].transferToAdmin(assetName,from,amount,nodeIp,nodePort)
        }
        async transferFromAdmin  (assetName,to,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].transferFromAdmin(assetName,to,amount,nodeIp,nodePort)
        }
        async addMinters (assetName,assetMinters,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].addMinters(assetName,assetMinters,nodeIp,nodePort)
        }
        async removeMinters (assetName,assetMinters,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].removeMinters(assetName,assetMinters,nodeIp,nodePort)
        }
        async mint (assetName,target,amount,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {	
		var minter=Window.mbUser
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].mint(assetName,minter,target,amount,nodeIp,nodePort)
        }
        async checkAdminBalance(assetName,nodeIp=this.localNodeIP,nodePort= this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].checkAdminBalance(assetName,nodeIp,nodePort)
        }
        async checkBalance (assetName,target,nodeIp=this.localNodeIP,nodePort=this.localNodePort)
        {
            _assets_js__WEBPACK_IMPORTED_MODULE_0__["default"].checkBalance(assetName,target,nodeIp,nodePort)
        }

    }
    window.Medblocks = Medblocks 
})() 


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2Fzc2V0cy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvY3J5cHRvLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9lcnJvcnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2hlbHBlcnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL21lZGJsb2Nrcy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFBO0FBQWtDO0FBQ0g7QUFDTztBQUN0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsNEJBQTRCLGtEQUFLLGNBQWMsbURBQU87QUFDdEQ7QUFDQTtBQUNBLG1CQUFtQixtREFBTztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsWUFBWSxtREFBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxZQUFZLG1EQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLHFEQUFVO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsa0RBQUssY0FBYyxtREFBTztBQUN0RDtBQUNBO0FBQ0EsbUJBQW1CLG1EQUFPO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxZQUFZLG1EQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLDRCQUE0QixrREFBSyxjQUFjLG1EQUFPO0FBQ3REO0FBQ0E7QUFDQSxtQkFBbUIsbURBQU87QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFlBQVksbURBQU87QUFDbkI7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsWUFBWSxtREFBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSw0QkFBNEIsa0RBQUssY0FBYyxtREFBTztBQUN0RDtBQUNBO0FBQ0EsbUJBQW1CLG1EQUFPO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxZQUFZLG1EQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsNEJBQTRCLGtEQUFLLGNBQWMsbURBQU87QUFDdEQ7QUFDQTtBQUNBLG1CQUFtQixtREFBTztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsWUFBWSxtREFBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLDRCQUE0QixrREFBSyxjQUFjLG1EQUFPO0FBQ3REO0FBQ0E7QUFDQSxtQkFBbUIsbURBQU87QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFlBQVksbURBQU87QUFDbkI7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSw0QkFBNEIsa0RBQUssY0FBYyxtREFBTztBQUN0RDtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsbURBQU87QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFlBQVksbURBQU87QUFDbkI7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsNEJBQTRCLGtEQUFLLGNBQWMsbURBQU87QUFDdEQ7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLG1EQUFPO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxZQUFZLG1EQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLDRCQUE0QixrREFBSyxjQUFjLG1EQUFPO0FBQ3REO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixtREFBTztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsWUFBWSxtREFBTztBQUNuQjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFFBQVEsbURBQU87QUFDZjtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFFBQVEsbURBQU87QUFDZjtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ2Usb0VBQUs7Ozs7Ozs7Ozs7Ozs7QUM5WnBCO0FBQUE7QUFBQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ2tDO0FBQ2xDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCx1QkFBdUIsOEVBQThFO0FBQ3JHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLGtCQUFrQjtBQUN6QyxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVCxjQUFjLG1EQUFPO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckMsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsbUJBQW1CLGtCQUFrQjtBQUNyQyxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsbUJBQW1CLGtCQUFrQjtBQUNyQyxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxtQkFBbUIsa0JBQWtCO0FBQ3JDLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckMsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxtREFBTztBQUNmO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLG1EQUFPO0FBQ3pCO0FBQ0EsbUJBQW1CLGdCQUFnQjtBQUNuQyxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsbURBQU87QUFDZixTQUFTLG1CQUFtQjtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsbURBQU87QUFDekI7QUFDQSxtQkFBbUI7QUFDbkIsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxtREFBTztBQUNmO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLG1EQUFPO0FBQ3pCO0FBQ0EsbUJBQW1CLGdCQUFnQjtBQUNuQyxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRWUsb0U7Ozs7Ozs7Ozs7OztBQy9oQmY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuQnFDO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsU0FBUztBQUM1QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSwrQ0FBK0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQix3QkFBd0I7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixvREFBUztBQUMzQjtBQUNBO0FBQ2Usc0U7Ozs7Ozs7Ozs7OztBQ3hFZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWdDO0FBQ0U7QUFDSDtBQUN1QjtBQUN0RDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLG1DQUFtQztBQUNqRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYTtBQUNBLDBCQUEwQiwwREFBZTtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixtREFBTztBQUN0QyxxQ0FBcUMsa0RBQUs7QUFDMUMsK0JBQStCLGtEQUFLO0FBQ3BDO0FBQ0EsMkNBQTJDLGtEQUFLLHFCQUFxQixrREFBSztBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLGdEQUFnRCxtREFBTzs7QUFFNUU7QUFDQSxhQUFhO0FBQ2Isa0NBQWtDLGtEQUFLLGNBQWMsbURBQU87QUFDNUQ7QUFDQTtBQUNBLDJCQUEyQixtREFBTztBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixnQ0FBZ0MsbURBQU87QUFDdkM7QUFDQTtBQUNBLHVCQUF1Qjs7QUFFdkI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLHVDQUF1QyxtREFBTztBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQyxrREFBSyx5QkFBeUIsbURBQU8scUJBQXFCLG1EQUFPO0FBQ3RHO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLFM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxrREFBSztBQUN0QyxpQ0FBaUMsa0RBQUs7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFHQUFxRyxtQkFBbUI7QUFDeEg7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0Msa0RBQUs7QUFDdkMsa0NBQWtDLGtEQUFLO0FBQ3ZDLGE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsOEJBQThCLG1EQUFPO0FBQ3JDLDBCO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNDQUFzQyxrREFBSywyQkFBMkIsa0RBQUs7QUFDM0Usc0NBQXNDLGtEQUFLLDJCQUEyQixrREFBSztBQUMzRTtBQUNBOztBQUVBO0FBQ0Esa0NBQWtDLGtEQUFLO0FBQ3ZDLGtDQUFrQyxrREFBSztBQUN2Qyw2QkFBNkIsbURBQU87QUFDcEMsNkJBQTZCLG1EQUFPO0FBQ3BDO0FBQ0EseUJBQXlCLG1EQUFPO0FBQ2hDLHlCQUF5QixtREFBTztBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLGdDQUFnQyxtREFBTztBQUN2QztBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxpQkFBaUIsR0FBRyxtREFBTztBQUMzQjtBQUNBO0FBQ0Esa0RBQWtELGtEQUFLLHlCQUF5QixtREFBTyxxQkFBcUIsbURBQU87QUFDbkgsa0RBQWtELGtEQUFLLHlCQUF5QixtREFBTyxxQkFBcUIsbURBQU87QUFDbkg7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLG9CQUFvQixtREFBTztBQUMzQjtBQUNBO0FBQ0E7QUFDQSxtREFBbUQsa0RBQUssZ0NBQWdDLGtEQUFLLHFCQUFxQixtREFBTywrQ0FBK0MsbURBQU87QUFDL0ssbURBQW1ELGtEQUFLLGdDQUFnQyxrREFBSyxxQkFBcUIsbURBQU8sK0NBQStDLG1EQUFPO0FBQy9LO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixrREFBSztBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMENBQTBDLGtEQUFLO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0Esd0JBQXdCLG1EQUFPO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx3Q0FBd0Msa0RBQUsscUJBQXFCLGtEQUFLO0FBQ3ZFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiw2Q0FBNkMsbURBQU87O0FBRXpFO0FBQ0EsbUJBQW1CLG1EQUFPO0FBQzFCLGFBQWE7QUFDYjtBQUNBLGtDQUFrQyxrREFBSyxjQUFjLG1EQUFPO0FBQzVEO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQixtREFBTztBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLG9CQUFvQixtREFBTztBQUMzQjtBQUNBLGlCQUFpQjs7QUFFakI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxrQ0FBa0Msa0RBQUssY0FBYyxtREFBTztBQUM1RDtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsbURBQU87QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSxvQkFBb0IsbURBQU87QUFDM0I7QUFDQSxpQkFBaUI7O0FBRWpCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLGdCQUFnQixtREFBTztBQUN2QjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsc0NBQXNDO0FBQ25FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsMERBQWU7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtCQUErQixtREFBTztBQUN0QztBQUNBLHFDQUFxQyxrREFBSztBQUMxQywrQkFBK0Isa0RBQUs7QUFDcEM7QUFDQSxxQkFBcUIsbURBQU87QUFDNUI7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsYUFBYTtBQUNiLGdCQUFnQixtREFBTztBQUN2QjtBQUNBO0FBQ0EsbURBQW1ELGtEQUFLLGdFQUFnRSx1QkFBdUI7QUFDL0k7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhOztBQUViOztBQUVBO0FBQ0EsMkNBQTJDLFdBQVc7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixnQ0FBZ0MsbURBQU87QUFDdkM7QUFDQTtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxZQUFZLGtEQUFLO0FBQ2pCO0FBQ0E7QUFDQSxZQUFZLGtEQUFLO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFlBQVksa0RBQUs7QUFDakI7QUFDQTtBQUNBO0FBQ0EsWUFBWSxrREFBSztBQUNqQjtBQUNBO0FBQ0E7QUFDQSxZQUFZLGtEQUFLO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFlBQVksa0RBQUs7QUFDakI7QUFDQTtBQUNBO0FBQ0EsWUFBWSxrREFBSztBQUNqQjtBQUNBO0FBQ0E7QUFDQSxZQUFZLGtEQUFLO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFlBQVksa0RBQUs7QUFDakI7QUFDQTtBQUNBO0FBQ0EsWUFBWSxrREFBSztBQUNqQjtBQUNBO0FBQ0EsUztBQUNBO0FBQ0EsWUFBWSxrREFBSztBQUNqQjtBQUNBO0FBQ0E7QUFDQSxZQUFZLGtEQUFLO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLFlBQVksa0RBQUs7QUFDakI7O0FBRUE7QUFDQTtBQUNBLENBQUMiLCJmaWxlIjoibWIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9qcy9tZWRibG9ja3MuanNcIik7XG4iLCJpbXBvcnQgaGVscGVycyBmcm9tICcuL2hlbHBlcnMuanMnXHJcbmltcG9ydCBjcnlwdCBmcm9tICcuL2NyeXB0by5qcydcclxuaW1wb3J0IHtMb2dpbkVycm9yfSBmcm9tICcuL2Vycm9ycy5qcydcclxudmFyIGFzc2V0ID0ge31cclxuLy8gYXNzZXRBZG1pbiBpcyBsb2dnZWQgaW4gdXNlclxyXG5hc3NldC5uZXdBc3NldCA9IGFzeW5jIChhc3NldE5hbWUsYXNzZXRBZG1pbixhc3NldE1pbnRlcnMsYmFsYW5jZSxub2RlSXAsbm9kZVBvcnQpID0+IHtcclxuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgYXNzZXROYW1lOiBhc3NldE5hbWUsXHJcbiAgICAgICAgYXNzZXRBZG1pbjogYXNzZXRBZG1pbixcclxuICAgICAgICBhc3NldE1pbnRlcnM6IGFzc2V0TWludGVycyxcclxuICAgICAgICBiYWxhbmNlOiBwYXJzZUludChiYWxhbmNlKVxyXG4gICAgICAgIH0pXHJcbiAgICAvLyBjb25zb2xlLmxvZyhkYXRhKVxyXG4gICAgY29uc3Qgc2lnbmF0dXJlID0gYXdhaXQgY3J5cHQuZ2V0U2lnbmF0dXJlKGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIoZGF0YSkpIFxyXG4gICAgY29uc3QgYm9keSA9IHtcclxuICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgIHNpZ25hdHVyZTogaGVscGVycy5hcnJheVRvQmFzZTY0KHNpZ25hdHVyZSlcclxuICAgIH1cclxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvbmV3QXNzZXRcIiAgICAgICAgXHJcbiAgICBmZXRjaCh1cmwsIHtcclxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXHJcbiAgICB9KVxyXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXHJcbiAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlSnNvbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlSnNvbilcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgICAgIClcclxuICAgICAgICAuY2F0Y2goZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIG9jY3VyZWQgd2hpbGUgY3JlYXRpbmcgbmV3IGFzc2V0OiAnICsgZSlcclxuICAgICAgICB9KVxyXG59XHJcblxyXG5hc3NldC5nZXRNaW50ZXJzID0gYXN5bmMgKGFzc2V0TmFtZSxub2RlSXAsbm9kZVBvcnQpID0+IHtcclxuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgYXNzZXROYW1lOiBhc3NldE5hbWVcclxuICAgICAgICB9KVxyXG4gICAgY29uc29sZS5sb2coZGF0YSlcclxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvZ2V0TWludGVyc1wiICAgICAgICBcclxuICAgIGZldGNoKHVybCwge1xyXG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgYm9keTogZGF0YVxyXG4gICAgfSlcclxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxyXG4gICAgICAgICAgICByZXNwb25zZS5qc29uKClcclxuICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUpzb24pXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgICApXHJcbiAgICAgICAgLmNhdGNoKGUgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIGdldHRpbmcgbWludGVyczogJyArIGUpXHJcbiAgICAgICAgfSlcclxufVxyXG5cclxuLypcclxuZGF0YTp7XHJcbiAgICAgICAgICAgICAgICBBc3NldE5hbWUgIHN0cmluZyBganNvbjpcImFzc2V0TmFtZVwiYFxyXG4gICAgICAgICAgICAgICAgTmV3QWRtaW4gICBzdHJpbmcgYGpzb246XCJuZXdBZG1pblwiYFxyXG59LFxyXG5zaWduYXR1cmUgKGFkbWluKVxyXG4qL1xyXG5hc3NldC5jaGFuZ2VBZG1pbiA9IGFzeW5jIChhc3NldE5hbWUsYXNzZXRBZG1pbixub2RlSXAsbm9kZVBvcnQpID0+IHtcclxuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgYXNzZXROYW1lOiBhc3NldE5hbWUsXHJcbiAgICAgICAgbmV3QWRtaW46IGFzc2V0QWRtaW4sXHJcbiAgICAgICAgfSlcclxuICAgIGNvbnNvbGUubG9nKGRhdGEpXHJcbiAgICB0cnkge1xyXG4gICAgICAgIGlmKFdpbmRvdy5lUHJpdmF0ZUtleSA9PSB1bmRlZmluZWQpXHJcbiAgICAgICAgdGhyb3cgbmV3IExvZ2luRXJyb3IoKVxyXG4gICAgfVxyXG4gICAgY2F0Y2ggKGUpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGUpXHJcbiAgICAgICAgcmV0dXJuXHJcbiAgICB9XHJcbiAgICBjb25zdCBzaWduYXR1cmUgPSBhd2FpdCBjcnlwdC5nZXRTaWduYXR1cmUoaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihkYXRhKSkgXHJcbiAgICBjb25zdCBib2R5ID0ge1xyXG4gICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgc2lnbmF0dXJlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoc2lnbmF0dXJlKVxyXG4gICAgfVxyXG4gICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL2Fzc2V0cy9jaGFuZ2VBZG1pblwiICAgICAgICBcclxuICAgIGZldGNoKHVybCwge1xyXG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcclxuICAgIH0pXHJcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcclxuICAgICAgICAgICAgcmVzcG9uc2UuanNvbigpXHJcbiAgICAgICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2VKc29uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2VKc29uKVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICB9XHJcbiAgICAgICAgKVxyXG4gICAgICAgIC5jYXRjaChlID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igb2NjdXJlZCB3aGlsZSBjaGFuZ2luZyBhc3NldCBhZG1pbjogJyArIGUpXHJcbiAgICAgICAgfSlcclxufVxyXG4vKlxyXG5kYXRhOntcclxuICAgICAgICAgICAgICAgQXNzZXROYW1lICAgIHN0cmluZyAgIGBqc29uOlwiYXNzZXROYW1lXCJgXHJcbiAgICAgICAgICAgICAgIEFtb3VudCAgICAgICBpbnQgICAgICBganNvbjpcImFtb3VudFwiYFxyXG59LFxyXG5zaWduYXR1cmUgKG1pbnRlcilcclxuKi9cclxuYXNzZXQuYnVybiA9IGFzeW5jIChhc3NldE5hbWUsYW1vdW50LG5vZGVJcCxub2RlUG9ydCkgPT4ge1xyXG4gICAgY29uc3QgZGF0YSA9IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICBhc3NldE5hbWU6IGFzc2V0TmFtZSxcclxuICAgICAgICBhbW91bnQ6IHBhcnNlSW50KGFtb3VudClcclxuICAgICAgICB9KVxyXG4gICAgY29uc29sZS5sb2coZGF0YSlcclxuICAgIGNvbnN0IHNpZ25hdHVyZSA9IGF3YWl0IGNyeXB0LmdldFNpZ25hdHVyZShoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKGRhdGEpKSBcclxuICAgIGNvbnN0IGJvZHkgPSB7XHJcbiAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICBzaWduYXR1cmU6IGhlbHBlcnMuYXJyYXlUb0Jhc2U2NChzaWduYXR1cmUpXHJcbiAgICB9XHJcbiAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiKyBub2RlUG9ydCArXCIvYXNzZXRzL2J1cm5cIiAgICAgICAgXHJcbiAgICBmZXRjaCh1cmwsIHtcclxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXHJcbiAgICB9KVxyXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXHJcbiAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlSnNvbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlSnNvbilcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgICAgIClcclxuICAgICAgICAuY2F0Y2goZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIG9jY3VyZWQgd2hpbGUgYnVybmluZzogJyArIGUpXHJcbiAgICAgICAgfSlcclxufVxyXG5cclxuYXNzZXQudG90YWxTdXBwbHkgID0gYXN5bmMgKGFzc2V0TmFtZSxub2RlSXAsbm9kZVBvcnQpID0+IHtcclxuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgYXNzZXROYW1lOiBhc3NldE5hbWVcclxuICAgICAgICB9KVxyXG4gICAgY29uc29sZS5sb2coZGF0YSlcclxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvdG90YWxTdXBwbHlcIiAgICAgICAgXHJcbiAgICBmZXRjaCh1cmwsIHtcclxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgIGJvZHk6IGRhdGFcclxuICAgIH0pXHJcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcclxuICAgICAgICAgICAgcmVzcG9uc2UuanNvbigpXHJcbiAgICAgICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2VKc29uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2VKc29uKVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICB9XHJcbiAgICAgICAgKVxyXG4gICAgICAgIC5jYXRjaChlID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igb2NjdXJlZCB3aGlsZSBnZXR0aW5nIHRvdGFsIHN1cHBseTogJyArIGUpXHJcbiAgICAgICAgfSlcclxufVxyXG4vKlxyXG5kYXRhOntcclxuICAgICAgICAgICAgICAgIEFzc2V0TmFtZSAgIHN0cmluZyBganNvbjpcImFzc2V0TmFtZVwiYFxyXG4gICAgICAgICAgICAgICAgRnJvbSAgICAgICAgc3RyaW5nIGBqc29uOlwiZnJvbVwiYFxyXG4gICAgICAgICAgICAgICAgVG8gICAgICAgICAgc3RyaW5nIGBqc29uOlwidG9cImBcclxuICAgICAgICAgICAgICAgIEFtb3VudCAgICAgIGludCBganNvbjpcImFtb3VudFwiYFxyXG59LFxyXG5zaWduYXR1cmUgKEZyb20pXHJcbiovXHJcblxyXG5hc3NldC50cmFuc2ZlciA9IGFzeW5jIChhc3NldE5hbWUsZnJvbSx0byxhbW91bnQsbm9kZUlwLG5vZGVQb3J0KSA9PiB7XHJcbiAgICBjb25zdCBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxyXG4gICAgICAgIGZyb206IGZyb20sXHJcbiAgICAgICAgdG86dG8sXHJcbiAgICAgICAgYW1vdW50OiBwYXJzZUludChhbW91bnQpXHJcbiAgICAgICAgfSlcclxuICAgIGNvbnNvbGUubG9nKGRhdGEpXHJcbiAgICBjb25zdCBzaWduYXR1cmUgPSBhd2FpdCBjcnlwdC5nZXRTaWduYXR1cmUoaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihkYXRhKSkgXHJcbiAgICBjb25zdCBib2R5ID0ge1xyXG4gICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgc2lnbmF0dXJlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoc2lnbmF0dXJlKVxyXG4gICAgfVxyXG4gICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL2Fzc2V0cy90cmFuc2ZlclwiICAgICAgICBcclxuICAgIGZldGNoKHVybCwge1xyXG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcclxuICAgIH0pXHJcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcclxuICAgICAgICAgICAgcmVzcG9uc2UuanNvbigpXHJcbiAgICAgICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2VKc29uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2VKc29uKVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICB9XHJcbiAgICAgICAgKVxyXG4gICAgICAgIC5jYXRjaChlID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igb2NjdXJlZCB3aGlsZSB0cmFuc2ZlcmluZzogJyArIGUpXHJcbiAgICAgICAgfSlcclxufVxyXG5cclxuLy8gZGF0YTp7XHJcbi8vICAgICBBc3NldE5hbWUgICBzdHJpbmcgYGpzb246XCJhc3NldE5hbWVcImBcclxuLy8gICAgIEZyb20gICAgICAgIHN0cmluZyBganNvbjpcImZyb21cImBcclxuLy8gICAgIEFtb3VudCAgICAgIGludCBganNvbjpcImFtb3VudFwiYFxyXG4vLyB9LFxyXG4vLyBzaWduYXR1cmUgKEZyb20pXHJcblxyXG5hc3NldC50cmFuc2ZlclRvQWRtaW4gPSBhc3luYyAoYXNzZXROYW1lLGZyb20sYW1vdW50LG5vZGVJcCxub2RlUG9ydCkgPT4ge1xyXG4gICAgY29uc3QgZGF0YSA9IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICBhc3NldE5hbWU6IGFzc2V0TmFtZSxcclxuICAgICAgICBmcm9tOiBmcm9tLFxyXG4gICAgICAgIGFtb3VudDogcGFyc2VJbnQoYW1vdW50KVxyXG4gICAgICAgIH0pXHJcbiAgICBjb25zb2xlLmxvZyhkYXRhKVxyXG4gICAgY29uc3Qgc2lnbmF0dXJlID0gYXdhaXQgY3J5cHQuZ2V0U2lnbmF0dXJlKGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIoZGF0YSkpIFxyXG4gICAgY29uc3QgYm9keSA9IHtcclxuICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgIHNpZ25hdHVyZTogaGVscGVycy5hcnJheVRvQmFzZTY0KHNpZ25hdHVyZSlcclxuICAgIH1cclxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvdHJhbnNmZXJUb0FkbWluXCIgICAgICAgIFxyXG4gICAgZmV0Y2godXJsLCB7XHJcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxyXG4gICAgfSlcclxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxyXG4gICAgICAgICAgICByZXNwb25zZS5qc29uKClcclxuICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUpzb24pXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgICApXHJcbiAgICAgICAgLmNhdGNoKGUgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIHRyYW5zZmVyaW5nIHRvIGFkbWluOiAnICsgZSlcclxuICAgICAgICB9KVxyXG59XHJcblxyXG4vLyBkYXRhOntcclxuLy8gICAgIEFzc2V0TmFtZSAgIHN0cmluZyBganNvbjpcImFzc2V0TmFtZVwiYFxyXG4vLyAgICAgVG8gICAgICAgICAgc3RyaW5nIGBqc29uOlwidG9cImBcclxuLy8gICAgIEFtb3VudCAgICAgIGludCBganNvbjpcImFtb3VudFwiYFxyXG4vLyB9LFxyXG4vLyBzaWduYXR1cmUgKGFkbWluKVxyXG5cclxuYXNzZXQudHJhbnNmZXJGcm9tQWRtaW4gPSBhc3luYyAoYXNzZXROYW1lLHRvLGFtb3VudCxub2RlSXAsbm9kZVBvcnQpID0+IHtcclxuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgYXNzZXROYW1lOiBhc3NldE5hbWUsXHJcbiAgICAgICAgdG86IHRvLFxyXG4gICAgICAgIGFtb3VudDogcGFyc2VJbnQoYW1vdW50KVxyXG4gICAgICAgIH0pXHJcbiAgICBjb25zb2xlLmxvZyhkYXRhKVxyXG4gICAgY29uc3Qgc2lnbmF0dXJlID0gYXdhaXQgY3J5cHQuZ2V0U2lnbmF0dXJlKGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIoZGF0YSkpIFxyXG4gICAgY29uc3QgYm9keSA9IHtcclxuICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgIHNpZ25hdHVyZTogaGVscGVycy5hcnJheVRvQmFzZTY0KHNpZ25hdHVyZSlcclxuICAgIH1cclxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvdHJhbnNmZXJGcm9tQWRtaW5cIiAgICAgICAgXHJcbiAgICBmZXRjaCh1cmwsIHtcclxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXHJcbiAgICB9KVxyXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXHJcbiAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlSnNvbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlSnNvbilcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgICAgIClcclxuICAgICAgICAuY2F0Y2goZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIG9jY3VyZWQgd2hpbGUgdHJhbnNmZXJpbmcgZnJvbSBhZG1pbjogJyArIGUpXHJcbiAgICAgICAgfSlcclxufVxyXG5cclxuXHJcblxyXG4vL2RhdGE6e1xyXG4vLyAgICAgQXNzZXROYW1lICAgIHN0cmluZyAgIGBqc29uOlwiYXNzZXROYW1lXCJgXHJcbi8vICAgICBNaW50ZXJzICAgICAgW11zdHJpbmcgYGpzb246XCJtaW50ZXJzXCJgXHJcbi8vIH0sXHJcbi8vc2lnbmF0dXJlIChhZG1pbilcclxuYXNzZXQuYWRkTWludGVycyA9IGFzeW5jIChhc3NldE5hbWUsbWludGVycyxub2RlSXAsbm9kZVBvcnQpID0+IHtcclxuICAgIGNvbnN0IGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgYXNzZXROYW1lOiBhc3NldE5hbWUsXHJcbiAgICAgICAgbWludGVyczogbWludGVycyxcclxuICAgICAgICB9KVxyXG4gICAgY29uc29sZS5sb2coZGF0YSlcclxuICAgIGNvbnN0IHNpZ25hdHVyZSA9IGF3YWl0IGNyeXB0LmdldFNpZ25hdHVyZShoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKGRhdGEpKSBcclxuICAgIGNvbnNvbGUubG9nKHNpZ25hdHVyZSlcclxuICAgIGNvbnN0IGJvZHkgPSB7XHJcbiAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICBzaWduYXR1cmU6IGhlbHBlcnMuYXJyYXlUb0Jhc2U2NChzaWduYXR1cmUpXHJcbiAgICB9XHJcbiAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiKyBub2RlUG9ydCArXCIvYXNzZXRzL2FkZE1pbnRlcnNcIiAgICAgICAgXHJcbiAgICBmZXRjaCh1cmwsIHtcclxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXHJcbiAgICB9KVxyXG4gICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXHJcbiAgICAgICAgICAgIHJlc3BvbnNlLmpzb24oKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlSnNvbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlSnNvbilcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgICAgIClcclxuICAgICAgICAuY2F0Y2goZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIG9jY3VyZWQgd2hpbGUgYWRkaW5nIG1pbnRlcnM6ICcgKyBlKVxyXG4gICAgICAgIH0pXHJcbn1cclxuXHJcbmFzc2V0LnJlbW92ZU1pbnRlcnMgPSBhc3luYyAoYXNzZXROYW1lLG1pbnRlcnMsbm9kZUlwLG5vZGVQb3J0KSA9PiB7XHJcbiAgICBjb25zdCBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxyXG4gICAgICAgIG1pbnRlcnM6IG1pbnRlcnMsXHJcbiAgICAgICAgfSlcclxuICAgIGNvbnNvbGUubG9nKGRhdGEpXHJcbiAgICBjb25zdCBzaWduYXR1cmUgPSBhd2FpdCBjcnlwdC5nZXRTaWduYXR1cmUoaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihkYXRhKSkgXHJcbiAgICBjb25zb2xlLmxvZyhzaWduYXR1cmUpXHJcbiAgICBjb25zdCBib2R5ID0ge1xyXG4gICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgc2lnbmF0dXJlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoc2lnbmF0dXJlKVxyXG4gICAgfVxyXG4gICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL2Fzc2V0cy9yZW1vdmVNaW50ZXJzXCIgICAgICAgIFxyXG4gICAgZmV0Y2godXJsLCB7XHJcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxyXG4gICAgfSlcclxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxyXG4gICAgICAgICAgICByZXNwb25zZS5qc29uKClcclxuICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUpzb24pXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgICApXHJcbiAgICAgICAgLmNhdGNoKGUgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIHJlbW92aW5nIG1pbnRlcnM6ICcgKyBlKVxyXG4gICAgICAgIH0pXHJcbn1cclxuXHJcbi8vIC8vIGRhdGE6e1xyXG4vLyAvLyAgICAgQXNzZXROYW1lICAgIHN0cmluZyAgIGBqc29uOlwiYXNzZXROYW1lXCJgXHJcbi8vIC8vICAgICAgTWludGVyICAgICAgIHN0cmluZyAgIGBqc29uOlwibWludGVyXCJgXHJcbi8vIC8vICAgICAgVGFyZ2V0ICAgICAgIHN0cmluZyAgIGBqc29uOlwidGFyZ2V0XCJgXHJcbi8vIC8vICAgICAgQW1vdW50ICAgICAgIGludCAgICAgIGBqc29uOlwiYW1vdW50XCJgXHJcbi8vIC8vIH0sXHJcbi8vIC8vIHNpZ25hdHVyZSAobWludGVyKVxyXG5cclxuYXNzZXQubWludCA9IGFzeW5jIChhc3NldE5hbWUsbWludGVyLHRhcmdldCxhbW91bnQsbm9kZUlwLG5vZGVQb3J0KSA9PiB7XHJcbiAgICBjb25zdCBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgIGFzc2V0TmFtZTogYXNzZXROYW1lLFxyXG4gICAgICAgIG1pbnRlcjogbWludGVyLFxyXG4gICAgICAgIHRhcmdldDogdGFyZ2V0LFxyXG4gICAgICAgIGFtb3VudDogcGFyc2VJbnQoYW1vdW50KVxyXG4gICAgICAgIH0pXHJcbiAgICBjb25zb2xlLmxvZyhkYXRhKVxyXG4gICAgY29uc3Qgc2lnbmF0dXJlID0gYXdhaXQgY3J5cHQuZ2V0U2lnbmF0dXJlKGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIoZGF0YSkpXHJcbiAgICBjb25zb2xlLmxvZyhzaWduYXR1cmUpXHJcbiAgICBjb25zdCBib2R5ID0ge1xyXG4gICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgc2lnbmF0dXJlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoc2lnbmF0dXJlKVxyXG4gICAgfVxyXG4gICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIisgbm9kZVBvcnQgK1wiL2Fzc2V0cy9taW50XCIgICAgICAgIFxyXG4gICAgZmV0Y2godXJsLCB7XHJcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxyXG4gICAgfSlcclxuICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxyXG4gICAgICAgICAgICByZXNwb25zZS5qc29uKClcclxuICAgICAgICAgICAgICAgIC50aGVuKGFzeW5jIChyZXNwb25zZUpzb24pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUpzb24pXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgICApXHJcbiAgICAgICAgLmNhdGNoKGUgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIG1pbnRpbmc6ICcgKyBlKVxyXG4gICAgICAgIH0pXHJcbn1cclxuXHJcblxyXG5hc3NldC5jaGVja0JhbGFuY2UgPSAoYXNzZXROYW1lLHRhcmdldCxub2RlSXAsbm9kZVBvcnQpID0+IHtcclxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvYmFsYW5jZU9mXCIgICAgICBcclxuICAgIGNvbnN0IGJvZHkgPSB7XHJcbiAgICAgICAgYXNzZXROYW1lOiBhc3NldE5hbWUsXHJcbiAgICAgICAgdGFyZ2V0OiB0YXJnZXRcclxuICAgIH1cclxuICAgIGZldGNoICh1cmwsIHtcclxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXHJcbiAgICB9KVxyXG4gICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxyXG4gICAgICAgIHJlc3BvbnNlLmpzb24oKVxyXG4gICAgICAgIC50aGVuKChyZXNwb25zZUpzb249PiBjb25zb2xlLmxvZyhyZXNwb25zZUpzb24pKSlcclxuICAgIH0pXHJcbn1cclxuYXNzZXQuY2hlY2tBZG1pbkJhbGFuY2UgPSAoYXNzZXROYW1lLCBub2RlSXAsbm9kZVBvcnQpID0+IHtcclxuICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrIG5vZGVQb3J0ICtcIi9hc3NldHMvYmFsYW5jZU9mQWRtaW5cIiAgICAgIFxyXG4gICAgY29uc3QgYm9keSA9IHtcclxuICAgICAgICBhc3NldE5hbWU6IGFzc2V0TmFtZVxyXG4gICAgfVxyXG4gICAgZmV0Y2ggKHVybCwge1xyXG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcclxuICAgIH0pXHJcbiAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXHJcbiAgICAgICAgcmVzcG9uc2UuanNvbigpXHJcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlSnNvbj0+IGNvbnNvbGUubG9nKHJlc3BvbnNlSnNvbikpKVxyXG4gICAgfSlcclxufVxyXG5leHBvcnQgZGVmYXVsdCBhc3NldFxyXG4iLCIvKipcclxuICAgICogU0VDVElPTjogY3J5cHRvLmpzXHJcbiAgICAqL1xyXG5cclxuLypcclxuKiBDb250YWlucyBhbGwgbmVjZXNzYXJ5IGNyeXB0b2dyYXBoaWMgZnVuY3Rpb25zIGltbGVtZW50ZWQgdGhyb3VnaCB3ZWJjcnlwdG9cclxuKi9cclxuaW1wb3J0IGhlbHBlcnMgZnJvbSAnLi9oZWxwZXJzLmpzJ1xyXG52YXIgY3J5cHQgPSB7fVxyXG4vL2NvbnN0IGl2ID0gbmV3IFVpbnQ4QXJyYXkoWzE1NCwgMTg4LCA5MywgNjUsIDg3LCAxMzgsIDM3LCAyNTQsIDI0MCwgNzgsIDEwLCAxMDVdKSBcclxuXHJcbmNyeXB0LmdlbmVyYXRlX1JTQV9LZXlwYWlyID0gYXN5bmMgKCkgPT4ge1xyXG4gICAgbGV0IGdlbmVyYXRlZEtleSA9IGF3YWl0IGNyeXB0by5zdWJ0bGUuZ2VuZXJhdGVLZXlcclxuICAgICAgICAoXHJcbiAgICAgICAgICAgIC8vYWxnb3JpdGhtSWRlbnRpZmllclxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJSU0EtT0FFUFwiLFxyXG4gICAgICAgICAgICAgICAgbW9kdWx1c0xlbmd0aDogMjA0OCxcclxuICAgICAgICAgICAgICAgIHB1YmxpY0V4cG9uZW50OiBuZXcgVWludDhBcnJheShbMHgwMSwgMHgwMCwgMHgwMV0pLFxyXG4gICAgICAgICAgICAgICAgaGFzaDpcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnU0hBLTI1NidcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIC8vYm9vbGVhbiBleHRyYWN0aWJsZSB0cnVlIHNvIGV4cG9ydEtleSBjYW4gYmUgY2FsbGVkXHJcbiAgICAgICAgICAgIHRydWUsXHJcbiAgICAgICAgICAgIC8va2V5VXNhZ2VzIHZhbHVlcyBzZXQgdG8gZW5jcnlwdCxkZWNyeXB0XHJcbiAgICAgICAgICAgIFtcImVuY3J5cHRcIiwgXCJkZWNyeXB0XCJdXHJcbiAgICAgICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXlwYWlyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBrZXlwYWlyXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goKGUpID0+IHsgY29uc29sZS5lcnJvcihcIkVycm9yIG9jY3VyZWQgZHVyaW5nIFJTQS1PRUFQIGtleSBnZW5lcmF0aW9uIE1lc3NhZ2UgOlwiICsgZSkgfSlcclxuICAgIHZhciBqc29uUHJpdmF0ZUtleSA9IGF3YWl0IGNyeXB0by5zdWJ0bGUuZXhwb3J0S2V5KFwiandrXCIsIGdlbmVyYXRlZEtleS5wcml2YXRlS2V5KVxyXG4gICAgdmFyIGpzb25QdWJsaWNLZXkgPSBhd2FpdCBjcnlwdG8uc3VidGxlLmV4cG9ydEtleShcImp3a1wiLCBnZW5lcmF0ZWRLZXkucHVibGljS2V5KVxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBrZXlQYWlySnNvbjpcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHB1YmxpY0tleUpzb246IGpzb25QdWJsaWNLZXksLy9Kc29uIHdlYiBkaWN0aW9uYXJ5IG9iamVjdFxyXG4gICAgICAgICAgICBwcml2YXRlS2V5SnNvbjoganNvblByaXZhdGVLZXlcclxuICAgICAgICB9LFxyXG4gICAgICAgIGtleVBhaXJDcnlwdG86XHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBwdWJsaWNLZXlDcnlwdG86IGdlbmVyYXRlZEtleS5wdWJsaWNLZXksLy9DcnlwdG9LZXkgb2JqZWN0XHJcbiAgICAgICAgICAgIHByaXZhdGVLZXlDcnlwdG86IGdlbmVyYXRlZEtleS5wcml2YXRlS2V5XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5jcnlwdC5nZW5lcmF0ZV9SU0FTU0FfS2V5UGFpciA9IGFzeW5jICgpID0+IHtcclxuICAgIHZhciBnZW5lcmF0ZWRLZXkgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5nZW5lcmF0ZUtleVxyXG4gICAgICAgIChcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbmFtZTogXCJSU0FTU0EtUEtDUzEtdjFfNVwiLFxyXG4gICAgICAgICAgICAgICAgbW9kdWx1c0xlbmd0aDogMjA0OCwgLy9jYW4gYmUgMTAyNCwgMjA0OCwgb3IgNDA5NlxyXG4gICAgICAgICAgICAgICAgcHVibGljRXhwb25lbnQ6IG5ldyBVaW50OEFycmF5KFsweDAxLCAweDAwLCAweDAxXSksXHJcbiAgICAgICAgICAgICAgICBoYXNoOiB7IG5hbWU6IFwiU0hBLTI1NlwiIH0sIC8vY2FuIGJlIFwiU0hBLTFcIiwgXCJTSEEtMjU2XCIsIFwiU0hBLTM4NFwiLCBvciBcIlNIQS01MTJcIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB0cnVlLCAvL3doZXRoZXIgdGhlIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXHJcbiAgICAgICAgICAgIFtcInNpZ25cIiwgXCJ2ZXJpZnlcIl0gLy9jYW4gYmUgYW55IGNvbWJpbmF0aW9uIG9mIFwic2lnblwiIGFuZCBcInZlcmlmeVwiXHJcbiAgICAgICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXlwYWlyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBrZXlwYWlyXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxyXG4gICAgICAgIH0pXHJcbiAgICB2YXIganNvblByaXZhdGVLZXkgPSBhd2FpdCBjcnlwdG8uc3VidGxlLmV4cG9ydEtleShcImp3a1wiLCBnZW5lcmF0ZWRLZXkucHJpdmF0ZUtleSlcclxuICAgIHZhciBqc29uUHVibGljS2V5ID0gYXdhaXQgY3J5cHRvLnN1YnRsZS5leHBvcnRLZXkoXCJqd2tcIiwgZ2VuZXJhdGVkS2V5LnB1YmxpY0tleSlcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAga2V5UGFpckpzb246XHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBwdWJsaWNLZXlKc29uOiBqc29uUHVibGljS2V5LC8vSnNvbiB3ZWIgZGljdGlvbmFyeSBvYmplY3RcclxuICAgICAgICAgICAgcHJpdmF0ZUtleUpzb246IGpzb25Qcml2YXRlS2V5XHJcbiAgICAgICAgfSxcclxuICAgICAgICBrZXlQYWlyQ3J5cHRvOlxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcHVibGljS2V5Q3J5cHRvOiBnZW5lcmF0ZWRLZXkucHVibGljS2V5LC8vQ3J5cHRvS2V5IG9iamVjdFxyXG4gICAgICAgICAgICBwcml2YXRlS2V5Q3J5cHRvOiBnZW5lcmF0ZWRLZXkucHJpdmF0ZUtleVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuY3J5cHQuZ2VuZXJhdGVfQUVTX0tleSA9IGFzeW5jICgpID0+IHtcclxuICAgIHZhciBrZXkgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5nZW5lcmF0ZUtleShcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIG5hbWU6IFwiQUVTLUdDTVwiLFxyXG4gICAgICAgICAgICBsZW5ndGg6IDI1NiwgLy9jYW4gYmUgIDEyOCwgMTkyLCBvciAyNTZcclxuICAgICAgICB9LFxyXG4gICAgICAgIHRydWUsIC8vd2hldGhlciB0aGUga2V5IGlzIGV4dHJhY3RhYmxlIChpLmUuIGNhbiBiZSB1c2VkIGluIGV4cG9ydEtleSlcclxuICAgICAgICBbXCJlbmNyeXB0XCIsIFwiZGVjcnlwdFwiXSAvL2NhbiBcImVuY3J5cHRcIiwgXCJkZWNyeXB0XCIsIFwid3JhcEtleVwiLCBvciBcInVud3JhcEtleVwiXHJcbiAgICApXHJcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICAvL3JldHVybnMgYSBrZXkgb2JqZWN0XHJcbiAgICAgICAgICAgIHJldHVybiBrZXlcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIHJldHVybiBrZXlcclxufVxyXG5cclxuY3J5cHQuZ2V0UGtjczggPSBhc3luYyAocHJpdmF0ZUtleUNyeXB0bykgPT4ge1xyXG4gICAgdmFyIHBrX3BrY3M4ID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuZXhwb3J0S2V5KC8vcmV0dXJucyBhcnJheSBidWZmZXJcclxuICAgICAgICBcInBrY3M4XCIsIC8vY2FuIGJlIFwiandrXCIgKHB1YmxpYyBvciBwcml2YXRlKSwgXCJzcGtpXCIgKHB1YmxpYyBvbmx5KSwgb3IgXCJwa2NzOFwiIChwcml2YXRlIG9ubHkpXHJcbiAgICAgICAgcHJpdmF0ZUtleUNyeXB0byAvL2NhbiBiZSBhIHB1YmxpY0tleSBvciBwcml2YXRlS2V5LCBhcyBsb25nIGFzIGV4dHJhY3RhYmxlIHdhcyB0cnVlXHJcbiAgICApXHJcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleWRhdGEpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIHRoZSBleHBvcnRlZCBrZXkgZGF0YVxyXG4gICAgICAgICAgICByZXR1cm4ga2V5ZGF0YSAvL2FycmF5QnVmZmVyIG9mIHRoZSBwcml2YXRlIGtleVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcclxuICAgICAgICB9KVxyXG4gICAgcmV0dXJuIHBrX3BrY3M4XHJcbn1cclxuY3J5cHQuZ2V0UGVtID0gYXN5bmMgKHB1YmxpY0tleUNyeXB0bykgPT4ge1xyXG4gICAgdmFyIHNwa2kgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5leHBvcnRLZXkoXHJcbiAgICAgICAgXCJzcGtpXCIsIC8vY2FuIGJlIFwiandrXCIgKHB1YmxpYyBvciBwcml2YXRlKSwgXCJzcGtpXCIgKHB1YmxpYyBvbmx5KSwgb3IgXCJwa2NzOFwiIChwcml2YXRlIG9ubHkpXHJcbiAgICAgICAgcHVibGljS2V5Q3J5cHRvIC8vY2FuIGJlIGEgcHVibGljS2V5IG9yIHByaXZhdGVLZXksIGFzIGxvbmcgYXMgZXh0cmFjdGFibGUgd2FzIHRydWVcclxuICAgIClcclxuICAgICAgICAudGhlbihmdW5jdGlvbiAoa2V5ZGF0YSkge1xyXG4gICAgICAgICAgICAvL3JldHVybnMgdGhlIGV4cG9ydGVkIGtleSBkYXRhXHJcbiAgICAgICAgICAgIHJldHVybiBrZXlkYXRhXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxyXG4gICAgICAgIH0pXHJcbiAgICBsZXQgcGVtID0gaGVscGVycy5zcGtpVG9QRU0oc3BraSlcclxuICAgIHJldHVybiBwZW1cclxufVxyXG5jcnlwdC5nZXRQcml2YXRlS2V5RnJvbUJ1ZmZlciA9IGFzeW5jIChwcmlhdnRlS2V5QnVmZmVyKSA9PiB7XHJcbiAgICB2YXIgcHJpdmF0ZUtleUNyeXB0byA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcclxuICAgICAgICBcInBrY3M4XCIsIC8vY2FuIGJlIFwiandrXCIgKHB1YmxpYyBvciBwcml2YXRlKSwgXCJzcGtpXCIgKHB1YmxpYyBvbmx5KSwgb3IgXCJwa2NzOFwiIChwcml2YXRlIG9ubHkpXHJcbiAgICAgICAgcHJpdmF0ZUtleUJ1ZmZlcixcclxuICAgICAgICB7ICAgLy90aGVzZSBhcmUgdGhlIGFsZ29yaXRobSBvcHRpb25zXHJcbiAgICAgICAgICAgIG5hbWU6IFwiUlNBLU9BRVBcIixcclxuICAgICAgICAgICAgaGFzaDogeyBuYW1lOiBcIlNIQS0yNTZcIiB9LCAvL2NhbiBiZSBcIlNIQS0xXCIsIFwiU0hBLTI1NlwiLCBcIlNIQS0zODRcIiwgb3IgXCJTSEEtNTEyXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZhbHNlLCAvL3doZXRoZXIgdGhlIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXHJcbiAgICAgICAgW1wiZGVjcnlwdFwiXSAvL1wiZW5jcnlwdFwiIG9yIFwid3JhcEtleVwiIGZvciBwdWJsaWMga2V5IGltcG9ydCBvclxyXG4gICAgICAgIC8vXCJkZWNyeXB0XCIgb3IgXCJ1bndyYXBLZXlcIiBmb3IgcHJpdmF0ZSBrZXkgaW1wb3J0c1xyXG4gICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIGEgcHVibGljS2V5IChvciBwcml2YXRlS2V5IGlmIHlvdSBhcmUgaW1wb3J0aW5nIGEgcHJpdmF0ZSBrZXkpXHJcbiAgICAgICAgICAgIHJldHVybiBrZXlcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIHJldHVybiBwcml2YXRlS2V5Q3J5cHRvXHJcbn1cclxuY3J5cHQuZ2V0U1ByaXZhdGVLZXlGcm9tQnVmZmVyID0gYXN5bmMgKHByaXZhdGVLZXlCdWZmZXIpID0+IHtcclxuICAgIHZhciBzUHJpdmF0ZUtleUNyeXB0byA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcclxuICAgICAgICBcInBrY3M4XCIsIC8vY2FuIGJlIFwiandrXCIgKHB1YmxpYyBvciBwcml2YXRlKSwgXCJzcGtpXCIgKHB1YmxpYyBvbmx5KSwgb3IgXCJwa2NzOFwiIChwcml2YXRlIG9ubHkpXHJcbiAgICAgICAgcHJpdmF0ZUtleUJ1ZmZlcixcclxuICAgICAgICB7ICAgLy90aGVzZSBhcmUgdGhlIGFsZ29yaXRobSBvcHRpb25zXHJcbiAgICAgICAgICAgIG5hbWU6IFwiUlNBU1NBLVBLQ1MxLXYxXzVcIixcclxuICAgICAgICAgICAgaGFzaDogeyBuYW1lOiBcIlNIQS0yNTZcIiB9LCAvL2NhbiBiZSBcIlNIQS0xXCIsIFwiU0hBLTI1NlwiLCBcIlNIQS0zODRcIiwgb3IgXCJTSEEtNTEyXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZhbHNlLCAvL3doZXRoZXIgdGhlIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXHJcbiAgICAgICAgW1wic2lnblwiXSAvL1widmVyaWZ5XCIgZm9yIHB1YmxpYyBrZXkgaW1wb3J0LCBcInNpZ25cIiBmb3IgcHJpdmF0ZSBrZXkgaW1wb3J0c1xyXG4gICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChwdWJsaWNLZXkpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIGEgcHVibGljS2V5IChvciBwcml2YXRlS2V5IGlmIHlvdSBhcmUgaW1wb3J0aW5nIGEgcHJpdmF0ZSBrZXkpXHJcbiAgICAgICAgICAgIHJldHVybiBwdWJsaWNLZXlcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIHJldHVybiBzUHJpdmF0ZUtleUNyeXB0b1xyXG59XHJcbmNyeXB0LmdldEVQcml2YXRlS2V5RnJvbUJ1ZmZlciA9IGFzeW5jIChwcml2YXRlS2V5QnVmZmVyKSA9PiB7XHJcbiAgICB2YXIgcHJpdmF0ZUtleUNyeXB0byA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcclxuICAgICAgICBcInBrY3M4XCIsIC8vY2FuIGJlIFwiandrXCIgKHB1YmxpYyBvciBwcml2YXRlKSwgXCJzcGtpXCIgKHB1YmxpYyBvbmx5KSwgb3IgXCJwa2NzOFwiIChwcml2YXRlIG9ubHkpXHJcbiAgICAgICAgcHJpdmF0ZUtleUJ1ZmZlcixcclxuICAgICAgICB7ICAgLy90aGVzZSBhcmUgdGhlIGFsZ29yaXRobSBvcHRpb25zXHJcbiAgICAgICAgICAgIG5hbWU6IFwiUlNBLU9BRVBcIixcclxuICAgICAgICAgICAgaGFzaDogeyBuYW1lOiBcIlNIQS0yNTZcIiB9LCAvL2NhbiBiZSBcIlNIQS0xXCIsIFwiU0hBLTI1NlwiLCBcIlNIQS0zODRcIiwgb3IgXCJTSEEtNTEyXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHRydWUsIC8vd2hldGhlciB0aGUga2V5IGlzIGV4dHJhY3RhYmxlIChpLmUuIGNhbiBiZSB1c2VkIGluIGV4cG9ydEtleSlcclxuICAgICAgICBbXCJkZWNyeXB0XCJdIC8vXCJlbmNyeXB0XCIgb3IgXCJ3cmFwS2V5XCIgZm9yIHB1YmxpYyBrZXkgaW1wb3J0IG9yXHJcbiAgICAgICAgLy9cImRlY3J5cHRcIiBvciBcInVud3JhcEtleVwiIGZvciBwcml2YXRlIGtleSBpbXBvcnRzXHJcbiAgICApXHJcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICAvL3JldHVybnMgYSBwdWJsaWNLZXkgKG9yIHByaXZhdGVLZXkgaWYgeW91IGFyZSBpbXBvcnRpbmcgYSBwcml2YXRlIGtleSlcclxuICAgICAgICAgICAgcmV0dXJuIGtleVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJlcnJvciBkZWNyeXB0aW5nIGZyb20gZGVjcnlwdGVkIGJ1ZmZlclwiICsgZSlcclxuICAgICAgICB9KVxyXG4gICAgcmV0dXJuIHByaXZhdGVLZXlDcnlwdG9cclxufVxyXG5jcnlwdC5nZXRFUHVibGljS2V5RnJvbUJ1ZmZlciA9IGFzeW5jIChwdWJsaWNLZXlCdWZmZXIpID0+IHtcclxuICAgIHZhciBwdWJsaWNLZXlDcnlwdG8gPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5pbXBvcnRLZXkoXHJcbiAgICAgICAgXCJzcGtpXCIsIC8vY2FuIGJlIFwiandrXCIgKHB1YmxpYyBvciBwcml2YXRlKSwgXCJzcGtpXCIgKHB1YmxpYyBvbmx5KSwgb3IgXCJwa2NzOFwiIChwcml2YXRlIG9ubHkpXHJcbiAgICAgICAgcHVibGljS2V5QnVmZmVyLC8vcGFzcyBhcnJheWJ1ZmZlciBvZiB0aGUgcHVibGljIGtleVxyXG4gICAgICAgIHsgICAvL3RoZXNlIGFyZSB0aGUgYWxnb3JpdGhtIG9wdGlvbnNcclxuICAgICAgICAgICAgbmFtZTogXCJSU0EtT0FFUFwiLFxyXG4gICAgICAgICAgICBoYXNoOiB7IG5hbWU6IFwiU0hBLTI1NlwiIH0sIC8vY2FuIGJlIFwiU0hBLTFcIiwgXCJTSEEtMjU2XCIsIFwiU0hBLTM4NFwiLCBvciBcIlNIQS01MTJcIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdHJ1ZSwgLy93aGV0aGVyIHRoZSBrZXkgaXMgZXh0cmFjdGFibGUgKGkuZS4gY2FuIGJlIHVzZWQgaW4gZXhwb3J0S2V5KVxyXG4gICAgICAgIFtcImVuY3J5cHRcIl0gLy9cImVuY3J5cHRcIiBvciBcIndyYXBLZXlcIiBmb3IgcHVibGljIGtleSBpbXBvcnQgb3JcclxuICAgICAgICAvL1wiZGVjcnlwdFwiIG9yIFwidW53cmFwS2V5XCIgZm9yIHByaXZhdGUga2V5IGltcG9ydHNcclxuICAgIClcclxuICAgICAgICAudGhlbihmdW5jdGlvbiAoa2V5KSB7XHJcbiAgICAgICAgICAgIC8vcmV0dXJucyBhIHB1YmxpY0tleSAob3IgcHJpdmF0ZUtleSBpZiB5b3UgYXJlIGltcG9ydGluZyBhIHByaXZhdGUga2V5KVxyXG4gICAgICAgICAgICByZXR1cm4ga2V5XHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlLCBlLnN0YWNrKVxyXG4gICAgICAgIH0pXHJcbiAgICByZXR1cm4gcHVibGljS2V5Q3J5cHRvXHJcbn1cclxuY3J5cHQuZ2V0U1B1YmxpY0tleUZyb21CdWZmZXIgPSBhc3luYyAocHVibGljS2V5QnVmZmVyKSA9PiB7XHJcbiAgICB2YXIgZVB1YmxpY0tleUNyeXB0byA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcclxuICAgICAgICBcInNwa2lcIiwgLy9jYW4gYmUgXCJqd2tcIiAocHVibGljIG9yIHByaXZhdGUpLCBcInNwa2lcIiAocHVibGljIG9ubHkpLCBvciBcInBrY3M4XCIgKHByaXZhdGUgb25seSlcclxuICAgICAgICBwdWJsaWNLZXlCdWZmZXIsXHJcbiAgICAgICAgeyAgIC8vdGhlc2UgYXJlIHRoZSBhbGdvcml0aG0gb3B0aW9uc1xyXG4gICAgICAgICAgICBuYW1lOiBcIlJTQVNTQS1QS0NTMS12MV81XCIsXHJcbiAgICAgICAgICAgIGhhc2g6IHsgbmFtZTogXCJTSEEtMjU2XCIgfSwgLy9jYW4gYmUgXCJTSEEtMVwiLCBcIlNIQS0yNTZcIiwgXCJTSEEtMzg0XCIsIG9yIFwiU0hBLTUxMlwiXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0cnVlLCAvL3doZXRoZXIgdGhlIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXHJcbiAgICAgICAgW1widmVyaWZ5XCJdIC8vXCJ2ZXJpZnlcIiBmb3IgcHVibGljIGtleSBpbXBvcnQsIFwic2lnblwiIGZvciBwcml2YXRlIGtleSBpbXBvcnRzXHJcbiAgICApXHJcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHB1YmxpY0tleSkge1xyXG4gICAgICAgICAgICAvL3JldHVybnMgYSBwdWJsaWNLZXkgKG9yIHByaXZhdGVLZXkgaWYgeW91IGFyZSBpbXBvcnRpbmcgYSBwcml2YXRlIGtleSlcclxuICAgICAgICAgICAgcmV0dXJuIHB1YmxpY0tleVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcclxuICAgICAgICB9KVxyXG4gICAgcmV0dXJuIGVQdWJsaWNLZXlDcnlwdG9cclxufVxyXG5jcnlwdC5lbmNyeXB0QWVzS2V5ID0gYXN5bmMgKGFlc0tleUJ1ZmZlciwgcHVibGljS2V5Q3J5cHRvKSA9PiB7XHJcbiAgICAvLyBwayBpcyBwcml2YXRla2V5IENyeXRvS2V5IG9iamVjdFxyXG4gICAgLy9nZW5lcmF0ZSByYW5kb20gYWVzIGtleSB0aGVuIGVuY3J5cHQgdGhlIGFlcyBrZXkgd2l0aCB1c2VyJ3MgKG93bmVyKSBwdWJsaWMga2V5IFxyXG4gICAgdmFyIGVuY3J5cHRlZEFlc0tleSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmVuY3J5cHQoXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBuYW1lOiBcIlJTQS1PQUVQXCIsXHJcbiAgICAgICAgICAgIC8vbGFiZWw6IFVpbnQ4QXJyYXkoWy4uLl0pIC8vb3B0aW9uYWxcclxuICAgICAgICB9LFxyXG4gICAgICAgIHB1YmxpY0tleUNyeXB0bywgLy9mcm9tIGdlbmVyYXRlS2V5IG9yIGltcG9ydEtleSBhYm92ZVxyXG4gICAgICAgIGFlc0tleUJ1ZmZlciAvL0FycmF5QnVmZmVyIG9mIGRhdGEgeW91IHdhbnQgdG8gZW5jcnlwdFxyXG4gICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChlbmNyeXB0ZWQpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIGFuIEFycmF5QnVmZmVyIGNvbnRhaW5pbmcgdGhlIGVuY3J5cHRlZCBkYXRhXHJcbiAgICAgICAgICAgIHJldHVybiBlbmNyeXB0ZWRcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIHJldHVybiBlbmNyeXB0ZWRBZXNLZXkvL2VuY3J5cHQgYWVzIGtleSB3aXRoIHB1YmxpYyBrZXkgXHJcbn1cclxuY3J5cHQuZGVjcnlwdEFlc0tleSA9IGFzeW5jIChhZXNLZXlCdWZmZXIsIHByaXZhdGVLZXlDcnlwdG8pID0+IHtcclxuICAgIC8vIHBrIGlzIHByaXZhdGVrZXkgQ3J5dG9LZXkgb2JqZWN0XHJcbiAgICAvL2dlbmVyYXRlIHJhbmRvbSBhZXMga2V5IHRoZW4gZW5jcnlwdCB0aGUgYWVzIGtleSB3aXRoIHVzZXIncyAob3duZXIpIHB1YmxpYyBrZXkgXHJcbiAgICB2YXIgZGVjcnlwdGVkQWVzS2V5ID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuZGVjcnlwdChcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIG5hbWU6IFwiUlNBLU9BRVBcIixcclxuICAgICAgICAgICAgLy9sYWJlbDogVWludDhBcnJheShbLi4uXSkgLy9vcHRpb25hbFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcHJpdmF0ZUtleUNyeXB0bywgLy9mcm9tIGdlbmVyYXRlS2V5IG9yIGltcG9ydEtleSBhYm92ZVxyXG4gICAgICAgIGFlc0tleUJ1ZmZlciAvL0FycmF5QnVmZmVyIG9mIGRhdGEgeW91IHdhbnQgdG8gZGVjcnlwdFxyXG4gICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChkZWNyeXB0ZWQpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIGFuIEFycmF5QnVmZmVyIGNvbnRhaW5pbmcgdGhlIGVuY3J5cHRlZCBkYXRhXHJcbiAgICAgICAgICAgIHJldHVybiBkZWNyeXB0ZWRcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIHJldHVybiBkZWNyeXB0ZWRBZXNLZXkvL2VuY3J5cHQgYWVzIGtleSB3aXRoIHB1YmxpYyBrZXkgXHJcbn1cclxuY3J5cHQuZGVjcnlwdF9BRVMgPSBhc3luYyAocHJpdmF0ZUtleVN0cmluZywgcGFzc3dvcmQpID0+IHtcclxuICAgIHZhciBpdiA9IG5ldyBVaW50OEFycmF5KFsxNTQsIDE4OCwgOTMsIDY1LCA4NywgMTM4LCAzNywgMjU0LCAyNDAsIDc4LCAxMCwgMTA1XSlcclxuICAgIHZhciBrZXkgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5pbXBvcnRLZXkoXHJcbiAgICAgICAgXCJyYXdcIiwgLy9vbmx5IFwicmF3XCIgaXMgYWxsb3dlZFxyXG4gICAgICAgIGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIocGFzc3dvcmQpLCAvL3lvdXIgdXNlcidzIHBhc3N3b3JkXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBuYW1lOiBcIlBCS0RGMlwiLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZmFsc2UsIC8vd2hldGhlciB0aGUga2V5IGlzIGV4dHJhY3RhYmxlIChpLmUuIGNhbiBiZSB1c2VkIGluIGV4cG9ydEtleSlcclxuICAgICAgICBbXCJkZXJpdmVLZXlcIl0gLy9jYW4gYmUgYW55IGNvbWJpbmF0aW9uIG9mIFwiZGVyaXZlS2V5XCIgYW5kIFwiZGVyaXZlQml0c1wiXHJcbiAgICApXHJcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICByZXR1cm4ga2V5XHJcbiAgICAgICAgICAgIC8vcmV0dXJuIGltcG9ydGVkIGtleSBvYmplY3QgZnJvbSBwYXNzd29yZFx0XHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxyXG4gICAgICAgIH0pXHJcbiAgICB2YXIgZW5rZXkgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5kZXJpdmVLZXkoXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBcIm5hbWVcIjogXCJQQktERjJcIixcclxuICAgICAgICAgICAgc2FsdDogaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihwYXNzd29yZCksXHJcbiAgICAgICAgICAgIGl0ZXJhdGlvbnM6IDEwMDAsXHJcbiAgICAgICAgICAgIGhhc2g6IHsgbmFtZTogXCJTSEEtMVwiIH0sIC8vY2FuIGJlIFwiU0hBLTFcIiwgXCJTSEEtMjU2XCIsIFwiU0hBLTM4NFwiLCBvciBcIlNIQS01MTJcIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAga2V5LCAvL3lvdXIga2V5IGZyb20gZ2VuZXJhdGVLZXkgb3IgaW1wb3J0S2V5XHJcbiAgICAgICAgeyAvL3RoZSBrZXkgdHlwZSB5b3Ugd2FudCB0byBjcmVhdGUgYmFzZWQgb24gdGhlIGRlcml2ZWQgYml0c1xyXG4gICAgICAgICAgICBuYW1lOiBcIkFFUy1HQ01cIiwgLy9jYW4gYmUgYW55IEFFUyBhbGdvcml0aG0gKFwiQUVTLUNUUlwiLCBcIkFFUy1DQkNcIiwgXCJBRVMtQ01BQ1wiLCBcIkFFUy1HQ01cIiwgXCJBRVMtQ0ZCXCIsIFwiQUVTLUtXXCIsIFwiRUNESFwiLCBcIkRIXCIsIG9yIFwiSE1BQ1wiKVxyXG4gICAgICAgICAgICAvL3RoZSBnZW5lcmF0ZUtleSBwYXJhbWV0ZXJzIGZvciB0aGF0IHR5cGUgb2YgYWxnb3JpdGhtXHJcbiAgICAgICAgICAgIGxlbmd0aDogMTI4LCAvL2NhbiBiZSAgMTI4LCAxOTIsIG9yIDI1NlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdHJ1ZSwgLy93aGV0aGVyIHRoZSBkZXJpdmVkIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXHJcbiAgICAgICAgW1wiZW5jcnlwdFwiLCBcImRlY3J5cHRcIl0gLy9saW1pdGVkIHRvIHRoZSBvcHRpb25zIGluIHRoYXQgYWxnb3JpdGhtJ3MgaW1wb3J0S2V5XHJcbiAgICApXHJcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICAvL3JldHVybnMgdGhlIGRlcml2ZWQga2V5XHJcbiAgICAgICAgICAgIHJldHVybiBrZXlcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIC8vc3RlcCAyOiB1c2Uga2V5IHRvIGRlY3J5cHRcclxuICAgIHZhciBkZWNyeXB0ZWRfZGF0YSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmRlY3J5cHQoXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBuYW1lOiBcIkFFUy1HQ01cIixcclxuICAgICAgICAgICAgaXY6IGl2LCAvL1RoZSBpbml0aWFsaXphdGlvbiB2ZWN0b3IgeW91IHVzZWQgdG8gZW5jcnlwdFxyXG4gICAgICAgICAgICAvL2FkZGl0aW9uYWxEYXRhOiBBcnJheUJ1ZmZlciwgLy9UaGUgYWRkdGlvbmFsRGF0YSB5b3UgdXNlZCB0byBlbmNyeXB0IChpZiBhbnkpXHJcbiAgICAgICAgICAgIC8vdGFnTGVuZ3RoOiAxMjgsIC8vVGhlIHRhZ0xlbmd0aCB5b3UgdXNlZCB0byBlbmNyeXB0IChpZiBhbnkpXHJcbiAgICAgICAgfSxcclxuICAgICAgICBlbmtleSwgLy9mcm9tIGdlbmVyYXRlS2V5IG9yIGltcG9ydEtleSBhYm92ZVxyXG4gICAgICAgIC8vaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihwcml2YXRlS2V5U3RyaW5nKSAvL0FycmF5QnVmZmVyIG9mIHRoZSBkYXRhIGZvciBkdW1teSBkYXRhXHJcbiAgICAgICAgcHJpdmF0ZUtleVN0cmluZy8vIGZvciBkdW1teSBkYXRhIGFzIGl0IGlzIGFscmVhZHkgYW4gYXJyYXkgYnVmZmVyXHJcbiAgICApXHJcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGRlY3J5cHRlZCkge1xyXG4gICAgICAgICAgICAvL3JldHVybnMgYW4gQXJyYXlCdWZmZXIgY29udGFpbmluZyB0aGUgZGVjcnlwdGVkIGRhdGFcclxuICAgICAgICAgICAgcmV0dXJuIGRlY3J5cHRlZFxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZSlcclxuICAgICAgICB9KVxyXG4gICAgcmV0dXJuIGRlY3J5cHRlZF9kYXRhXHJcbn1cclxuY3J5cHQuZW5jcnlwdEZpbGVBZXMgPSBhc3luYyAoZmlsZUJ1ZmZlciwgYWVzS2V5LCBpdikgPT4ge1xyXG4gICAgbGV0IGVuY3J5cHRlZEZpbGVCdWZmZXIgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5lbmNyeXB0KFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgbmFtZTogXCJBRVMtR0NNXCIsXHJcblxyXG4gICAgICAgICAgICAvL0Rvbid0IHJlLXVzZSBpbml0aWFsaXphdGlvbiB2ZWN0b3JzIVxyXG4gICAgICAgICAgICAvL0Fsd2F5cyBnZW5lcmF0ZSBhIG5ldyBpdiBldmVyeSB0aW1lIHlvdXIgZW5jcnlwdCFcclxuICAgICAgICAgICAgLy9SZWNvbW1lbmRlZCB0byB1c2UgMTIgYnl0ZXMgbGVuZ3RoXHJcbiAgICAgICAgICAgIC8vaXY6IHdpbmRvdy5jcnlwdG8uZ2V0UmFuZG9tVmFsdWVzKG5ldyBVaW50OEFycmF5KDEyKSksXHJcbiAgICAgICAgICAgIGl2OiBpdixcclxuICAgICAgICAgICAgLy9UYWcgbGVuZ3RoIChvcHRpb25hbClcclxuICAgICAgICAgICAgdGFnTGVuZ3RoOiAxMjgsIC8vY2FuIGJlIDMyLCA2NCwgOTYsIDEwNCwgMTEyLCAxMjAgb3IgMTI4IChkZWZhdWx0KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYWVzS2V5LCAvL2Zyb20gZ2VuZXJhdGVLZXkgb3IgaW1wb3J0S2V5IGFib3ZlXHJcbiAgICAgICAgZmlsZUJ1ZmZlciAvL0FycmF5QnVmZmVyIG9mIGRhdGEgeW91IHdhbnQgdG8gZW5jcnlwdFxyXG4gICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChlbmNyeXB0ZWQpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIGFuIEFycmF5QnVmZmVyIGNvbnRhaW5pbmcgdGhlIGVuY3J5cHRlZCBkYXRhXHJcbiAgICAgICAgICAgIHJldHVybiBlbmNyeXB0ZWRcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIHJldHVybiBlbmNyeXB0ZWRGaWxlQnVmZmVyXHJcbn1cclxuY3J5cHQuZGVjcnlwdEZpbGVBZXMgPSBhc3luYyAoZmlsZUJ1ZmZlciwgYWVzS2V5LCBpdikgPT4ge1xyXG4gICAgbGV0IGRlY3J5cHRlZEZpbGVCdWZmZXIgPSBhd2FpdCB3aW5kb3cuY3J5cHRvLnN1YnRsZS5kZWNyeXB0KFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgbmFtZTogXCJBRVMtR0NNXCIsXHJcbiAgICAgICAgICAgIGl2OiBpdiwgLy9UaGUgaW5pdGlhbGl6YXRpb24gdmVjdG9yIHlvdSB1c2VkIHRvIGVuY3J5cHRcclxuICAgICAgICAgICAgLy9hZGRpdGlvbmFsRGF0YTogQXJyYXlCdWZmZXIsIC8vVGhlIGFkZHRpb25hbERhdGEgeW91IHVzZWQgdG8gZW5jcnlwdCAoaWYgYW55KVxyXG4gICAgICAgICAgICB0YWdMZW5ndGg6IDEyOCwgLy9UaGUgdGFnTGVuZ3RoIHlvdSB1c2VkIHRvIGVuY3J5cHQgKGlmIGFueSlcclxuICAgICAgICB9LFxyXG4gICAgICAgIGFlc0tleSwgLy9mcm9tIGdlbmVyYXRlS2V5IG9yIGltcG9ydEtleSBhYm92ZVxyXG4gICAgICAgIGZpbGVCdWZmZXIgLy9BcnJheUJ1ZmZlciBvZiB0aGUgZGF0YVxyXG4gICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChkZWNyeXB0ZWQpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIGFuIEFycmF5QnVmZmVyIGNvbnRhaW5pbmcgdGhlIGRlY3J5cHRlZCBkYXRhXHJcbiAgICAgICAgICAgIHJldHVybiBkZWNyeXB0ZWRcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIHJldHVybiBkZWNyeXB0ZWRGaWxlQnVmZmVyXHJcbn1cclxuY3J5cHQuZ2V0U2lnbmF0dXJlID0gYXN5bmMgKGJ1ZmZlcikgPT4ge1xyXG4gICAgLy8gaWYgKFdpbmRvdy5zUHJpdmF0ZUtleT09IHVuZGVmaW5lZCl7XHJcbiAgICAvLyBcdHRoaXMuZ2V0UHJpdmF0ZUtleShlbWFpbElkKVxyXG4gICAgLy8gfVxyXG4gICAgbGV0IHNpZ25hdHVyZSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLnNpZ24oXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBuYW1lOiBcIlJTQVNTQS1QS0NTMS12MV81XCIsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBXaW5kb3cuc1ByaXZhdGVLZXksIC8vZnJvbSBnZW5lcmF0ZUtleSBvciBpbXBvcnRLZXkgYWJvdmVcclxuICAgICAgICBidWZmZXIvL0FycmF5QnVmZmVyIG9mIGRhdGEgeW91IHdhbnQgdG8gc2lnblxyXG4gICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChzaWduYXR1cmUpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIGFuIEFycmF5QnVmZmVyIGNvbnRhaW5pbmcgdGhlIHNpZ25hdHVyZVxyXG4gICAgICAgICAgICByZXR1cm4gc2lnbmF0dXJlXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxyXG4gICAgICAgIH0pXHJcbiAgICByZXR1cm4gc2lnbmF0dXJlXHJcbn1cclxuY3J5cHQuZXhwb3J0QWVzID0gYXN5bmMgKGFlc0tleSkgPT4ge1xyXG4gICAgbGV0IGtleSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmV4cG9ydEtleShcclxuICAgICAgICBcInJhd1wiLCAvL2NhbiBiZSBcImp3a1wiIG9yIFwicmF3XCJcclxuICAgICAgICBhZXNLZXkgLy9leHRyYWN0YWJsZSBtdXN0IGJlIHRydWVcclxuICAgIClcclxuICAgICAgICAudGhlbihmdW5jdGlvbiAoa2V5ZGF0YSkge1xyXG4gICAgICAgICAgICAvL3JldHVybnMgdGhlIGV4cG9ydGVkIGtleSBkYXRhXHJcbiAgICAgICAgICAgIHJldHVybiBrZXlkYXRhXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKVxyXG4gICAgICAgIH0pXHJcbiAgICByZXR1cm4ga2V5XHJcbn1cclxuY3J5cHQuaW1wb3J0QWVzID0gYXN5bmMgKGFlc0tleUJ1ZmZlcikgPT4ge1xyXG4gICAgbGV0IGtleSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcclxuICAgICAgICBcInJhd1wiLCAvL2NhbiBiZSBcImp3a1wiIG9yIFwicmF3XCJcclxuICAgICAgICBhZXNLZXlCdWZmZXIsXHJcbiAgICAgICAgeyAgIC8vdGhpcyBpcyB0aGUgYWxnb3JpdGhtIG9wdGlvbnNcclxuICAgICAgICAgICAgbmFtZTogXCJBRVMtR0NNXCIsXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0cnVlLCAvL3doZXRoZXIgdGhlIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXHJcbiAgICAgICAgW1wiZW5jcnlwdFwiLCBcImRlY3J5cHRcIl0gLy9jYW4gXCJlbmNyeXB0XCIsIFwiZGVjcnlwdFwiLCBcIndyYXBLZXlcIiwgb3IgXCJ1bndyYXBLZXlcIlxyXG4gICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIHRoZSBzeW1tZXRyaWMga2V5XHJcbiAgICAgICAgICAgIHJldHVybiBrZXlcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIHJldHVybiBrZXlcclxufVxyXG5jcnlwdC5lbmNyeXB0a2V5ID0gYXN5bmMgKHBhc3N3b3JkLCBrZXlCdWZmZXIpID0+IHtcclxuICAgIC8vY3JlYXRlIEluaXRpYWxpemF0aW9uIHZlY3RvciBmb3IgYWVzIGtleVxyXG4gICAgbGV0IGl2ID0gd2luZG93LmNyeXB0by5nZXRSYW5kb21WYWx1ZXMobmV3IFVpbnQ4QXJyYXkoMTYpKVxyXG4gICAgLy9jcmVhdGUgYSBrZXkgZnJvbSB1c2VyJ3MgcGFzc3dvcmRcclxuICAgIGxldCBwYXNzd29yZEtleSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcclxuICAgICAgICBcInJhd1wiLFxyXG4gICAgICAgIGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIocGFzc3dvcmQpLFxyXG4gICAgICAgIHsgXCJuYW1lXCI6IFwiUEJLREYyXCIgfSxcclxuICAgICAgICBmYWxzZSxcclxuICAgICAgICBbXCJkZXJpdmVLZXlcIl1cclxuICAgIClcclxuICAgIGxldCBlbmNyeXB0aW9uS2V5ID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuZGVyaXZlS2V5KFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiUEJLREYyXCIsXHJcbiAgICAgICAgICAgIHNhbHQ6IGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIocGFzc3dvcmQpLFxyXG4gICAgICAgICAgICBpdGVyYXRpb25zOiAxMDAwLFxyXG4gICAgICAgICAgICBoYXNoOiB7IG5hbWU6IFwiU0hBLTFcIiB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBwYXNzd29yZEtleSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIkFFUy1HQ01cIixcclxuICAgICAgICAgICAgXCJsZW5ndGhcIjogMTI4XHJcbiAgICAgICAgfSxcclxuICAgICAgICB0cnVlLFxyXG4gICAgICAgIFtcImVuY3J5cHRcIiwgXCJkZWNyeXB0XCJdXHJcbiAgICApXHJcbiAgICBsZXQgZW5jcnlwdGVkS2V5RGF0YSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmVuY3J5cHRcclxuICAgICAgICAoe1xyXG4gICAgICAgICAgICBuYW1lOiBcIkFFUy1HQ01cIixcclxuICAgICAgICAgICAgaXY6IGl2XHJcbiAgICAgICAgfSxcclxuICAgICAgICAgICAgZW5jcnlwdGlvbktleSxcclxuICAgICAgICAgICAga2V5QnVmZmVyXHJcbiAgICAgICAgKVxyXG4gICAgLy9sZXQgZW5jcnlwdGVkS2V5QnVmZmVyID0gbmV3IFVpbnQ4QXJyYXkoZW5jcnlwdGVkS2V5RGF0YSlcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgZW5jcnlwdGVkS2V5QnVmZmVyOiBlbmNyeXB0ZWRLZXlEYXRhLFxyXG4gICAgICAgIGl2OiBpdlxyXG4gICAgfVxyXG59XHJcbmNyeXB0LmRlY3J5cHRLZXkgPSBhc3luYyAocGFzc3dvcmQsIGtleUJ1ZmZlciwgaXYpID0+IHtcclxuICAgIC8vY29uc29sZS5sb2coaXYpIFxyXG4gICAgLy9jb25zb2xlLmxvZyhrZXlCdWZmZXIpXHJcbiAgICAvL2NvbnNvbGUubG9nKHBhc3N3b3JkKVxyXG4gICAgLy9yZXR1cm5zIGJpbmFyeSBzdHJlYW0gb2YgcHJpdmF0ZSBrZXlcclxuICAgIHZhciBwYXNzd29yZEtleSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmltcG9ydEtleShcclxuICAgICAgICBcInJhd1wiLCAvL29ubHkgXCJyYXdcIiBpcyBhbGxvd2VkXHJcbiAgICAgICAgaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihwYXNzd29yZCksIC8veW91ciB1c2VyJ3MgcGFzc3dvcmRcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIG5hbWU6IFwiUEJLREYyXCIsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmYWxzZSwgLy93aGV0aGVyIHRoZSBrZXkgaXMgZXh0cmFjdGFibGUgKGkuZS4gY2FuIGJlIHVzZWQgaW4gZXhwb3J0S2V5KVxyXG4gICAgICAgIFtcImRlcml2ZUtleVwiXSAvL2NhbiBiZSBhbnkgY29tYmluYXRpb24gb2YgXCJkZXJpdmVLZXlcIiBhbmQgXCJkZXJpdmVCaXRzXCJcclxuICAgIClcclxuICAgICAgICAudGhlbihmdW5jdGlvbiAoa2V5KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBrZXlcclxuICAgICAgICAgICAgLy9yZXR1cm4gaW1wb3J0ZWQga2V5IG9iamVjdCBmcm9tIHBhc3N3b3JkXHRcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSlcclxuICAgIHZhciBkZWNyeXB0aW9uS2V5ID0gYXdhaXQgd2luZG93LmNyeXB0by5zdWJ0bGUuZGVyaXZlS2V5KFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgXCJuYW1lXCI6IFwiUEJLREYyXCIsXHJcbiAgICAgICAgICAgIHNhbHQ6IGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIocGFzc3dvcmQpLFxyXG4gICAgICAgICAgICBpdGVyYXRpb25zOiAxMDAwLFxyXG4gICAgICAgICAgICBoYXNoOiB7IG5hbWU6IFwiU0hBLTFcIiB9LCAvL2NhbiBiZSBcIlNIQS0xXCIsIFwiU0hBLTI1NlwiLCBcIlNIQS0zODRcIiwgb3IgXCJTSEEtNTEyXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHBhc3N3b3JkS2V5LCAvL3lvdXIga2V5IGZyb20gZ2VuZXJhdGVLZXkgb3IgaW1wb3J0S2V5XHJcbiAgICAgICAgeyAvL3RoZSBrZXkgdHlwZSB5b3Ugd2FudCB0byBjcmVhdGUgYmFzZWQgb24gdGhlIGRlcml2ZWQgYml0c1xyXG4gICAgICAgICAgICBuYW1lOiBcIkFFUy1HQ01cIiwgLy9jYW4gYmUgYW55IEFFUyBhbGdvcml0aG0gKFwiQUVTLUNUUlwiLCBcIkFFUy1DQkNcIiwgXCJBRVMtQ01BQ1wiLCBcIkFFUy1HQ01cIiwgXCJBRVMtQ0ZCXCIsIFwiQUVTLUtXXCIsIFwiRUNESFwiLCBcIkRIXCIsIG9yIFwiSE1BQ1wiKVxyXG4gICAgICAgICAgICAvL3RoZSBnZW5lcmF0ZUtleSBwYXJhbWV0ZXJzIGZvciB0aGF0IHR5cGUgb2YgYWxnb3JpdGhtXHJcbiAgICAgICAgICAgIGxlbmd0aDogMTI4LCAvL2NhbiBiZSAgMTI4LCAxOTIsIG9yIDI1NlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdHJ1ZSwgLy93aGV0aGVyIHRoZSBkZXJpdmVkIGtleSBpcyBleHRyYWN0YWJsZSAoaS5lLiBjYW4gYmUgdXNlZCBpbiBleHBvcnRLZXkpXHJcbiAgICAgICAgW1wiZW5jcnlwdFwiLCBcImRlY3J5cHRcIl0gLy9saW1pdGVkIHRvIHRoZSBvcHRpb25zIGluIHRoYXQgYWxnb3JpdGhtJ3MgaW1wb3J0S2V5XHJcbiAgICApXHJcbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICAvL3JldHVybnMgdGhlIGRlcml2ZWQga2V5XHJcbiAgICAgICAgICAgIHJldHVybiBrZXlcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgfSk7XHJcbiAgICAvL3N0ZXAgMjogdXNlIGtleSB0byBkZWNyeXB0XHJcbiAgICB2YXIgZGVjcnlwdGVkS2V5RGF0YSA9IGF3YWl0IHdpbmRvdy5jcnlwdG8uc3VidGxlLmRlY3J5cHQoXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBuYW1lOiBcIkFFUy1HQ01cIixcclxuICAgICAgICAgICAgaXY6IGl2LCAvL1RoZSBpbml0aWFsaXphdGlvbiB2ZWN0b3IgeW91IHVzZWQgdG8gZW5jcnlwdFxyXG4gICAgICAgICAgICAvL2FkZGl0aW9uYWxEYXRhOiBBcnJheUJ1ZmZlciwgLy9UaGUgYWRkdGlvbmFsRGF0YSB5b3UgdXNlZCB0byBlbmNyeXB0IChpZiBhbnkpXHJcbiAgICAgICAgICAgIC8vdGFnTGVuZ3RoOiAxMjgsIC8vVGhlIHRhZ0xlbmd0aCB5b3UgdXNlZCB0byBlbmNyeXB0IChpZiBhbnkpXHJcbiAgICAgICAgfSxcclxuICAgICAgICBkZWNyeXB0aW9uS2V5LCAvL2Zyb20gZ2VuZXJhdGVLZXkgb3IgaW1wb3J0S2V5IGFib3ZlXHJcbiAgICAgICAgLy90aGlzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKHByaXZhdGVLZXlTdHJpbmcpIC8vQXJyYXlCdWZmZXIgb2YgdGhlIGRhdGEgZm9yIGR1bW15IGRhdGFcclxuICAgICAgICBrZXlCdWZmZXIvLyBmb3IgZHVtbXkgZGF0YSBhcyBpdCBpcyBhbHJlYWR5IGFuIGFycmF5IGJ1ZmZlclxyXG4gICAgKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChkZWNyeXB0ZWQpIHtcclxuICAgICAgICAgICAgLy9yZXR1cm5zIGFuIEFycmF5QnVmZmVyIGNvbnRhaW5pbmcgdGhlIGRlY3J5cHRlZCBkYXRhXHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJrZXkgZGVjcnlwdGVkOiBcIisgZGVjcnlwdGVkKVxyXG4gICAgICAgICAgICByZXR1cm4gZGVjcnlwdGVkXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkVycm9lciB3aGlsZSBkZWNyeXB0aW5nIHByaXZhdGUga2V5OiBcIiArIGUpXHJcbiAgICAgICAgfSlcclxuICAgIC8vY29uc29sZS5sb2coZGVjcnlwdGVkS2V5RGF0YSlcclxuICAgIHJldHVybiBkZWNyeXB0ZWRLZXlEYXRhXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNyeXB0IiwiY2xhc3MgSHR0cEVycm9yIGV4dGVuZHMgRXJyb3Ige1xyXG4gICAgY29uc3RydWN0b3IocmVzcG9uc2UpIHtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy5uYW1lID0gJ0h0dHBFcnJvcidcclxuICAgICAgICB0aGlzLm1lc3NhZ2UgPSAnSW52YWxpZCBSZXNwb25zZSBzdGF0dXM6ICcgKyByZXNwb25zZS5zdGF0dXNcclxuICAgIH1cclxufVxyXG5jbGFzcyBQZXJtaXNzaW9uRXJyb3IgZXh0ZW5kcyBFcnJvciB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy5uYW1lID0gJ1Blcm1pc3Npb25FcnJvcidcclxuICAgICAgICB0aGlzLm1lc3NhZ2UgPSAnSW52YWxpZCBQZXJtaXNzaW9ucyBmb3IgdGhlIHJlcXVlc3RlZCBtZWRibG9jaydcclxuICAgIH1cclxufVxyXG5jbGFzcyBMb2dpbkVycm9yIGV4dGVuZHMgRXJyb3Ige1xyXG4gICAgY29uc3RydWN0b3IoKXtcclxuICAgIHN1cGVyKClcclxuICAgIHRoaXMubmFtZSA9ICdMb2dpbkVycm9yJ1xyXG4gICAgdGhpcy5tZXNzYWdlID0gJ1BsZWFzZSBtYWtlIHN1cmUgeW91IGFyZSBsb2dnZWQgaW4gdG8gY29udGludWUuJ1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCB7SHR0cEVycm9yLFBlcm1pc3Npb25FcnJvcixMb2dpbkVycm9yfSIsIlxyXG5pbXBvcnQge0h0dHBFcnJvcn0gZnJvbSAnLi9lcnJvcnMuanMnXHJcbnZhciBoZWxwZXJzID0ge31cclxuaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyID0gKGJhc2U2NCkgPT4ge1xyXG4gICAgdmFyIGJpbmFyeV9zdHJpbmcgPSB3aW5kb3cuYXRvYihiYXNlNjQpXHJcbiAgICB2YXIgbGVuID0gYmluYXJ5X3N0cmluZy5sZW5ndGhcclxuICAgIHZhciBieXRlcyA9IG5ldyBVaW50OEFycmF5KGxlbilcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcclxuICAgICAgICBieXRlc1tpXSA9IGJpbmFyeV9zdHJpbmcuY2hhckNvZGVBdChpKVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGJ5dGVzLmJ1ZmZlclxyXG59XHJcblxyXG5oZWxwZXJzLmFycmF5VG9CYXNlNjQgPSAoYXJyYXkpID0+IHtcclxuICAgIHJldHVybiBidG9hKFN0cmluZy5mcm9tQ2hhckNvZGUuYXBwbHkobnVsbCwgbmV3IFVpbnQ4QXJyYXkoYXJyYXkpKSlcclxufVxyXG5oZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyID0gKHN0cikgPT4geyAvL25lZWRlZCBmb3IgaW1wb3J0a2V5IEkvUFxyXG4gICAgdmFyIGVuY29kZXIgPSBuZXcgVGV4dEVuY29kZXIoXCJ1dGYtOFwiKVxyXG4gICAgcmV0dXJuIGVuY29kZXIuZW5jb2RlKHN0cilcclxufVxyXG5oZWxwZXJzLmNvbnZlcnRBcnJheUJ1ZmZlcnRvU3RyaW5nID0gKGJ1ZmZlcikgPT4ge1xyXG4gICAgdmFyIGRlY29kZXIgPSBuZXcgVGV4dERlY29kZXIoXCJ1dGYtOFwiKVxyXG4gICAgcmV0dXJuIGRlY29kZXIuZGVjb2RlKGJ1ZmZlcilcclxufVxyXG4vLyBDYWxjdWxhdGVzIGhleGFkZWNpbWFsIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiBhcnJheSBidWZmZXJcclxuaGVscGVycy5oZXhTdHJpbmcgPSAoYnVmZmVyKSA9PiB7XHJcbiAgICBjb25zdCBieXRlQXJyYXkgPSBuZXcgVWludDhBcnJheShidWZmZXIpXHJcbiAgICBjb25zdCBoZXhDb2RlcyA9IFsuLi5ieXRlQXJyYXldLm1hcCh2YWx1ZSA9PiB7XHJcbiAgICAgICAgY29uc3QgaGV4Q29kZSA9IHZhbHVlLnRvU3RyaW5nKDE2KVxyXG4gICAgICAgIGNvbnN0IHBhZGRlZEhleENvZGUgPSBoZXhDb2RlLnBhZFN0YXJ0KDIsICcwJylcclxuICAgICAgICByZXR1cm4gcGFkZGVkSGV4Q29kZVxyXG4gICAgfSlcclxuICAgIHJldHVybiBoZXhDb2Rlcy5qb2luKCcnKVxyXG59XHJcblxyXG5oZWxwZXJzLnJlbW92ZUxpbmVzID0gKHBlbSkgPT4ge1xyXG4gICAgdmFyIGxpbmVzID0gcGVtLnNwbGl0KCdcXG4nKVxyXG4gICAgdmFyIGVuY29kZWRTdHJpbmcgPSAnJ1xyXG4gICAgZm9yICh2YXIgaSA9IDE7IGkgPCAobGluZXMubGVuZ3RoIC0gMSk7IGkrKykge1xyXG4gICAgICAgIGVuY29kZWRTdHJpbmcgKz0gbGluZXNbaV0udHJpbSgpXHJcbiAgICB9XHJcbiAgICByZXR1cm4gZW5jb2RlZFN0cmluZ1xyXG59XHJcblxyXG5oZWxwZXJzLnNwa2lUb1BFTSA9IChrZXlCdWZmZXIpID0+IHtcclxuICAgIC8vIHZhciBrZXlkYXRhQjY0UyA9IGhlbHBlcnMuY29udmVyQXJyYXlCdWZmZXJ0b1N0cmluZyhrZXlCdWZmZXIpIFxyXG4gICAgLy8gdmFyIGtleWRhdGFiNjQgPSB0aGlzLmI2NEVuY29kZVVuaWNvZGUoa2V5ZGF0YUI2NFMpXHJcbiAgICAvLyB2YXIga2V5ZGF0YUI2NFBlbSA9IHRoaXMuZm9ybWF0QXNQZW0oa2V5ZGF0YUJcdDY0KS8vYWRkIG5ldyBsaW5lcyBhbmQgc3BsaXQgaW50byA2NFxyXG4gICAgdmFyIGtleWRhdGFCNjQgPSBoZWxwZXJzLmFycmF5VG9CYXNlNjQoa2V5QnVmZmVyKVxyXG4gICAgdmFyIGtleWRhdGFCNjRQZW0gPSBoZWxwZXJzLmZvcm1hdEFzUGVtKGtleWRhdGFCNjQpXHJcbiAgICByZXR1cm4ga2V5ZGF0YUI2NFBlbVxyXG59XHJcblxyXG5oZWxwZXJzLmZvcm1hdEFzUGVtID0gKHN0cikgPT4ge1xyXG4gICAgdmFyIGZpbmFsU3RyaW5nID0gJy0tLS0tQkVHSU4gUFVCTElDIEtFWS0tLS0tXFxuJ1xyXG5cclxuICAgIHdoaWxlIChzdHIubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIGZpbmFsU3RyaW5nICs9IHN0ci5zdWJzdHJpbmcoMCwgNjQpICsgJ1xcbidcclxuICAgICAgICBzdHIgPSBzdHIuc3Vic3RyaW5nKDY0KVxyXG4gICAgfVxyXG5cclxuICAgIGZpbmFsU3RyaW5nID0gZmluYWxTdHJpbmcgKyBcIi0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLVwiXHJcblxyXG4gICAgcmV0dXJuIGZpbmFsU3RyaW5nXHJcbn1cclxuaGVscGVycy5oYW5kbGVGZXRjaEVycm9yID0gKHJlc3BvbnNlKSA9PiB7XHJcbiAgICBpZiAocmVzcG9uc2Uuc3RhdHVzICE9PSAyMDAgJiYgcmVzcG9uc2Uuc3RhdHVzICE9PSAyMDIpIHtcclxuICAgICAgICAvL2NvbnNvbGUuZXJyb3IoXCJSZXNwb25zZSBPYmplY3Q6IFwiKVxyXG4gICAgICAgIC8vY29uc29sZS5lcnJvcihcIlJlc3BvbnNlIG9iamVjdDogXCIrSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKVxyXG4gICAgICAgIHRocm93IG5ldyBIdHRwRXJyb3IocmVzcG9uc2UpXHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgaGVscGVycyIsImltcG9ydCBhc3NldCBmcm9tICcuL2Fzc2V0cy5qcyc7XHJcbmltcG9ydCBoZWxwZXJzIGZyb20gJy4vaGVscGVycy5qcydcclxuaW1wb3J0IGNyeXB0IGZyb20gJy4vY3J5cHRvLmpzJ1xyXG5pbXBvcnQge1Blcm1pc3Npb25FcnJvcixIdHRwRXJyb3J9IGZyb20gJy4vZXJyb3JzLmpzJztcclxuKGZ1bmN0aW9uICgpIHtcclxuICAgIC8qKmFzc2V0XHJcbiAgICAgKiBTRUNUSU9OOiBtZWRibG9ja3MuanNcclxuICAgICAqL1xyXG4gICAgXHJcbiAgICAvKlxyXG4gICAgKiBNYWluIE1lZGJsb2NrcyBjbGFzc1xyXG4gICAgKi9cclxuXHJcbiAgICBjbGFzcyBNZWRibG9ja3Mge1xyXG4gICAgICAgIGNvbnN0cnVjdG9yKGxvY2FsTm9kZUlQID0gXCIxMjcuMC4wLjFcIixsb2NhbE5vZGVQb3J0ID0gXCI4MDgwXCIsbG9jYWxJcGZzSXAgPSBcIjEyNy4wLjAuMVwiLGxvY2FsSXBmc1BvcnQgPSBcIjUwMDFcIikge1xyXG4gICAgICAgICAgICB0aGlzLmxvY2FsTm9kZUlQID0gbG9jYWxOb2RlSVAsIFxyXG4gICAgICAgICAgICB0aGlzLmxvY2FsTm9kZVBvcnQgPSBsb2NhbE5vZGVQb3J0LFxyXG4gICAgICAgICAgICB0aGlzLmxvY2FsSXBmc0lwID0gbG9jYWxJcGZzSXAsXHJcbiAgICAgICAgICAgIHRoaXMubG9jYWxJcGZzUG9ydCA9IGxvY2FsSXBmc1BvcnRcclxuICAgICAgICB9XHJcbiAgICAgICAgYXN5bmMgYWRkUGVybWlzc2lvbihoYXNoLHNlbmRlckVtYWlsSWQscmVjZWl2ZXJFbWFpbElkLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAscG9ydCA9IHRoaXMubG9jYWxOb2RlUG9ydClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAvLzE6IEdldCBCbG9jayBmcm9tIGJsb2NrY2hhaW4gYW5kIGNoZWNrIGlmIHRoZSBsb2dnZWRpbiB1c2VyIGlzIHRoZSBvd25lclxyXG4gICAgICAgICAgICBsZXQgYmxvY2sgPSBhd2FpdCB0aGlzLmdldEJsb2NrKGhhc2gsbm9kZUlwLHBvcnQpXHJcbiAgICAgICAgICAgIC8vMjpEZWNyeXB0IHRoZSByZWNlaXZlcktleSB0byBnZXQgYWVzIGtleVxyXG4gICAgICAgICAgICBsZXQgcmVjZWl2ZXJLZXlTXHJcbiAgICAgICAgICAgIGZvciAobGV0IGluZGV4PTAgOyBpbmRleCA8IGJsb2NrLnBlcm1pc3Npb25zLmxlbmd0aCA7IGluZGV4KyspXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGlmKGJsb2NrLnBlcm1pc3Npb25zW2luZGV4XS5yZWNlaXZlckVtYWlsSWQ9PSBzZW5kZXJFbWFpbElkKXtcclxuICAgICAgICAgICAgICAgICAgICByZWNlaXZlcktleVM9IGJsb2NrLnBlcm1pc3Npb25zW2luZGV4XS5yZWNlaXZlcktleVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRyeVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGlmIChyZWNlaXZlcktleVM9PXVuZGVmaW5lZClcclxuICAgICAgICAgICAgeyAgIFxyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IFBlcm1pc3Npb25FcnJvcigpICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2F0Y2goZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZSlcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGUubWVzc2FnZSlcclxuICAgICAgICAgICAgICAgIHJldHVyblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGxldCByZWNlaXZlcktleUIgPSBoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIocmVjZWl2ZXJLZXlTKVxyXG4gICAgICAgICAgICBsZXQgYWVzS2V5QnVmZmVyID0gYXdhaXQgY3J5cHQuZGVjcnlwdEFlc0tleShyZWNlaXZlcktleUIsV2luZG93LmVQcml2YXRlS2V5KVxyXG4gICAgICAgICAgICBsZXQgYWVzS2V5ID0gYXdhaXQgY3J5cHQuaW1wb3J0QWVzKGFlc0tleUJ1ZmZlcilcclxuICAgICAgICAgICAgLy9sZXQgcmVjZWl2ZXJLZXkgPSBibG9jay5wZXJtaXNzaW9uc1swXVtcInJlY2VpdmVyS2V5XCJdIHdvcmtzXHJcbiAgICAgICAgICAgIGxldCByZWNlaXZlcktleUJ1ZmZlciA9IChhd2FpdCBjcnlwdC5lbmNyeXB0QWVzS2V5KGF3YWl0IGNyeXB0LmV4cG9ydEFlcyhhZXNLZXkpLCBhd2FpdCB0aGlzLmdldFB1YmxpY0tleShyZWNlaXZlckVtYWlsSWQsbm9kZUlwLHBvcnQpKSlcclxuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIrbm9kZUlwK1wiOlwiK3BvcnQrXCIvYWRkUGVybWlzc2lvblwiIFxyXG4gICAgICAgICAgICBsZXQgZGF0YSA9IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICAgICAgICAgIGlwZnNIYXNoOiBoYXNoLFxyXG4gICAgICAgICAgICAgICAgc2VuZGVyRW1haWxJZDogc2VuZGVyRW1haWxJZCxcclxuICAgICAgICAgICAgICAgIHBlcm1pc3Npb25zOiAgW1xyXG4gICAgICAgICAgICAgICAgICAgIHsgcmVjZWl2ZXJFbWFpbElkOiByZWNlaXZlckVtYWlsSWQsIHJlY2VpdmVyS2V5OiBoZWxwZXJzLmFycmF5VG9CYXNlNjQocmVjZWl2ZXJLZXlCdWZmZXIpIH1cclxuXHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH0pIFxyXG4gICAgICAgICAgICBsZXQgc2lnbmF0dXJlID0gYXdhaXQgY3J5cHQuZ2V0U2lnbmF0dXJlKGhlbHBlcnMuY29udmVydFN0cmluZ1RvQXJyYXlCdWZmZXIoZGF0YSkpIFxyXG4gICAgICAgICAgICBjb25zdCBib2R5ID0ge1xyXG4gICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICAgICAgICAgIHNpZ25hdHVyZTogaGVscGVycy5hcnJheVRvQmFzZTY0KHNpZ25hdHVyZSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmZXRjaCh1cmwsIHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpKVxyXG4gICAgICAgICAgICAuY2F0Y2goIChlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgICAgICAgICByZXR1cm59KVxyXG5cclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgYXN5bmMgZ2V0UHVibGljS2V5KGVtYWlsSWQsbm9kZUlwID0gdGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydCA9IHRoaXMubG9jYWxOb2RlUG9ydCkvLyBmb3IgYWRkYmxvY2sgZ2V0dGluZyBwdWJsaWMga2V5cyBvZiBvdGhlciB1c2Vyc1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIgKyBub2RlSXAgKyBcIjpcIitub2RlUG9ydCtcIi9nZXRVc2VyXCIgXHJcbiAgICAgICAgICAgIGNvbnN0IGJvZHkgPSB7XHJcbiAgICAgICAgICAgICAgICBlbWFpbElkOiBlbWFpbElkXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIGVQdWJsaWNLZXkgPSBhd2FpdCBmZXRjaCh1cmwsIHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2UpID0+IHtoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYXdhaXQgcmVzcG9uc2UuanNvbigpXHJcbiAgICAgICAgICAgICAgICAudGhlbihcclxuICAgICAgICAgICAgICAgICAgICBhc3luYyAocmVzcG9uc2VKc29uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGF3YWl0IGNyeXB0LmdldEVQdWJsaWNLZXlGcm9tQnVmZmVyKGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihoZWxwZXJzLnJlbW92ZUxpbmVzKHJlc3BvbnNlSnNvbi5lUHVibGljS2V5KSkpKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYXdhaXQgY3J5cHQuZ2V0RVB1YmxpY0tleUZyb21CdWZmZXIoaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKGhlbHBlcnMucmVtb3ZlTGluZXMocmVzcG9uc2VKc29uLmVQdWJsaWNLZXkpKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy9yZXR1cm4gYXdhaXQgY3J5cHQuZ2V0RVB1YmxpY0tleUZyb21CdWZmZXIoaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKGhlbHBlcnMucmVtb3ZlTGluZXMocmVzcG9uc2VKc29uLmVQdWJsaWNLZXkpKSlcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaChlID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJFcnJvciBvY2NlcmVkIHdoaWxlIGZldGNoaW5nIHB1YmxpYyBrZXlzIDpcIixlKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIHJldHVybiBlUHVibGljS2V5XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYXN5bmMgcmVnaXN0ZXIoZW1haWwscGFzc3dvcmQsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0LG5hbWUsc2V4KS8vZVB1YmxpY0tleSwgc1B1YmxpY0tleSAsIGVtYWlsaWQsIG1ldGFkYXRhW09wdGlvbmFsXVxyXG4gICAgICAgIC8vIE5vIG1vcmUgcGFzc3dvcmQgZW5jcnlwdGlvbiBvZiBwcml2YXRla2V5XHJcbiAgICAgICAgeyAgIFxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhuYW1lKVxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhzZXgpXHJcbiAgICAgICAgICAgIC8vdmFyIGl2PVwiaXZcIlxyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nIChcIlVzZXIgZW50ZXJlZCBwYXNzd29yZDogXCIgKyBwYXNzd29yZCkgXHJcbiAgICAgICAgICAgIHZhciBlS2V5UGFpciA9IGF3YWl0IGNyeXB0LmdlbmVyYXRlX1JTQV9LZXlwYWlyKCkgXHJcbiAgICAgICAgICAgIHZhciBzS2V5UGFpciA9IGF3YWl0IGNyeXB0LmdlbmVyYXRlX1JTQVNTQV9LZXlQYWlyKCkgXHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coXCJHZW5lcmF0ZWQgU2lnbmF0dXJlIGtleSBwYWlyOiBcXG5cIilcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhzS2V5UGFpcilcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIkdlbmVyYXRlZCBFbmNyeXB0aW9uIGtleSBwYWlyOiBcXG5cIilcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhlS2V5UGFpcikgXHJcbiAgICAgICAgICAgIC8vaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvOTU3NTM3L2hvdy1jYW4taS1kaXNwbGF5LWEtamF2YXNjcmlwdC1vYmplY3RcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhlS2V5UGFpci5rZXlQYWlyQ3J5cHRvLnByaXZhdGVLZXlDcnlwdG8pXHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coZUtleVBhaXIua2V5UGFpckNyeXB0by5wcml2YXRlS2V5Q3J5cHRvKVxyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLnN0b3JlUHVibGljS2V5cyhlbWFpbCxlS2V5UGFpci5rZXlQYWlyQ3J5cHRvLnB1YmxpY0tleUNyeXB0bywgc0tleVBhaXIua2V5UGFpckNyeXB0by5wdWJsaWNLZXlDcnlwdG8sbm9kZUlwLG5vZGVQb3J0KVxyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLnN0b3JlS2V5cyhlbWFpbCxlS2V5UGFpci5rZXlQYWlyQ3J5cHRvLnB1YmxpY0tleUNyeXB0byxzS2V5UGFpci5rZXlQYWlyQ3J5cHRvLnB1YmxpY0tleUNyeXB0bywgZUtleVBhaXIua2V5UGFpckNyeXB0by5wcml2YXRlS2V5Q3J5cHRvLCBzS2V5UGFpci5rZXlQYWlyQ3J5cHRvLnByaXZhdGVLZXlDcnlwdG8scGFzc3dvcmQsbm9kZUlwLG5vZGVQb3J0KSBcclxuICAgICAgICAgICAgbGV0IHVzZXJJZGVudGl0eVBhcmFtcyA9IHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IG5hbWUsXHJcbiAgICAgICAgICAgICAgICBzZXg6IHNleCxcclxuICAgICAgICAgICAgICAgIGVtYWlsaWQ6ZW1haWxcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh1c2VySWRlbnRpdHlQYXJhbXMpXHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMubG9naW4oZW1haWwscGFzc3dvcmQsbm9kZUlwLG5vZGVQb3J0KVxyXG4gICAgICAgICAgICAvL2NyZWF0ZSBhIGZpbGUgZm9yIHVzZXIgaW5kZW50aXR5IG1hbmFnZW1lbnQgXHJcbiAgICAgICAgICAgIGxldCB1c2VySWRlbnRpdHlGaWxlID0gbmV3IEZpbGUoW0pTT04uc3RyaW5naWZ5KHVzZXJJZGVudGl0eVBhcmFtcyldLFwidXNlcklkZW50aXR5RmlsZVwiLHt0eXBlOiBcInRleHQvcGxhaW5cIn0pXHJcbiAgICAgICAgICAgIC8vdXBsb2FkIHRoZSBmaWxlIHRvIGlwZnNcclxuICAgICAgICAgICAgdmFyIGEgPSBhd2FpdCB0aGlzLnVwbG9hZEZpbGUodXNlcklkZW50aXR5RmlsZSxlbWFpbCxlbWFpbCx0cnVlLG5vZGVJcCxub2RlUG9ydCx0aGlzLmxvY2FsSXBmc0lwLHRoaXMubG9jYWxJcGZzUG9ydClcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2coYSlcclxuICAgICAgICBcclxuICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgYXN5bmMgc3RvcmVQdWJsaWNLZXlzKGVtYWlsSWQsZVB1YmxpY0tleUNyeXB0bywgc1B1YmxpY0tleUNyeXB0byxub2RlSXAgPSB0aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0KS8vd29ya3NcclxuICAgICAgICB7Ly9CYWNrZW5kIHJlZ2lzdHJhdGlvblxyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiICsgbm9kZVBvcnQgKyBcIi9yZWdpc3RlclwiIFxyXG4gICAgICAgICAgICAvL2xldCBlbmNyeXB0ZWRTa2V5ID0gYXdhaXQgY3J5cHQuZW5jcnlwdGtleShwYXNzd29yZCxhd2FpdCBjcnlwdC5nZXRQa2NzOChzUHVibGljS2V5Q3J5cHRvKSlcclxuICAgICAgICAgICAgY29uc3QgYm9keSA9XHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGVtYWlsSWQ6IGVtYWlsSWQsXHJcbiAgICAgICAgICAgICAgICBlUHVibGljS2V5OiBhd2FpdCBjcnlwdC5nZXRQZW0oZVB1YmxpY0tleUNyeXB0byksXHJcbiAgICAgICAgICAgICAgICBzUHVibGljS2V5OiBhd2FpdCBjcnlwdC5nZXRQZW0oc1B1YmxpY0tleUNyeXB0bylcclxuICAgICAgICAgICAgfSBcclxuICAgICAgICAgICAgYXdhaXQgZmV0Y2godXJsLCB7XHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKT0+aGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKSlcclxuICAgICAgICAgICAgLmNhdGNoKCBlID0+IHsgXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciB3aGlsZSBzdG9yaW5nIHB1YmxpYyBrZXlzOiAnK2UpXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9XHJcbiAgICAgICAgYXN5bmMgc3RvcmVLZXlzKGVtYWlsSWQsIGVQdWJsaWNLZXlDcnlwdG8sIHNQdWJsaWNLZXlDcnlwdG8sIGVQcml2YXRlS2V5Q3J5cHRvLCBzUHJpdmF0ZUtleUNyeXB0byxwYXNzd29yZCxub2RlSXAgPSB0aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0KS8vd29ya3NcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coZVByaXZhdGVLZXlDcnlwdG8pXHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2coc1ByaXZhdGVLZXlDcnlwdG8pXHJcbiAgICAgICAgICAgIC8vZWNucnlwdCB0aGUgcHJpdmF0ZWtleXMgd2l0aCB1c2VyJ3MgcGFzc3dvcmQgXHJcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIgKyBub2RlUG9ydCArIFwiL3N0b3JlS2V5XCIgXHJcbiAgICAgICAgICAgIGxldCBlbmNyeXB0ZWRFS2V5ID0gYXdhaXQgY3J5cHQuZW5jcnlwdGtleShwYXNzd29yZCxhd2FpdCBjcnlwdC5nZXRQa2NzOChlUHJpdmF0ZUtleUNyeXB0bykpXHJcbiAgICAgICAgICAgIGxldCBlbmNyeXB0ZWRTa2V5ID0gYXdhaXQgY3J5cHQuZW5jcnlwdGtleShwYXNzd29yZCxhd2FpdCBjcnlwdC5nZXRQa2NzOChzUHJpdmF0ZUtleUNyeXB0bykpXHJcbiAgICAgICAgICAgIGNvbnN0IGJvZHkgPVxyXG4gICAgICAgICAgICB7XHJcblxyXG4gICAgICAgICAgICAgICAgZW1haWxJZDogZW1haWxJZCxcclxuICAgICAgICAgICAgICAgIHNQdWJsaWNLZXk6IGF3YWl0IGNyeXB0LmdldFBlbShzUHVibGljS2V5Q3J5cHRvKSxcclxuICAgICAgICAgICAgICAgIGVQdWJsaWNLZXk6IGF3YWl0IGNyeXB0LmdldFBlbShlUHVibGljS2V5Q3J5cHRvKSxcclxuICAgICAgICAgICAgICAgIHNQcml2YXRlS2V5OiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoZW5jcnlwdGVkU2tleS5lbmNyeXB0ZWRLZXlCdWZmZXIpLC8vcmV0dXJucyBhIHN0cmluZ1xyXG4gICAgICAgICAgICAgICAgZVByaXZhdGVLZXk6IGhlbHBlcnMuYXJyYXlUb0Jhc2U2NChlbmNyeXB0ZWRFS2V5LmVuY3J5cHRlZEtleUJ1ZmZlciksXHJcbiAgICAgICAgICAgICAgICBJVjp7XHJcbiAgICAgICAgICAgICAgICAgICAgaXZlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoZW5jcnlwdGVkRUtleS5pdiksXHJcbiAgICAgICAgICAgICAgICAgICAgaXZzOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoZW5jcnlwdGVkU2tleS5pdilcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBhd2FpdCBmZXRjaCh1cmwsIHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYm9keSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpKVxyXG4gICAgICAgICAgICAuY2F0Y2goIGU9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciB3aGlsZSBzdHJvaW5nIHByaXZhdGUga2V5cyA6JytlKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJufSlcclxuICAgICAgICB9XHJcbiAgICAgICAgYXN5bmMgbG9naW4oZW1haWwscGFzc3dvcmQsbm9kZUlwID0gdGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydCA9IHRoaXMubG9jYWxOb2RlUG9ydCkge1xyXG4gICAgICAgICAgICAvL2NyZWF0ZSBzZXNzaW9uIHZhcmlhYmxlcyBmb3IgdGhlIHVzZXJcclxuICAgICAgICAgICAgV2luZG93Lm1iVXNlciA9IGVtYWlsXHJcbiAgICAgICAgICAgIGF3YWl0IHRoaXMuZ2V0UHJpdmF0ZUtleXMoZW1haWwscGFzc3dvcmQsbm9kZUlwLG5vZGVQb3J0KVxyXG4gICAgICAgICAgICBhd2FpdCB0aGlzLmdldFB1YmxpY0tleXMoZW1haWwsbm9kZUlwLG5vZGVQb3J0KVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyBnZXRQdWJsaWNLZXlzKGVtYWlsSWQsbm9kZUlwID0gdGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydCA9IHRoaXMubG9jYWxOb2RlUG9ydCkvL2ZldGNoIHB1YmxpYyBrZXkgYW5kIHNldCBzZXNzaW9uIHZhcmlhYmxlXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiKyBub2RlUG9ydCArXCIvZ2V0VXNlclwiIFxyXG4gICAgICAgICAgICBjb25zdCBib2R5ID0ge1xyXG4gICAgICAgICAgICAgICAgZW1haWxJZDogZW1haWxJZFxyXG4gICAgICAgICAgICAgICAgLy8gc1B1YmxpY0tleTogYXdhaXQgY3J5cHQuZ2V0UGVtKFdpbmRvdy5zUHVibGljS2V5KVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZldGNoKHVybCwge1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgICAgICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IFxyXG4gICAgICAgICAgICAgICAgeyAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5qc29uKClcclxuICAgICAgICAgICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2VKc29uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFdpbmRvdy5lUHVibGljS2V5ID0gYXdhaXQgY3J5cHQuZ2V0RVB1YmxpY0tleUZyb21CdWZmZXIoaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKGhlbHBlcnMucmVtb3ZlTGluZXMocmVzcG9uc2VKc29uLmVQdWJsaWNLZXkpKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgV2luZG93LnNQdWJsaWNLZXkgPSBhd2FpdCBjcnlwdC5nZXRFUHVibGljS2V5RnJvbUJ1ZmZlcihoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIoaGVscGVycy5yZW1vdmVMaW5lcyhyZXNwb25zZUpzb24uc1B1YmxpY0tleSkpKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhXaW5kb3cuZVB1YmxpY0tleSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coV2luZG93LnNQdWJsaWNLZXkpXHJcbiAgICAgICAgICAgICAgICAgICAgfSl9XHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igb2NjdXJlZCB3aGlsZSBnZXR0aW5nIHB1YmxpYyBrZXlzOiAnK2UpXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyBnZXRQcml2YXRlS2V5cyhlbWFpbElkLHBhc3N3b3JkLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIitub2RlSXAgK1wiOlwiK25vZGVQb3J0K1wiL2dldEtleVwiIFxyXG4gICAgICAgICAgICBjb25zdCBib2R5ID0ge1xyXG4gICAgICAgICAgICAgICAgZW1haWxJZDogZW1haWxJZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGF3YWl0IGZldGNoKHVybCwge1xyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgICAgICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgcmVzcG9uc2UuanNvbigpXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlSnNvbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhhd2FpdCBjcnlwdC5kZWNyeXB0S2V5KHBhc3N3b3JkLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihyZXNwb25zZUpzb24uZVByaXZhdGVLZXkpLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihKU09OLnBhcnNlKHJlc3BvbnNlSnNvbi5JVikuaXZlKSkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFdpbmRvdy5lUHJpdmF0ZUtleSA9IGF3YWl0IGNyeXB0LmdldEVQcml2YXRlS2V5RnJvbUJ1ZmZlcihhd2FpdCBjcnlwdC5kZWNyeXB0S2V5KHBhc3N3b3JkLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihyZXNwb25zZUpzb24uZVByaXZhdGVLZXkpLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihyZXNwb25zZUpzb24uSVYuSVZFKSkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFdpbmRvdy5zUHJpdmF0ZUtleSA9IGF3YWl0IGNyeXB0LmdldFNQcml2YXRlS2V5RnJvbUJ1ZmZlcihhd2FpdCBjcnlwdC5kZWNyeXB0S2V5KHBhc3N3b3JkLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihyZXNwb25zZUpzb24uc1ByaXZhdGVLZXkpLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihyZXNwb25zZUpzb24uSVYuSVZTKSkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFdpbmRvdy5lUHVibGljS2V5ID0gYXdhaXQgY3J5cHQuZ2V0RVB1YmxpY0tleUZyb21CdWZmZXIoaGVscGVycy5iYXNlNjRUb0FycmF5QnVmZmVyKGhlbHBlcnMucmVtb3ZlTGluZXMocmVzcG9uc2VKc29uLmVQdWJsaWNLZXkpKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gV2luZG93LnNQdWJsaWNLZXkgPSBhd2FpdCBjcnlwdC5nZXRTUHVibGljS2V5RnJvbUJ1ZmZlcihoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIoaGVscGVycy5yZW1vdmVMaW5lcyhyZXNwb25zZUpzb24uc1B1YmxpY0tleSkpKSAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhXaW5kb3cuZVByaXZhdGVLZXkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFdpbmRvdy5zUHJpdmF0ZUtleSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coV2luZG93LnNQdWJsaWNLZXkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFdpbmRvdy5lUHVibGljS2V5KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvL2NvbnNvbGUubG9nKGhlbHBlcnMuY29udmVydEFycmF5QnVmZmVydG9TdHJpbmcoYXdhaXQgY3J5cHQuZGVjcnlwdEtleShwYXNzd29yZCxoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIocmVzcG9uc2VKc29uLnNQdWJsaWNLZXkpLGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihKU09OLnBhcnNlKHJlc3BvbnNlSnNvbi5JVikuaXZzcCkpKSlcclxuICAgICAgICAgICAgICAgICAgICB9KX1cclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgIC5jYXRjaChlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBvY2N1cmVkIHdoaWxlIGdldHRpbmcgcHJpdmF0ZSBrZXlzOiAnK2UpXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyB1cGxvYWRGaWxlKGZpbGUsIGNyZWF0b3JFbWFpbElkLCBvd25lckVtYWlsSWQsIGlzSWRlbnRpdHksIG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQsaXBmc05vZGVJcCA9IHRoaXMubG9jYWxJcGZzSXAsaXBmc1BvcnQgPSB0aGlzLmxvY2FsSXBmc1BvcnQpIHtcclxuICAgICAgICAgICAgLy8xOkdlbmVyYXRlIFJhbmRvbSBBZXMgS2V5IGFuZCBpdHMgcGFyYW1zXHJcbiAgICAgICAgICAgIGxldCBhZXNLZXkgPSBhd2FpdCBjcnlwdC5nZW5lcmF0ZV9BRVNfS2V5KClcclxuICAgICAgICAgICAgbGV0IGl2ID0gd2luZG93LmNyeXB0by5nZXRSYW5kb21WYWx1ZXMobmV3IFVpbnQ4QXJyYXkoMTYpKVxyXG4gICAgICAgICAgICAvLzI6IEdldCBIYXNoXHJcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgaXBmc05vZGVJcCArIFwiOlwiK2lwZnNQb3J0K1wiL2FwaS92MC9ibG9jay9wdXRcIiBcclxuICAgICAgICAgICAgdmFyIHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKClcclxuICAgICAgICAgICAgLy9mZXRjaCBkb2VzbnQgd29yayB3aXRoIGxvY2FsIGZpbGVzIHNvIEkgdXNlZCByZWFkZXIgXHJcbiAgICAgICAgICAgIHJlYWRlci5hZGRFdmVudExpc3RlbmVyKFwibG9hZFwiLCBhc3luYyAocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZmlsZVN0cmVhbSA9IG5ldyBGb3JtRGF0YSgpIFxyXG4gICAgICAgICAgICAgICAgLy8zOiBFbmNyeXB0IHRoZSBmaWxlIHdpdGggQWVzIEtleVxyXG4gICAgICAgICAgICAgICAgdmFyIGVuY3J5cHRlZEZpbGUgPSBhd2FpdCBjcnlwdC5lbmNyeXB0RmlsZUFlcyhyZWFkZXIucmVzdWx0LCBhZXNLZXksIGl2KS8vYWRkIGl2XHJcbiAgICAgICAgICAgICAgICBmaWxlU3RyZWFtLmFwcGVuZCgncGF0aCcsIChuZXcgQmxvYihbKGVuY3J5cHRlZEZpbGUpXSkpKVxyXG4gICAgICAgICAgICAgICAgZmV0Y2godXJsLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9keTogZmlsZVN0cmVhbSxcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4ociA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgaGFzaCA9IHIuanNvbigpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhlbihcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhc3luYyAocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vMzogQWRkQmxvY2sgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuYWRkQmxvY2socmVzcG9uc2UuS2V5LCBmaWxlLm5hbWUsIGZpbGUudHlwZSwgY3JlYXRvckVtYWlsSWQsIG93bmVyRW1haWxJZCwgYWVzS2V5LCBpdixub2RlSXAsbm9kZVBvcnQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc0lkZW50aXR5PT10cnVlKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMudXBkYXRlSWRlbnRpdHkocmVzcG9uc2UuS2V5LCBvd25lckVtYWlsSWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgcmVhZGVyLnJlYWRBc0FycmF5QnVmZmVyKGZpbGUpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGFzeW5jIHVwbG9hZEZpbGVzKGZpbGVMaXN0LCBjcmVhdG9yRW1haWxJZCwgb3duZXJFbWFpbElkLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAgLG5vZGVQb3J0ID0gdGhpcy5sb2NhbE5vZGVQb3J0LGlwZnNOb2RlSXAgPSB0aGlzLmxvY2FsSXBmc0lwLGlwZnNQb3J0ID0gdGhpcy5sb2NhbElwZnNQb3J0KSB7XHJcbiAgICAgICAgICAgIC8vZm9yIGxvb3AgZm9yIG11bHRpcGxlIGZpbGVzXHJcbiAgICAgICAgICAgIGZpbGVMaXN0LmZvckVhY2goZmlsZSA9PiB0aGlzLnVwbG9hZEZpbGUoZmlsZSwgY3JlYXRvckVtYWlsSWQsIG93bmVyRW1haWxJZCwgZmFsc2Usbm9kZUlwLG5vZGVQb3J0LGlwZnNOb2RlSXAsaXBmc1BvcnQpKVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyBhZGRCbG9jayhoYXNoLCBuYW1lLCBmb3JtYXQsIGNyZWF0b3JFbWFpbElkLCBvd25lckVtYWlsSWQsIGFlc0tleSwgaXYsIG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQpIHtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8vbGV0IHB1YmxpY1JlY2VpdmVyS2V5ID0gYXdhaXQgdGhpcy5nZXRQdWJsaWNLZXkob3duZXJFbWFpbElkLG5vZGVJcCxub2RlUG9ydClcclxuICAgICAgICAgICAgY29uc29sZS5sb2coYXdhaXQgdGhpcy5nZXRQdWJsaWNLZXkob3duZXJFbWFpbElkLG5vZGVJcCxub2RlUG9ydCkpXHJcbiAgICAgICAgICAgIGxldCBvd25lcktleUJ1ZmZlciA9IChhd2FpdCBjcnlwdC5lbmNyeXB0QWVzS2V5KGF3YWl0IGNyeXB0LmV4cG9ydEFlcyhhZXNLZXkpLCBhd2FpdCB0aGlzLmdldFB1YmxpY0tleShvd25lckVtYWlsSWQsbm9kZUlwLG5vZGVQb3J0KSkpXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKG93bmVyS2V5QnVmZmVyKVxyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIiArIG5vZGVJcCArIFwiOlwiK25vZGVQb3J0K1wiL2FkZEJsb2NrXCJcclxuICAgICAgICAgICAgY29uc3QgZGF0YSA9IEpTT04uc3RyaW5naWZ5KFt7XHJcbiAgICAgICAgICAgICAgICBpcGZzSGFzaDogaGFzaCxcclxuICAgICAgICAgICAgICAgIG5hbWU6IG5hbWUsXHJcbiAgICAgICAgICAgICAgICBmb3JtYXQ6IGZvcm1hdCxcclxuICAgICAgICAgICAgICAgIHNpZ25hdG9yeUVtYWlsSWQ6IGNyZWF0b3JFbWFpbElkLFxyXG4gICAgICAgICAgICAgICAgb3duZXJFbWFpbElkOiBvd25lckVtYWlsSWQsXHJcbiAgICAgICAgICAgICAgICBwZXJtaXNzaW9uczogW1xyXG4gICAgICAgICAgICAgICAgICAgIHsgcmVjZWl2ZXJFbWFpbElkOiBvd25lckVtYWlsSWQsIHJlY2VpdmVyS2V5OiBoZWxwZXJzLmFycmF5VG9CYXNlNjQob3duZXJLZXlCdWZmZXIpIH1cclxuXHJcbiAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgSVY6aGVscGVycy5hcnJheVRvQmFzZTY0KGl2KVxyXG4gICAgICAgICAgICB9XSlcclxuICAgICAgICAgICAgLy8gR2VuZXJhdGUgU2lnbmF0dXJlXHJcbiAgICAgICAgICAgIGxldCBzaWduYXR1cmUgPSBhd2FpdCBjcnlwdC5nZXRTaWduYXR1cmUoaGVscGVycy5jb252ZXJ0U3RyaW5nVG9BcnJheUJ1ZmZlcihkYXRhKSlcclxuICAgICAgICAgICAgY29uc3QgYmxvY2tkYXRhID1cclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICAgICAgICAgIHNpZ25hdHVyZTogaGVscGVycy5hcnJheVRvQmFzZTY0KHNpZ25hdHVyZSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gZmV0Y2godXJsLCB7XHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoYmxvY2tkYXRhKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5qc29uKClcclxuICAgICAgICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyB1cGRhdGVJZGVudGl0eShoYXNoLCBvd25lckVtYWlsSWQsIG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQpIHtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8vbGV0IHB1YmxpY1JlY2VpdmVyS2V5ID0gYXdhaXQgdGhpcy5nZXRQdWJsaWNLZXkob3duZXJFbWFpbElkLG5vZGVJcCxub2RlUG9ydClcclxuICAgICAgICAgICAgY29uc29sZS5sb2coYXdhaXQgdGhpcy5nZXRQdWJsaWNLZXkob3duZXJFbWFpbElkLG5vZGVJcCxub2RlUG9ydCkpXHJcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiICsgbm9kZUlwICsgXCI6XCIrbm9kZVBvcnQrXCIvdXBkYXRlSWRlbnRpdHlcIlxyXG4gICAgICAgICAgICBjb25zdCBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgICAgICAgICAgaWRlbnRpdHlGaWxlSGFzaDogaGFzaCxcclxuICAgICAgICAgICAgICAgIGVtYWlsSWQ6IG93bmVyRW1haWxJZFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAvLyBHZW5lcmF0ZSBTaWduYXR1cmVcclxuICAgICAgICAgICAgbGV0IHNpZ25hdHVyZSA9IGF3YWl0IGNyeXB0LmdldFNpZ25hdHVyZShoZWxwZXJzLmNvbnZlcnRTdHJpbmdUb0FycmF5QnVmZmVyKGRhdGEpKVxyXG4gICAgICAgICAgICBjb25zdCBibG9ja2RhdGEgPVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgICAgICAgICAgc2lnbmF0dXJlOiBoZWxwZXJzLmFycmF5VG9CYXNlNjQoc2lnbmF0dXJlKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBmZXRjaCh1cmwsIHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShibG9ja2RhdGEpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmpzb24oKVxyXG4gICAgICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBhc3luYyBnZXRCbG9jayhoYXNoLG5vZGVJcCA9IHRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQgPSB0aGlzLmxvY2FsTm9kZVBvcnQpLy93aW5kb3cgb2JqZWN0IGVycm9yXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSBcImh0dHA6Ly9cIitub2RlSXArXCI6XCIrbm9kZVBvcnQrXCIvZ2V0QmxvY2tcIlxyXG4gICAgICAgICAgICBjb25zdCBib2R5ID0ge1xyXG4gICAgICAgICAgICAgICAgaXBmc0hhc2g6IGhhc2hcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgYmxvY2sgPSBhd2FpdCBmZXRjaCh1cmwsIHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShib2R5KVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAudGhlbihhc3luYyAocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgIGhlbHBlcnMuaGFuZGxlRmV0Y2hFcnJvcihyZXNwb25zZSlcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlc3BvbnNlSnNvbiA9IGF3YWl0IHJlc3BvbnNlLmpzb24oKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZUpzb247XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaCggKGUpPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkVycm9yIHdoaWxlIGZldGNoaW5nIGJsb2NrIGZyb20gYmFja2VuZCA6XCIrZSlcclxuICAgICAgICAgICAgICAgIHJldHVybn0pXHJcbiAgICAgICAgICAgIHJldHVybiBibG9ja1xyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyBkb3dubG9hZEJsb2NrIChoYXNoLGVtYWlsSWQsbm9kZUlwID0gdGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydCA9IHRoaXMubG9jYWxOb2RlUG9ydCxpcGZzTm9kZUlwID0gdGhpcy5sb2NhbElwZnNJcCxpcGZzUG9ydCA9IHRoaXMubG9jYWxJcGZzUG9ydCkgXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBsZXQgYmxvY2tEYXRhID0gYXdhaXQgdGhpcy5nZXRCbG9jayhoYXNoLG5vZGVJcCxub2RlUG9ydClcclxuICAgICAgICAgICAgLy9jaGVjayBwZXJtaXNzaW9uc1xyXG4gICAgICAgICAgICBsZXQgcmVjZWl2ZXJLZXlTXHJcbiAgICAgICAgICAgIGZvciAobGV0IGluZGV4PTA7IGluZGV4IDwgYmxvY2tEYXRhLnBlcm1pc3Npb25zLmxlbmd0aDsgaW5kZXgrKylcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgaWYoYmxvY2tEYXRhLnBlcm1pc3Npb25zW2luZGV4XS5yZWNlaXZlckVtYWlsSWQ9PSBlbWFpbElkKXtcclxuICAgICAgICAgICAgICAgICAgICByZWNlaXZlcktleVM9IGJsb2NrRGF0YS5wZXJtaXNzaW9uc1tpbmRleF0ucmVjZWl2ZXJLZXlcclxuICAgICAgICAgICAgICAgICAgICBicmVha1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdHJ5IFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGlmIChyZWNlaXZlcktleVMgPT0gdW5kZWZpbmVkKXtcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBQZXJtaXNzaW9uRXJyb3IoKVxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICBjYXRjaChlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlKVxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZS5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGxldCByZWNlaXZlcktleUIgPSBoZWxwZXJzLmJhc2U2NFRvQXJyYXlCdWZmZXIocmVjZWl2ZXJLZXlTKVxyXG4gICAgICAgICAgICAvL2xvZ2luIGJlZm9yZSB5b3UgY2FsbCB0aGlzIG1ldGhvZFxyXG4gICAgICAgICAgICBsZXQgYWVzS2V5QnVmZmVyID0gYXdhaXQgY3J5cHQuZGVjcnlwdEFlc0tleShyZWNlaXZlcktleUIsV2luZG93LmVQcml2YXRlS2V5KVxyXG4gICAgICAgICAgICBsZXQgYWVzS2V5ID0gYXdhaXQgY3J5cHQuaW1wb3J0QWVzKGFlc0tleUJ1ZmZlcilcclxuICAgICAgICAgICAgLy9nZXQgZmlsZSBidWZmZXIgYW5kIGRlY3J5cHQgd2l0aCBhZXMga2V5XHJcbiAgICAgICAgICAgIGxldCBpdiA9IGhlbHBlcnMuYmFzZTY0VG9BcnJheUJ1ZmZlcihibG9ja0RhdGEuSVYpXHJcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IFwiaHR0cDovL1wiK2lwZnNOb2RlSXArXCI6XCIraXBmc1BvcnQrXCIvYXBpL3YwL2Jsb2NrL2dldD9hcmc9XCIraGFzaFxyXG4gICAgICAgICAgICB2YXIgZmlsZSA9IGF3YWl0IGZldGNoKCB1cmwsIHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDpcIlBPU1RcIixcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnRoZW4oYXN5bmMgKHJlc3BvbnNlKT0+IFxyXG4gICAgICAgICAgICB7Ly9jYW50IGNvbnZlcnQgdG8ganNvblxyXG4gICAgICAgICAgICAgICAgaGVscGVycy5oYW5kbGVGZXRjaEVycm9yKHJlc3BvbnNlKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGF3YWl0IHJlc3BvbnNlLmFycmF5QnVmZmVyKCkudGhlbihcclxuICAgICAgICAgICAgICAgICAgICBhc3luYyAoZW5jcnlwdGVkRmlsZUJ1ZmZlcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgZmlsZSA9IG5ldyBGaWxlKFthd2FpdCBjcnlwdC5kZWNyeXB0RmlsZUFlcyhlbmNyeXB0ZWRGaWxlQnVmZmVyLGFlc0tleSxpdildLGJsb2NrRGF0YS5uYW1lLHt0eXBlOiBibG9ja0RhdGEuZm9ybWF0fSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlKGZpbGUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmaWxlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICB9KVx0XHJcbiAgICAgICAgICAgIC5jYXRjaCgoZSk9PlxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFwiRXJyb3Igb2NjdXJlZCB3aGlsZSBkb3dubG9hZGluZyBibG9jazogXCIrZSlcclxuICAgICAgICAgICAgICAgIHJldHVyblxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgc2F2ZShmaWxlKSB7XHJcbiAgICAgICAgICAgIC8vdmFyIGJsb2IgPSBuZXcgQmxvYihbZGF0YV0sIHt0eXBlOnR5cGUgfSkgXHJcbiAgICAgICAgICAgIGlmKHdpbmRvdy5uYXZpZ2F0b3IubXNTYXZlT3JPcGVuQmxvYikge1xyXG4gICAgICAgICAgICAgICAgd2luZG93Lm5hdmlnYXRvci5tc1NhdmVCbG9iKGZpbGUsIGZpbGUubmFtZSkgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgIHZhciBlbGVtID0gd2luZG93LmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKSBcclxuICAgICAgICAgICAgICAgIGVsZW0uaHJlZiA9IHdpbmRvdy5VUkwuY3JlYXRlT2JqZWN0VVJMKGZpbGUpIFxyXG4gICAgICAgICAgICAgICAgZWxlbS5kb3dubG9hZCA9IGZpbGUubmFtZSAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChlbGVtKSBcclxuICAgICAgICAgICAgICAgIGVsZW0uY2xpY2soKSAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChlbGVtKSBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAvL05vdGUgdGhhdCwgZGVwZW5kaW5nIG9uIHlvdXIgc2l0dWF0aW9uLCB5b3UgbWF5IGFsc28gd2FudCB0byBjYWxsIFVSTC5yZXZva2VPYmplY3RVUkwgYWZ0ZXIgcmVtb3ZpbmcgZWxlbS4gXHJcbiAgICAgICAgLy9BY2NvcmRpbmcgdG8gdGhlIGRvY3MgZm9yIFVSTC5jcmVhdGVPYmplY3RVUkw6XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy9FYWNoIHRpbWUgeW91IGNhbGwgY3JlYXRlT2JqZWN0VVJMKCksIGEgbmV3IG9iamVjdCBVUkwgaXMgY3JlYXRlZCwgXHJcbiAgICAgICAgLy9ldmVuIGlmIHlvdSd2ZSBhbHJlYWR5IGNyZWF0ZWQgb25lIGZvciB0aGUgc2FtZSBvYmplY3QuIEVhY2ggb2YgdGhlc2UgbXVzdCBiZSByZWxlYXNlZCBieSBjYWxsaW5nIFVSTC5yZXZva2VPYmplY3RVUkwoKSBcclxuICAgICAgICAvL3doZW4geW91IG5vIGxvbmdlciBuZWVkIHRoZW0uIEJyb3dzZXJzIHdpbGwgcmVsZWFzZSB0aGVzZSBhdXRvbWF0aWNhbGx5IHdoZW4gdGhlIGRvY3VtZW50IGlzIHVubG9hZGVkICBcclxuICAgICAgICAvL2hvd2V2ZXIsIGZvciBvcHRpbWFsIHBlcmZvcm1hbmNlIGFuZCBtZW1vcnkgdXNhZ2UsIGlmIHRoZXJlIGFyZSBzYWZlIHRpbWVzIHdoZW4geW91IGNhbiBleHBsaWNpdGx5IHVubG9hZCB0aGVtLCBcclxuICAgICAgICAvL3lvdSBzaG91bGQgZG8gc28uXHJcblxyXG4gICAgICAgIGFzeW5jIGxpc3RCbG9jayAobm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0LG93bmVyRW1haWxJZCxwZXJtaXR0ZWRFbWFpbElkKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgY29uc3QgdXJsID0gXCJodHRwOi8vXCIrbm9kZUlwK1wiOlwiK25vZGVQb3J0K1wiL2xpc3RcIlxyXG4gICAgICAgICAgICB2YXIgYm9keSA9IHt9XHJcbiAgICAgICAgICAgIGlmIChvd25lckVtYWlsSWQhPXVuZGVmaW5lZCAmJiBwZXJtaXR0ZWRFbWFpbElkIT0gdW5kZWZpbmVkKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBib2R5Lm93bmVyRW1haWxJZD0gYXJndW1lbnRzWzJdXHJcbiAgICAgICAgICAgICAgICBib2R5LnBlcm1pdHRlZEVtYWlsSWQ9IGFyZ3VtZW50c1szXVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChwZXJtaXR0ZWRFbWFpbElkPT11bmRlZmluZWQpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGJvZHkub3duZXJFbWFpbElkPSBhcmd1bWVudHNbMl1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAob3duZXJFbWFpbElkPT11bmRlZmluZWQpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGJvZHkucGVybWl0dGVkRW1haWxJZD0gcGVybWl0dGVkRW1haWxJZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChvd25lckVtYWlsSWQ9PXVuZGVmaW5lZCAmJiBwZXJtaXR0ZWRFbWFpbElkPT11bmRlZmluZWQpXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGJvZHkub3duZXJFbWFpbElkPSBcIlwiXHJcbiAgICAgICAgICAgICAgICBib2R5LnBlcm1pdHRlZEVtYWlsSWQ9IFwiXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmZXRjaCh1cmwsIHtcclxuICAgICAgICAgICAgICAgIG1ldGhvZDpcIlBPU1RcIixcclxuICAgICAgICAgICAgICAgIGJvZHk6SlNPTi5zdHJpbmdpZnkoYm9keSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiBoZWxwZXJzLmhhbmRsZUZldGNoRXJyb3IocmVzcG9uc2UpKVxyXG4gICAgICAgICAgICAuY2F0Y2goZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpXHJcbiAgICAgICAgICAgICAgICByZXR1cm59KVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyBsb2dvdXQgKClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIC8vZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2N1cnJlbnRfdXNlcicpLmlubmVySFRNTD0gXCJcIlxyXG4gICAgICAgICAgICBkZWxldGUgV2luZG93Lm1iVXNlclxyXG4gICAgICAgICAgICBkZWxldGUgV2luZG93LmVQdWJsaWNLZXlcclxuICAgICAgICAgICAgZGVsZXRlIFdpbmRvdy5zUHVibGljS2V5XHJcbiAgICAgICAgICAgIGRlbGV0ZSBXaW5kb3cuZVByaXZhdGVLZXlcclxuICAgICAgICAgICAgZGVsZXRlIFdpbmRvdy5zUHJpdmF0ZUtleVxyXG4gICAgICAgIH1cclxuICAgICAgICAvL0Fzc2V0IGZ1bmN0aW9uc1xyXG5cclxuICAgICAgICBhc3luYyBuZXdBc3NldChhc3NldE5hbWUsYXNzZXRBZG1pbj1XaW5kb3cubWJVc2VyLGFzc2V0TWludGVycyxpbml0QmFsYW5jZSxub2RlSXA9dGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydD10aGlzLmxvY2FsTm9kZVBvcnQpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBhc3NldC5uZXdBc3NldChhc3NldE5hbWUsYXNzZXRBZG1pbixhc3NldE1pbnRlcnMsaW5pdEJhbGFuY2Usbm9kZUlwLG5vZGVQb3J0KVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyBnZXRNaW50ZXJzIChhc3NldE5hbWUsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KSB7XHJcbiAgICAgICAgICAgIGFzc2V0LmdldE1pbnRlcnMoYXNzZXROYW1lLG5vZGVJcCxub2RlUG9ydClcclxuICAgICAgICB9XHJcbiAgICAgICAgYXN5bmMgY2hhbmdlQWRtaW4oYXNzZXROYW1lLGFzc2V0QWRtaW49dGhpcy5tYlVzZXIsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgYXNzZXQuY2hhbmdlQWRtaW4oYXNzZXROYW1lLGFzc2V0QWRtaW4sbm9kZUlwLG5vZGVQb3J0KVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyBidXJuIChhc3NldE5hbWUsYW1vdW50LG5vZGVJcD10aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0PXRoaXMubG9jYWxOb2RlUG9ydClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFzc2V0LmJ1cm4oYXNzZXROYW1lLGFtb3VudCxub2RlSXAsbm9kZVBvcnQpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGFzeW5jIHRvdGFsU3VwcGx5ICAoYXNzZXROYW1lLG5vZGVJcD10aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0PXRoaXMubG9jYWxOb2RlUG9ydClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFzc2V0LnRvdGFsU3VwcGx5KGFzc2V0TmFtZSxub2RlSXAsbm9kZVBvcnQpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGFzeW5jIHRyYW5zZmVyIChhc3NldE5hbWUsZnJvbT10aGlzLm1iVXNlcix0byxhbW91bnQsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgYXNzZXQudHJhbnNmZXIoYXNzZXROYW1lLGZyb20sdG8sYW1vdW50LG5vZGVJcCxub2RlUG9ydClcclxuICAgICAgICB9XHJcbiAgICAgICAgYXN5bmMgdHJhbnNmZXJUb0FkbWluICAoYXNzZXROYW1lLGZyb20sYW1vdW50LG5vZGVJcD10aGlzLmxvY2FsTm9kZUlQLG5vZGVQb3J0PXRoaXMubG9jYWxOb2RlUG9ydClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFzc2V0LnRyYW5zZmVyVG9BZG1pbihhc3NldE5hbWUsZnJvbSxhbW91bnQsbm9kZUlwLG5vZGVQb3J0KVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyB0cmFuc2ZlckZyb21BZG1pbiAgKGFzc2V0TmFtZSx0byxhbW91bnQsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgYXNzZXQudHJhbnNmZXJGcm9tQWRtaW4oYXNzZXROYW1lLHRvLGFtb3VudCxub2RlSXAsbm9kZVBvcnQpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGFzeW5jIGFkZE1pbnRlcnMgKGFzc2V0TmFtZSxhc3NldE1pbnRlcnMsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9dGhpcy5sb2NhbE5vZGVQb3J0KVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgYXNzZXQuYWRkTWludGVycyhhc3NldE5hbWUsYXNzZXRNaW50ZXJzLG5vZGVJcCxub2RlUG9ydClcclxuICAgICAgICB9XHJcbiAgICAgICAgYXN5bmMgcmVtb3ZlTWludGVycyAoYXNzZXROYW1lLGFzc2V0TWludGVycyxub2RlSXA9dGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydD10aGlzLmxvY2FsTm9kZVBvcnQpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBhc3NldC5yZW1vdmVNaW50ZXJzKGFzc2V0TmFtZSxhc3NldE1pbnRlcnMsbm9kZUlwLG5vZGVQb3J0KVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyBtaW50IChhc3NldE5hbWUsdGFyZ2V0LGFtb3VudCxub2RlSXA9dGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydD10aGlzLmxvY2FsTm9kZVBvcnQpXHJcbiAgICAgICAge1x0XHJcblx0XHR2YXIgbWludGVyPVdpbmRvdy5tYlVzZXJcclxuICAgICAgICAgICAgYXNzZXQubWludChhc3NldE5hbWUsbWludGVyLHRhcmdldCxhbW91bnQsbm9kZUlwLG5vZGVQb3J0KVxyXG4gICAgICAgIH1cclxuICAgICAgICBhc3luYyBjaGVja0FkbWluQmFsYW5jZShhc3NldE5hbWUsbm9kZUlwPXRoaXMubG9jYWxOb2RlSVAsbm9kZVBvcnQ9IHRoaXMubG9jYWxOb2RlUG9ydClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGFzc2V0LmNoZWNrQWRtaW5CYWxhbmNlKGFzc2V0TmFtZSxub2RlSXAsbm9kZVBvcnQpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGFzeW5jIGNoZWNrQmFsYW5jZSAoYXNzZXROYW1lLHRhcmdldCxub2RlSXA9dGhpcy5sb2NhbE5vZGVJUCxub2RlUG9ydD10aGlzLmxvY2FsTm9kZVBvcnQpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBhc3NldC5jaGVja0JhbGFuY2UoYXNzZXROYW1lLHRhcmdldCxub2RlSXAsbm9kZVBvcnQpXHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuICAgIHdpbmRvdy5NZWRibG9ja3MgPSBNZWRibG9ja3MgXHJcbn0pKCkgXHJcbiJdLCJzb3VyY2VSb290IjoiIn0=