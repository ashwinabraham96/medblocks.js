var asset = {}
// assetAdmin is logged in user
asset.newAsset = (assetName,assetMinters,initData,nodeIp,nodePort) => {
    const body = {
        assetName: assetName,
        assetAdmin: this.mbUser,
        assetMinters: assetMinters,
        balance: initData
    }
    const url = "http://" + nodeIp + ":"+ nodePort +"/getUser"        
    fetch(url, {
        method: "POST",
        body: JSON.stringify(body)
    })
        .then((response) => {
            helpers.handleFetchError(response)
            response.json()
                .then(async (responseJson) => {
                    Window.ePublicKey = await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.ePublicKey)))
                    Window.sPublicKey = await crypt.getEPublicKeyFromBuffer(helpers.base64ToArrayBuffer(helpers.removeLines(responseJson.sPublicKey)))
                    console.log(Window.ePublicKey)
                    console.log(Window.sPublicKey)
                })
        }
        )
        .catch(e => {
            console.error('Error occured while getting public keys: ' + e)
        })
}
export default asset